<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Modules\Dashboard\Entities\AdminMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share([
            'side_menu'      => (Schema::hasTable('admin_menu'))
                ? AdminMenu::where('parent', '=', 0)
                    ->orWhereNull('parent')
                    ->orderBy('position', 'asc')
                    ->with('subMenus')
                    ->get()
                    ->toArray()
                : [],
        ]);
    }
}
