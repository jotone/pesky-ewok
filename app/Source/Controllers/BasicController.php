<?php

namespace App\Source\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

abstract class BasicController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Pagination filters
     * @var array
     */
    protected $filters = [
        'order_by'  => 'id',
        'order_dir' => 'asc',
        'page'      => 1,
        'select'    => '*'
    ];

    /**
     * Display a listing of the resource.
     * @param Request $request
     */
    public function index(Request $request)
    {
        abort(404);
    }

    /**
     * Show the specified resource.
     * @param int $id
     */
    public function show(int $id)
    {
        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     * @param Request $request
     */
    public function create(Request $request)
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @param Request $request
     */
    public function edit(int $id, Request $request)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     * @param int $id
     * @param Request $request
     */
    public function update(int $id, Request $request)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     */
    public function destroy(int $id)
    {
        abort(404);
    }
}
