<?php

namespace App\Source\Controllers;

use Illuminate\Http\Request;
use Modules\Settings\Entities\Settings;

abstract class BasicApiController extends BasicController
{
    /**
     * Set request data to filters
     * @param $args
     */
    protected function bindFilters($args)
    {
        //Get select query
        if (isset($args['select']) && is_array($args['select'])) {
            $this->filters['select'] = $this->getSelection($args['select']);
        }

        //Get search request
        if (isset($args['search']) && is_array($args['search'])) {
            $this->filters['search'] = $args['search'];
        }

        //Get order field
        if (isset($args['order_by']) && in_array($args['order_by'], $this->getModelColumns())) {
            $this->filters['order_by'] = $args['order_by'];
        }

        //Get order direction
        if (isset($args['order_dir']) && ($args['order_dir'] == 'asc' || $args['order_dir'] == 'desc')) {
            $this->filters['order_dir'] = $args['order_dir'];
        }

        //Get items per page
        $this->filters['take'] = (isset($args['take']) && is_numeric($args['take']) && $args['take'] >= 1)
            ? $args['take']
            : Settings::where('user_id', auth()->user()->id)->where('key', 'take')->value('value');

        //Get page number
        if (isset($args['page']) && is_numeric($args['page']) && $args['page'] >= 1) {
            $this->filters['page'] = $args['page'];
        }

        //Get request relations
        if (isset($args['with'])) {
            $this->filters['with'] = $args['with'];
        }
    }

    /**
     * Apply filter value "with" to model as callback function
     * @param $value
     * @param $with
     */
    protected function checkFilterQueryMethods(&$value, $with)
    {
        if (is_array($with)) {
            foreach ($with as $method) {
                if (method_exists($this->controllerSettings['model'], $method)) {
                    $value->$method = $value->$method()->get();
                }
            }
        } else {
            if (method_exists($this->controllerSettings['model'], $with)) {
                $value->{$with} = $value->{$with}()->get();
            }
        }
    }

    /**
     * Get model collection
     * @param Request $request
     * @return mixed
     */
    protected function getCollection(Request $request)
    {
        $this->bindFilters($request->only('order_by', 'order_dir', 'page', 'select', 'search', 'take', 'with'));

        $collection = (isset($this->controllerSettings['custom_collection']))
            ? $this->customCollection($this->filters['select'])
            : $this->controllerSettings['model']::select($this->filters['select']);

        if (isset($this->filters['with'])) {
            $collection->with($this->filters['with']);
        }

        if (isset($this->filters['search'])) {
            foreach ($this->filters['search'] as $key => $value) {
                if (strpos($key, '.') !== false) {
                    $key = explode('.', $key);

                    $collection->whereHas($key[0], function ($q) use ($key, $value) {
                        $q->where($key[1], 'ilike', '%' . $value . '%');
                    });
                } else {
                    $collection->where($key, 'ilike', '%' . $value . '%');
                }
            }
        }

        $total = $collection->count();

        if ($this->filters['take'] > 0) {
            $collection->skip(($this->filters['page'] - 1) * $this->filters['take'])->take($this->filters['take']);
        }

        $collection = $collection->orderBy($this->filters['order_by'], $this->filters['order_dir'])->get();

        if (isset($this->filters['with'])) {
            $collection->map(function ($value) {
                $this->checkFilterQueryMethods($value, $this->filters['with']);
                return $value;
            });
        }
        return [
            'collection' => $collection,
            'total'      => $total
        ];
    }

    /**
     * Get model by ID
     * @param int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    protected function getModel(int $id)
    {
        $model = $this->controllerSettings['model']::find($id);

        return (!$model)
            ? response([], 404)
            : response([
                'model' => $model
            ]);
    }

    /**
     * Run array intersection, then get unique values, then reset array keys
     * @param array $args
     * @return array
     */
    protected function getSelection(array $args): array
    {
        return array_values(array_unique(array_intersect($args, $this->getModelColumns())));
    }

    /**
     * Get model columns
     * @return array
     */
    protected function getModelColumns(): array
    {
        return \DB::getSchemaBuilder()->getColumnListing($this->controllerSettings['table']);
    }

    /**
     * Replace controller messages with attribute
     * @param $attr
     * @param $message
     * @return string
     */
    protected function message($message, $attr = null)
    {
        return ($attr)
            ? preg_replace('/\:attribute/', $attr, $message)
            : $message;
    }

    /**
     * Remove model by ID
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    protected function removeModel($id)
    {
        $model = $this->controllerSettings['model']::find($id);
        if (!$model) {
            return response([], 404);
        }
        $model->delete();
        return response([], 204);
    }

    /**
     * @param $validator
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    protected function validationErrorResponse($validator)
    {
        return response([
            'errors' => $validator->errors()
        ], 400);
    }
}