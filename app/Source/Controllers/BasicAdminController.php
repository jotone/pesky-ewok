<?php


namespace App\Source\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Dashboard\Entities\AdminMenu;
use Modules\Settings\Entities\Settings;

abstract class BasicAdminController extends BasicController
{
    /**
     * Create breadcrumbs array
     * @param Request $request
     * @return array
     */
    protected function buildBreadcrumbs(Request $request)
    {
        //Breadcrumbs array with start point
        $breadcrumbs = [
            [
                'route' => route('admin.dashboard.index'),
                'name'  => AdminMenu::where('slug', 'dashboard')->value('title')
            ]
        ];
        //Get current route name
        $route_name = explode('.', $request->route()->getName());

        //Get last item of route identifier
        $action = $route_name[count($route_name) - 1];

        //Remove first and last route array elements to get pure page name
        unset($route_name[count($route_name) - 1], $route_name[0]);
        $route_name = array_values($route_name);

        //Build breadcrumbs array
        foreach ($route_name as $route_slug) {
            if ($route_slug != 'dashboard') {
                $path[] = $route_slug;

                $breadcrumbs[] = [
                    'route' => route('admin.' . implode('.', $path) . '.index'),
                    'name'  => AdminMenu::where('route', implode('.', $path))->value('title')
                ];
            }
        }

        //Switch current page name
        switch ($action) {
            case 'index':
                $title = 'List';
                break;
            case 'create':
                $title = 'Create';
                break;
            case 'edit':
                $title = 'Edit';
                break;
            default:
                $title = null;
        }

        //Set breadcrumb last element
        $breadcrumbs[] = [
            'route' => null,
            'name'  => $title
        ];

        return $breadcrumbs;
    }

    /**
     * Create pagination parameters
     * @param $items_count
     * @param $filters
     * @return array
     */
    public function buildPagination($items_count, $filters)
    {
        //Get total pages count
        $total = ceil($items_count / $filters['take']);
        //Get approximate start page number
        $start = $filters['page'] - 3;
        //Get approximate last page number
        $last = $filters['page'] + 3;
        //Check if start page number is less than 0
        if ($start < 1) {
            //Set shift value for last page
            $shift = abs($start) + 1;
            //Set start page is 1
            $start = 1;
            //Apply shift value
            $last += $shift;
        }

        //Check if last page number is greater than total pages count
        if ($last > $total) {
            $last = $total;
        }

        //Check if current page value is nit shifted
        if ($filters['page'] >= ($total - 3)) {
            //Get shift value
            $shift = $total - $filters['page'];
            //Apply shift
            $start -= 3 - $shift;
            //Check if start page number is not less than 1
            if ($start < 1) {
                $start = 1;
            }
        }

        return [
            'current'  => (int)$filters['page'],
            'start'    => (int)$start,
            'endpoint' => (int)$last,
            'total'    => (int)$total,
        ];
    }

    /**
     * Render admin index page
     * @param \Illuminate\Http\Request $request
     * @param array $variables
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function renderIndex(Request $request, $variables = [])
    {
        $args = $request->only('page', 'search');

        //Get page number
        if (isset($args['page']) && is_numeric($args['page']) && $args['page'] >= 1) {
            $this->filters['page'] = $args['page'];
        }

        if (isset($args['search']) && is_array($args['search'])) {
            $this->filters['search_query'] = '';
            $this->filters['search'] = $args['search'];
        }

        $this->filters['take'] = Settings::where('user_id', Auth::user()->id)->where('key', 'take')->value('value');

        $this->filters['select'] = $this->requestProperties['select'];
        $this->filters['with'] = $this->requestProperties['with'];

        return view($this->controllerSettings['module'] . '::index', array_merge([
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //Request filters data
            'filters'     => $this->filters,
            //DataTable columns array
            'tableBuilder' => $this->tableBuilder,
        ], $variables));
    }
}