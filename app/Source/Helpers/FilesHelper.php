<?php

namespace App\Source\Helpers;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Modules\Files\Entities\File;
use Modules\Settings\Entities\Settings;
use Modules\User\Entities\User;

class FilesHelper
{
    /**
     * Create folder
     * @param string $entity
     * @param int $entity_id
     * @param $date_to_folders
     * @param string $tail
     * @return string
     */
    public static function createFolder(string $entity, int $entity_id, string $tail = '')
    {
        $folder = Str::finish(env('FILE_DUMP'), '/') . $entity . '/' . $entity_id;
        $date_to_folders = Settings::where('key', 'files_to_datefolders')->value('value');
        if ($date_to_folders) {
            $folder .= Str::start(date('Y/m/d'), '/');
        }

        if (!empty($tail)) {
            $folder .= Str::start($tail, '/');
        }

        if (!is_dir($folder)) {
            mkdir(public_path($folder), 0777, true);
        }

        return Str::finish($folder, '/');
    }

    /**
     * Get simplify mime type
     * @param string $file
     * @return string
     */
    public static function getFileType(string $file):string
    {
        return preg_replace('/\/.*/', '', mime_content_type(public_path($file)));
    }

    /**
     * Save file by blob content
     * @param string $file
     * @param string $name
     * @param string $entity
     * @param int $entity_id
     * @return array
     */
    public static function saveBlob(string $file, string $name, string $entity, int $entity_id)
    {
        $allowed_mimes = self::getAllowedMimes();

        $image_data = preg_match('/^data:image\/(\w+);base64,/', $file, $type);

        if (!$image_data || count($type) < 2) {
            return [
                'status' => 'error',
                'data'   => 'Неверное содержание файла.'
            ];
        }

        //Get file mime-type
        $mime = preg_replace('/(data:|;base64.*)/', '', $type[0]);

        //Check mime-type
        if (!isset($allowed_mimes[$mime])) {
            return [
                'status' => 'error',
                'data'   => 'Данный mime-type ' . $mime . ' не поддерживается.'
            ];
        }

        //Process image data
        $image_data = base64_decode(substr($file, strpos($file, ',') + 1));

        if (!$image_data) {
            return [
                'status' => 'error',
                'data'   => 'Неверное содержание файла.'
            ];
        }
        //Create folder for file
        $folder = FilesHelper::createFolder($entity, $entity_id);

        $ext = strtolower($type[1]);
        $name = preg_replace('/[\s\-_]+/', '-', $name);
        $file_path = pathinfo($name);

        //Get filename
        $name = (file_exists(public_path($folder) . $name))
            ? $file_path['filename'] . '-' . uniqid() . '.' . $ext
            : $file_path['filename'] . '.' . $ext;

        //save file
        file_put_contents(public_path($folder) . $name, $image_data);

        return [
            'status'   => 'success',
            'data'     => Str::start($folder . $name, '/'),
            'external' => false
        ];
    }

    /**
     * Save file
     * @param $file
     * @param $user_id
     * @return array
     */
    public static function saveFile($file, $entity, $entity_id)
    {
        $allowed_mimes = self::getAllowedMimes();
        if (!isset($allowed_mimes[$file->getMimeType()])) {
            return [
                'status' => 'error',
                'data'   => 'Неверное содержание файла.'
            ];
        }

        //Create folder for file
        $folder = FilesHelper::createFolder($entity, $entity_id);

        $mime = $file->getMimeType();

        $ext = explode(', ', $allowed_mimes[$mime])[0];

        $name = preg_replace('/[\s\-_]+/', '-', $file->getClientOriginalName());
        $file_path = pathinfo($name);

        $file_name = (file_exists(public_path($folder) . $name))
            ? $file_path['filename'] . '-' . uniqid() . $ext
            : $file_path['filename'] . $ext;

        $file->move(public_path($folder), $file_name);

        return [
            'status'   => 'success',
            'data'     => Str::start($folder . $file_name, '/'),
            'external' => false
        ];
    }

    /**
     * Get allowed file mime-types
     * @return mixed
     */
    public static function getAllowedMimes()
    {
        return Settings::select('caption', 'key')
            ->whereNotNull('section')
            ->where('value', '1')
            ->pluck('caption', 'key')
            ->toArray();
    }

    /**
     * Create and save thumbnail image
     * @param array $file_data
     * @param string $key
     * @param string $prefix
     * @param int $mult
     */
    public static function saveThumb(array $file_data, string $key, string $prefix, int $mult = 1): void
    {
        $settings_dimensions = Settings::where('key', $key)->value('value');

        $dimensions = (isset($settings_dimensions) && CommonFunctions::isAssoc($settings_dimensions))
            ? $settings_dimensions
            : ['width' => env('THUMB_WIDTH') * $mult, 'height' => env('THUMB_HEIGHT') * $mult];

        $thumb = Image::make(Str::finish(public_path($file_data['dirname']), '/') . $file_data['basename'])
            ->resize($dimensions['width'], $dimensions['height'], function ($constraint) {
                $constraint->aspectRatio();
            });

        $thumb_name = 'thumb-' . $prefix . '-' . $file_data['filename'];

        $dirname = public_path(Str::finish($file_data['dirname'], '/') . 'thumb/');

        if (!is_dir($dirname)) {
            mkdir($dirname, 0777, true);
        }

        $thumb->save($dirname . $thumb_name . '.jpg', 80, 'jpg');
    }
}