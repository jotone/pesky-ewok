<?php

namespace App\Source\Helpers;

class CommonFunctions
{
    /**
     * Convert binary value into hex
     * @param string $value
     * @return string
     */
    public static function bin2hex(string $value): string
    {
        return base_convert($value, 2, 16);
    }

    /**
     * Convert hex value into binary
     * @param string $value
     * @return string
     */
    public static function hex2bin(string $value): string
    {
        $str = str_split($value);
        $result = '';
        foreach ($str as $char) {
            $result .= str_pad(base_convert($char, 16, 2), 4, '0', STR_PAD_LEFT);
        }
        return $result;
    }

    /**
     * Check if string is json array
     * @param string $str
     * @return bool
     */
    public static function isJson(string $str) {
        json_decode($str);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * Check if array is associative
     * @param array $arr
     * @return bool
     */
    public static function isAssoc(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}