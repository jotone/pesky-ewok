<?php

namespace App\Console\Commands;

use App\Source\Helpers\CommonFunctions;
use Modules\Dashboard\Entities\AdminMenu;
use Modules\Permissions\Entities\Permission;
use Modules\Roles\Entities\Role;
use Modules\User\Entities\User;
use Modules\Settings\Entities\Settings;
use Illuminate\Console\Command;
use Nwidart\Modules\Facades\Module;

class appInstall extends Command
{
    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'app:install';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Install application environment.';

    /**
     * Create a new command instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @throws \Exception
     */
    public function handle()
    {
        //Remove menu items
        AdminMenu::select('id')->delete();
        //Create main user
        $user = $this->createUser();
        //Create menu and permissions
        $this->createMenuAndPermissions($user);
        //Create settings
        $this->createSettings();
        //Create folders
        $this->createFolders();

        if (!is_dir(public_path(env('FILE_DUMP')))) {
            mkdir(public_path(env('FILE_DUMP')), 0777, true);
        }
    }

    /**
     * Create menu item
     * @param $item
     * @param int $parent
     */
    protected static function createMenuItem($item, $parent = 0): void
    {
        $item['parent'] = $parent;

        $menu = AdminMenu::create([
            'title'      => $item['title'],
            'slug'       => $item['slug'],
            'route'      => $item['route'],
            'image'      => $item['image'],
            'parent'     => $parent,
            'position'   => $item['position'],
            'is_section' => $item['is_section'],
            'editable'   => $item['editable'],
        ]);

        if (isset($item['inner']) && !empty($item['inner'])) {
            foreach ($item['inner'] as $inner_item) {
                self::createMenuItem($inner_item, $menu->id);
            }
        }
    }

    /**
     * Create setting default values
     * @param $settings
     * @param $callback
     */
    protected function createDefaultValues($settings, $callback): void
    {
        foreach ($settings as $admin_slug => $sections) {
            $callback($admin_slug, $sections);
        }
    }

    /**
     * Create main user
     * @return mixed
     * @throws \Exception
     */
    protected function createUser()
    {
        if (!env('ADMIN_USER', false) || !env('ADMIN_PSW', false)) {
            throw new \Exception('Please add ADMIN_USER and ADMIN_PSW to .env file.');
        }
        $user = User::where('email', env('ADMIN_USER'))->first();

        if (!$user) {
            $user = User::create([
                'email'    => env('ADMIN_USER'),
                'password' => bcrypt(env('ADMIN_PSW'))
            ]);
        }
        $this->info('Main User created');

        return $user;
    }

    /**
     * Create menu, roles, permissions
     * @param User $user
     */
    protected function createMenuAndPermissions(User $user)
    {
        $menu = [];
        $role = null;
        //Apply modules configs
        foreach (Module::scan() as $value) {
            $name = mb_strtolower($value->getName());

            //Fill menu array
            if (config($name . '.menu')) {
                $temp = config($name . '.menu');
                if (!isset($temp['relate_to'])) {
                    if (isset($temp['inner'])) {
                        $inner = $temp['inner'];
                        if (isset($menu[$temp['slug']])) {
                            $inner = array_merge($inner, $menu[$temp['slug']]['inner']);
                        }
                        unset($temp['inner']);
                        $menu[$temp['slug']] = $temp;
                        $menu[$temp['slug']]['inner'] = $inner;
                    } else {
                        $menu[$temp['slug']] = $temp;
                    }
                } else {
                    $menu[$temp['relate_to']]['inner'][] = $temp;
                }
            }
            //Create admin permissions
            if (config($name . '.permissions')) {
                $permissions = config($name . '.permissions');
                if (CommonFunctions::isAssoc($permissions)) {
                    $permissions = [$permissions];
                }
                foreach ($permissions as $permission) {
                    $model = Permission::where('route', $permission['route'])->first();

                    if (!$model) {
                        Permission::create([
                            'title' => $permission['title'],
                            'route' => $permission['route'],
                        ]);
                    } else {
                        $model->title = $permission['title'];
                        $model->save();
                    }
                }
            }

            if ($name == 'roles') {
                $role = Role::create(config($name . '.default_values'));
            }
        }

        //Set permissions to admin roles
        $user->roles()->attach($role->id);
        foreach (Permission::get() as $permission) {
            if ($role) {
                $role->permissions()->save($permission, ['value' => 'f']);
            }
        }
        $this->info('Admin role created');

        //Create menu
        foreach ($menu as $item) {
            self::createMenuItem($item);
        }
        $this->info('Admin menu created');
    }

    /**
     * Create site settings
     */
    protected function createSettings()
    {
        //Apply modules configs
        foreach (Module::scan() as $value) {
            $name = mb_strtolower($value->getName());

            $default_values = [config($name . '.default_values')];

            if ($default_values[0]) {
                foreach ($default_values as $item) {
                    //Check the item is settings
                    if ($name == 'settings') {
                        $this->createDefaultValues($item, function ($admin_slug, $sections) {
                            $admin_menu = AdminMenu::where('slug', $admin_slug)->value('id');

                            foreach ($sections as $section_name => $section_data) {
                                if ($section_name == 'none') {
                                    $section_name = null;
                                }

                                foreach ($section_data as $setting) {
                                    $type = 'text';
                                    if (is_bool($setting['value'])) {
                                        $type = 'bool';
                                    }

                                    if (is_string($setting['value'])) {
                                        if (strpos($setting['value'], ', ') !== false) {
                                            $type = 'array';
                                        } else {
                                            $type = 'string';
                                        }
                                    }

                                    Settings::create([
                                        'caption'       => $setting['caption'],
                                        'key'           => $setting['key'],
                                        'value'         => $setting['value'],
                                        'description'   => (isset($setting['about']))
                                            ? $setting['about']
                                            : null,
                                        'type'          => $type,
                                        'global'        => true,
                                        'section'       => $section_name,
                                        'admin_menu_id' => $admin_menu
                                    ]);
                                }
                            }
                        });
                    }
                }
            }
        }
        $this->info('Admin settings created');
    }

    /**
     * Create image folders
     */
    protected function createFolders()
    {
        if (!is_dir(public_path(env('USER_PHOTO_DIR')))) {
            mkdir(public_path(env('USER_PHOTO_DIR')), 0777, true);
        }
        if (!is_dir(public_path(env('THUMB_PHOTO_DIR')))) {
            mkdir(public_path(env('THUMB_PHOTO_DIR')), 0777, true);
        }
        if (!is_dir(public_path(env('TEMP_PHOTO_DIR')))) {
            mkdir(public_path(env('TEMP_PHOTO_DIR')), 0777, true);
        }
    }
}
