<?php

namespace App\Http\Middleware;

use App\Source\Helpers\CommonFunctions;
use Closure;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Settings\Entities\Settings;

/**
 * Check if user has access to admin panel
 * Class RedirectAdmin
 * @package App\Http\Middleware
 */
class RedirectAdmin
{
    protected static $routePermissions = [
        'admin.:route.index'  => 0,
        'api.:route.index'    => 0,
        'api.:route.show'     => 0,
        'admin.:route.create' => 1,
        'api.:route.store'    => 1,
        'admin.:route.edit'   => 2,
        'api.:route.update'   => 2,
        'api.:route.destroy'  => 3
    ];

    /**
     * @param $request
     * @param Closure $next
     * @param string $guard
     * @return ResponseFactory|RedirectResponse|Response|Redirector|mixed
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {
        $route_name = app('router')->getRoutes()->match($request)->getName();

        if (!Auth::check() && !auth()->user()) {
            return ($request->ajax())
                ? response([], 401)
                : redirect(route('admin.auth.index'));
        }

        $request_name = preg_replace(
            '/(^admin\.|^api\.|\.create.*|\.edit.*|\.store.*|\.update.*|\.destroy.*|\.show.*|\.index.*)/',
            '',
            $route_name
        );

        $user = Auth::user();
        if (!$user) {
            $user = auth()->user();
        }
        $user->last_seen_at = now();
        $user->save();

        $permissions = [];
        foreach ($user->roles()->get() as $role) {
            $permission = $role->permissions()->where('route', $request_name)->first();

            if ($permission) {
                if (isset($permissions[$permission->route])) {
                    $value = hexdec($permission->pivot->value);
                    if ($value > hexdec($permissions[$permission->route])) {
                        $permissions[$permission->route] = $permission->pivot->value;
                    }
                } else {
                    $permissions[$permission->route] = $permission->pivot->value;
                }
            }
        }

        $auth = false;

        foreach ($permissions as $key => $val) {
            $route_permission_key = preg_replace('/' . $key . '/', ':route', $route_name);
            $user_permission = str_split(CommonFunctions::hex2bin($permissions[$request_name]));

            if (isset(self::$routePermissions[$route_permission_key])) {
                $auth = ($user_permission[self::$routePermissions[$route_permission_key]] === '1');
            }
        }

        return ($auth || (
                !$auth
                && $request->ajax()
                && (
                    $route_name == 'api.settings.index.admin'
                    || ($route_name == 'api.settings.update.user' && $request->route('id') == $user->id)
                )
            ))
            ? $next($request)
            : ($request->ajax()
                ? response([], 403)
                : abort(403));
    }
}