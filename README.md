# Requirements

- PHP ^7.3
- Postgres ^12
- Node.js ^12.9.1
- npm ^6.13
- libfreetype6-dev
- libjpeg62-turbo-dev
- libpng-dev
- libmagickwand-dev
- imagemagick
- php-imagick
- php-dev
- php-cli
- php-mbstring
- php-intl
- php-bcmath
- php-zip
- php-pgsql
- php-memcached
- ffmpeg
- ffmpegthumbs
- ffmpegthumbnailer
- ffmpeg-doc
- libxslt-dev
- libxpm-dev
- libvpx-dev
- libicu-dev
- libmcrypt-dev
- libpq-dev
- libxml2-dev
- libzmq3-dev
- libmemcached-dev
- memcached

# Installation

## Docker

Copy _.env.example_ file to _.env_

```bash
cp .env.example .env
```

Then run

```bash
make dc-build
make dc-i
```