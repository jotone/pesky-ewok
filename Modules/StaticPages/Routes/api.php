<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'as'         => 'api.',
    'middleware' => ['api'],
    'namespace'  => 'Api',
], function () {
    Route::group([
        'middleware' => ['jwt.verify', 'admin']
    ], function () {
        Route::resource('/static_pages', 'StaticPagesController')->except(['create', 'edit']);
    });
});