<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'as'         => 'admin.',
    'middleware' => ['admin'],
    'namespace'  => 'Admin',
    'prefix'     => 'admin'
], function () {
    Route::resource('/static_pages', 'StaticPagesController')->only(['index', 'create', 'edit']);
});
