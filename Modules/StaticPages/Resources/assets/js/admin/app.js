/**
 * Get items list
 * @param collection
 * @returns {string}
 */
App.getList = collection => {
  let body = '';
  for (let i in collection) {
    body += Templates.staticPages.render(collection[i]);
  }
  return body;
};

App.modules.staticPages = {
  editors: {},
  init: () => {
    const $list = $('.construction-elements-list')[0];

    new Sortable($list, {
      animation: 150,
      group: {
        name: 'group',
        pull: 'clone'
      },
      multiDrag: true,
      selectedClass: 'selected',
      sort: false
    });

    App.modules.staticPages.renewAcceptList();
  },
  /**
   * Set new width
   * @param $list
   */
  renewWidth: $list => {
    const width = Math.floor(100 / $list.children('li').length);
    $list.find('li').css({'width': width + '%'});

    if (!$list.find('li').length) {
      $list.html(Templates.staticPages.plug());
    }
  },
  /**
   * Add list as sortable
   */
  renewAcceptList: () => {
    $('.constructor-wrap .grid .control-row').each(function () {
      const $list = $(this).find('.grid-content-wrap');
      new Sortable($list[0], {
        animation: 150,
        group: 'group',
        onAdd: function (e) {
          $list.find('.grid-content-hint').remove();
          let items = [];
          if (!e.items.length) {
            items.push(e.item);
          } else {
            items = items.concat(e.items);
          }

          for (let i in items) {
            const elem = $list.find(items[i]);
            elem.empty().append(Templates.staticPages.blockControls(elem.data('type')));
          }
          App.modules.staticPages.renewWidth($list);
        }
      });
    });
  }
};

App.modules.staticPages.events = {
  '.constructor-wrap': [{
    /**
     * Add content row
     */
    action: 'on.click',
    item: 'button[name="addRow"]',
    callback: () => {
      $('.constructor-grid-wrap .grid').append(Templates.staticPages.row());
      App.modules.staticPages.renewAcceptList();
    }
  }, {
    /**
     * Edit column content
     */
    action: 'on.click',
    item: 'button[name="editCol"]',
    callback: function () {
      const $popup = $('.page-constructor-popup');
      const $parent = $(this).closest('li');
      const type = $parent.data('type');
      $parent.attr('data-selected', 1);

      $popup.attr('data-type', type);
      $popup.find('.title span').text(App.common.ucfirst(type));
      $popup.find('.page-constructor-content').html(Templates.staticPages.popup(type));

      if (!$parent.find('.grid-container-content button[name="editCol"]').length) {
        let value = JSON.parse($parent.attr('data-content'));
        switch (type) {
          case 'list':
          case 'radio':
            Templates.staticPages.elements.list.setValue($popup, value.content);
            break;
          case 'string':
          case 'text':
            Templates.staticPages.elements.string.setValue($popup, value.content);
            break;
          default:
            Templates.staticPages.elements[type].setValue($popup, value.content);
        }
      } else {
        switch (type) {
          case 'image':
          case 'slider':
            App.modules.file.replace($popup.find('.fileUpload'));
            break;
          case 'tabs':
            $popup.find('.tabs-container-list li:first').addClass('active');
            $popup.find('.tabs-container-texts-wrap .tabs-container-text:first').addClass('active');
            break;
        }
      }
      App.modules.popup.open($popup)
    }
  }, {
    /**
     * Remove content row
     */
    action: 'on.click',
    item: 'button[name="removeRow"]',
    callback: function () {
      const res = confirm('Do you really want to remove content row?');
      if (res) {
        $(this).closest('.control-row').remove();
      }
    }
  }, {
    /**
     * Remove content block
     */
    action: 'on.click',
    item: 'button[name="removeCol"]',
    callback: function () {
      const res = confirm('Do you really want to remove content block?');
      if (res) {
        const $list = $(this).closest('.grid-content-wrap');
        $(this).closest('li').remove();
        App.modules.staticPages.renewWidth($list);
      }
    }
  }, {
    /**
     * Click slide left/right
     */
    action: 'on.click',
    item: '.preview-slider-left, .preview-slider-right',
    callback: function () {
      const $parent = $(this).closest('.preview-slider');
      const $active = $parent.find('.preview-slider-body li.active');
      $active.removeClass('active');

      let $el;
      if ($(this).hasClass('preview-slider-right')) {
        $el = ($active.next('li').length)
          ? $active.next('li')
          : $parent.find('.preview-slider-body li:first');

      } else {
        $el = ($active.prev('li').length)
          ? $active.prev('li')
          : $parent.find('.preview-slider-body li:last');
      }
      $el.addClass('active');
    }
  }],
  '.page-constructor-popup': [{
    /**
     * Remove data-selected on popup close
     */
    action: 'on.click',
    item: '.close',
    callback: function () {
      $('.grid-content-wrap li').removeAttr('data-selected');
    }
  }, {
    /**
     * Apply page content
     */
    action: 'on.click',
    item: 'button[name="applySection"]',
    callback: function () {
      const $parent = $(this).closest('.page-constructor-popup');
      const type = $parent.attr('data-type');
      const $gridElem = $('.grid-content-wrap li[data-selected]');
      let value = '';

      switch (type) {
        case 'list':
          value = Templates.staticPages.elements.list.getValue($parent, $gridElem);
          break;
        case 'radio':
          value = Templates.staticPages.elements.radio.getValue($parent, $gridElem);
          break;
        case 'text':
          value = Templates.staticPages.elements.string.getValue($parent, $gridElem);
          break;
        default:
          value = Templates.staticPages.elements[type].getValue($parent, $gridElem);
      }

      if (!value.length) {
        value = Templates.staticPages.editButton();
      }

      $gridElem.find('.grid-container-content').html(value);
      $gridElem.removeAttr('data-selected');

      $(this).closest('.popup-wrap').hide();
      $('.overlay').hide();
    }
  }, {
    /**
     * Set list content
     */
    action: 'on.click',
    item: '.list-container-wrap button[name="AddItem"]',
    callback: function () {
      const $parent = $(this).closest('.list-container-wrap');
      $parent.find('.list-container').append(Templates.staticPages.elements.list.item())
    }
  }, {
    /**
     * Remove list content
     */
    action: 'on.click',
    item: '.list-container-wrap .remove-list-container',
    callback: function () {
      const res = confirm('Do you really wand to remove this item?');
      if (res) {
        $(this).closest('li').remove();
      }
    }
  }, {
    /**
     * Move list content
     */
    action: 'on.click',
    item: '.list-container-controls-move-wrap .fa',
    callback: function () {
      const $item = $(this).closest('li');
      const newItem = $item.clone();

      if ($(this).hasClass('fa-angle-up') && $item.prev('li').length) {
        $item.prev('li').before(newItem);
        $item.remove();
      }

      if ($(this).hasClass('fa-angle-down') && $item.next('li').length) {
        $item.next('li').after(newItem);
        $item.remove();
      }
    }
  }, {
    /**
     * Add table column
     */
    action: 'on.click',
    item: '.table-container th .add-button',
    callback: function () {
      const rowsCount = $(this).closest('table').find('tr').length - 1;
      let i = 0;
      $(this).closest('table').find('tr').each(function () {
        if (i < rowsCount) {
          const $parent = $(this).closest('tr');
          const node = ($parent.find('th').length)
            ? 'th'
            : 'td';
          const item = $parent.find(node + ':first').html();

          $parent.find(node + ':last').before('<' + node + '>' + item + '</' + node + '>');
        }
        i++;
      });

      const collCount = $(this).closest('table').find('thead th').length;
      $(this).closest('table').find('tbody tr:last td').attr('colspan', collCount)
    }
  }, {
    /**
     * Add table row
     */
    action: 'on.click',
    item: '.table-container td .add-button',
    callback: function () {
      const item = $(this).closest('tbody').find('tr:first').html();
      $(this).closest('table').find('tbody tr:last').before('<tr>' + item + '</tr>');
    }
  }, {
    /**
     * Tab add
     */
    action: 'on.click',
    item: '.tabs-container-list .add-button',
    callback: function () {
      $(this).closest('li').before(Templates.staticPages.elements.tabs.li);
      $(this).closest('.tabs-container').find('.tabs-container-texts-wrap').append(Templates.staticPages.elements.tabs.text)
    }
  }, {
    /**
     * Tab click
     */
    action: 'on.click',
    item: '.tabs-container-list li',
    callback: function () {
      const tabsCount = $(this).closest('ul').find('li').length - 1;
      const index = $(this).index();
      if (index < tabsCount) {
        $(this).closest('ul').find('li').removeClass('active');
        $(this).addClass('active');

        const $parent = $(this).closest('.tabs-container');
        $parent.find('.tabs-container-caption span').text('#' + (index + 1));
        $parent.find('.tabs-container-texts-wrap .tabs-container-text').removeClass('active');
        $parent.find('.tabs-container-texts-wrap .tabs-container-text:eq(' + index + ')').addClass('active')
      }
    }
  }]
};

Templates.staticPages = {
  render: item => {
    console.log(item)
  },
  /**
   * Block controls
   * @returns {string}
   */
  blockControls: type => '<div class="grid-content-item-wrap">' +
    '<div class="block-controls-wrap"><div class="controls">' +
    Templates.staticPages.editButton() +
    Templates.staticPages.deleteButton('removeCol', 'Remove block.') +
    '<span class="block-type">' + type + '</span>' +
    '</div></div>' +
    '<div class="grid-container-content flex-center-center">' +
    Templates.staticPages.editButton() +
    '</div></div>',
  /**
   * Edit button
   * @returns {string}
   */
  editButton: () => '<button class="grid-button text-orange" name="editCol" type="button" title="Edit block.">' +
    '<span class="fa fa-pencil-square"></span>' +
    '</button>',
  /**
   * Delete button
   * @param {string} name
   * @param {string} title
   * @returns {string}
   */
  deleteButton: (name, title = '') =>
    '<button class="grid-button text-red" name="' + name + '" type="button" title="' + title + '">' +
    '<span class="fa fa-minus-circle"></span>' +
    '</button>',
  /**
   * Empty row plug
   * @returns {string}
   */
  plug: () => '<li class="grid-content-hint">Drag content element here&hellip;</li>',
  /**
   * Add empty row
   * @returns {string}
   */
  row: () => '<div class="control-row">' +
    '<ul class="grid-content-wrap flex-center-around">' + Templates.staticPages.plug() + '</ul>' +
    '<div class="row-controls">' +
    Templates.staticPages.deleteButton('removeRow', 'Remove row') +
    '</div></div>',
  /**
   * Popup elements
   */
  elements: {
    checkbox: {
      /**
       * Popup heading content element
       * @returns {string}
       */
      popupTag: () => '<div class="form-item-container">' +
        '<div class="row">' +
        '<p>Input name:</p>' +
        '<input name="checkboxContainerName" class="input-text col-1">' +
        '</div>' +
        '<div class="row">' +
        '<p>Placement of text relative to the checkbox:</p>' +
        '<label>' +
        '<input name="checkboxContainerPlace" type="radio" value="1" checked><span>Before</span>' +
        '</label><br><label>' +
        '<input name="checkboxContainerPlace" type="radio" value="0"><span>After</span>' +
        '</label></div>' +
        '<div class="row">' +
        '<input name="checkboxContainerText" class="input-text col-1" placeholder="Checkbox text&hellip;">' +
        '</div>' +
        '<div class="row flex-center-start">' +
        'Checkbox default state: <input name="checkboxContainerState" type="checkbox">' +
        '</div></div>',
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        const content = {
          name: App.common.transliteration.str2url($parent.find('input[name="checkboxContainerName"]').val().trim()),
          pos: parseInt($parent.find('input[name="checkboxContainerPlace"]:checked').val()),
          text: $parent.find('input[name="checkboxContainerText"]').val().trim(),
          val: (true === $parent.find('input[name="checkboxContainerState"]').prop('checked')) ? 1 : 0
        };
        const checked = (1 === content.val)
          ? 'checked'
          : '';
        const value = '<input type="checkbox" ' + checked + '>';

        $gridElem.attr('data-content', JSON.stringify({content: content}));

        return (1 === content.pos)
          ? content.text + value
          : value + content.text;
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        $popup.find('input[name="checkboxContainerName"]').val(value.name);
        $popup.find('input[name="checkboxContainerPlace"]').prop('checked', false);
        $popup.find('input[name="checkboxContainerPlace"][value="' + value.pos + '"]').prop('checked', true);
        $popup.find('input[name="checkboxContainerText"]').val(value.text);
        $popup.find('input[name="checkboxContainerState"]').prop('checked', parseInt(value.val));
      }
    },
    formEnd: {
      /**
       * String content element
       * @returns {string}
       */
      popupTag: () => '<div class="flex-center-center">End of form</div>',
      /**
       * Get value from popup
       * @returns {string}
       */
      getValue: () => '<div>&lt;FORM END&gt;</div>',
      /**
       * Set value to content popup
       * @returns {string}
       */
      setValue: () => ''
    },
    formStart: {
      /**
       * String content element
       * @returns {string}
       */
      popupTag: () => '<div class="form-item-container">' +
        '<div class="row">' +
        '<p>Form action route:</p>' +
        '<input name="formContainerAction" class="input-text col-1">' +
        '</div>' +
        '<div class="row">' +
        '<p>Form method:</p>' +
        '<select name="formContainerMethod" class="input-text-small">' +
        '<option value="GET">GET</option>' +
        '<option value="POST">POST</option>' +
        '<option value="PUT">PUT</option>' +
        '</select>' +
        '</div>' +
        '</div>',
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        const content = {
          action: $parent.find('input[name="formContainerAction"]').val().trim(),
          method: $parent.find('select[name="formContainerMethod"]').val()
        };
        $gridElem.attr('data-content', JSON.stringify({content: content}));

        return '<div>&lt;FORM START&gt;[<strong>' + content.method + '</strong>]::' + content.action + '</div>';
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        $popup.find('input[name="formContainerAction"]').val(value.action);
        $popup.find('select[name="formContainerMethod"] option[value="' + value.method + '"]').prop('selected', true);
      }
    },
    heading: {
      /**
       * Popup heading content element
       * @returns {string}
       */
      popupTag: () => '<select name="blockHeading" class="input-text-small col-1-5">' +
        '<option value="h1">H1</option>' +
        '<option value="h2">H2</option>' +
        '<option value="h3">H3</option>' +
        '<option value="h4">H4</option>' +
        '<option value="h5">H5</option>' +
        '<option value="h6">H6</option>' +
        '</select>' +
        Templates.staticPages.elements.string.popupTag('Enter heading caption'),
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        const tag = $parent.find('select[name="blockHeading"]').val();
        let value = $parent.find('[name="blockValue"]').val().trim();
        if (value.length) {
          $gridElem.attr('data-content', JSON.stringify({content: {tag: tag, val: value}}));
          value = App.common.tag(value, tag);
        }
        return value;
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        $popup.find('select[name="blockHeading"] option[value="' + value.tag + '"]').prop('selected', true);
        $popup.find('input[name="blockValue"]').val(value.val);
      }
    },
    htmlEditor: {
      /**
       * String content element
       * @returns {string}
       */
      popupTag: () => '<textarea class="wysiwyg" id="' + App.common.uniquid() + '"></textarea>',
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {*}
       */
      getValue: ($parent, $gridElem) => {
        const editorId = $parent.find('.wysiwyg').attr('id');
        const value = CKEDITOR.instances[editorId].getData();
        $gridElem.attr('data-content', JSON.stringify({content: value}));
        return value;
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        const editorId = $popup.find('.wysiwyg').attr('id');
        if (typeof CKEDITOR.instances[editorId] === 'undefined') {
          CKEDITOR.replace(editorId);
        }
        CKEDITOR.instances[editorId].setData(value);
      },
    },
    image: {
      /**
       * String content element
       * @returns {string}
       */
      popupTag: () => '<div class="fileUpload col-1" data-type="image"></div>',
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {*}
       */
      getValue: ($parent, $gridElem) => {
        const $image = $parent.find('.file-wrap img');
        let value = '';

        if ($image.length && $image.attr('alt') !== 'stub') {
          const content = {
            title: $parent.find('input[name="img_alt"]').val(),
            caption: $parent.find('input[name="img_caption"]').val(),
            description: $parent.find('textarea[name="img_description"]').val()
          };

          if (typeof $image.attr('data-upload') !== 'undefined') {
            //Uploaded
            content.src = $image.attr('src');
            content.name = $image.attr('alt');
          } else {
            //External
            content.src = $image.attr('alt');
            content.type = 'image';
          }

          $gridElem.attr('data-content', JSON.stringify({content: content}));

          value = '<div class="image-container"><img src="' + content.src + '" alt="">';

          if (content.title || content.caption || content.description) {
            let etc = '';
            if (content.title) etc += '<p class="tar">' + content.title + '</p>';
            if (content.caption) etc += '<p class="tar">' + content.caption + '</p>';
            if (content.description) etc += '<p class="tar">' + content.description + '</p>';
            value += '<div class="etc">' + etc + '</div>';
          }

          value += '</div>';
        }

        return value;
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        App.modules.file.replace($popup.find('.fileUpload'), value);
      }
    },
    input: {
      /**
       * String content element
       * @returns {string}
       */
      popupTag: () => '<div class="form-item-container">' +
        '<div class="row">' +
        '<p>Input name:</p>' +
        '<input name="inputContainerName" class="input-text col-1">' +
        '</div>' +
        '<div class="row">' +
        '<p>Placement of text relative to the input:</p>' +
        '<label>' +
        '<input name="inputContainerPlace" type="radio" value="1" checked><span>Before</span>' +
        '</label><br><label>' +
        '<input name="inputContainerPlace" type="radio" value="0"><span>After</span>' +
        '</label></div>' +
        '<div class="row">' +
        '<p>Text related to input:</p>' +
        '<input name="inputContainerText" class="input-text col-1">' +
        '</div>' +
        '<div class="row">' +
        '<p>Input placeholder text:</p>' +
        '<input name="inputContainerPlaceholder" class="input-text col-1">' +
        '</div></div>',
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        const content = {
          name: App.common.transliteration.str2url($parent.find('input[name="inputContainerName"]').val().trim()),
          pos: parseInt($parent.find('input[name="inputContainerPlace"]:checked').val()),
          text: $parent.find('input[name="inputContainerText"]').val().trim(),
          holder: $parent.find('input[name="inputContainerPlaceholder"]').val().trim()
        };
        const value = '<input placeholder="' + content.holder + '">';
        $gridElem.attr('data-content', JSON.stringify({content: content}));
        return (1 === content.pos)
          ? content.text + value
          : value + content.text;
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        $popup.find('input[name="inputContainerName"]').val(value.name);
        $popup.find('input[name="inputContainerPlace"]').prop('checked', false);
        $popup.find('input[name="inputContainerPlace"][value="' + value.pos + '"]').prop('checked', true);
        $popup.find('input[name="inputContainerText"]').val(value.text);
        $popup.find('input[name="inputContainerPlaceholder"]').val(value.holder);
      }
    },
    list: {
      /**
       * String content element
       * @returns {string}
       */
      popupTag: () => '<div class="list-container-wrap">' +
        '<ul class="list-container col-1 overflow-box"></ul>' +
        '<div class="tac"><button name="AddItem" class="button blue">Add Item</button></div>' +
        '</div>',
      /**
       * Content list element
       * @param value
       * @returns {string}
       */
      item: (value = '') => '<li class="flex-center-between">' +
        '<div class="list-container-controls-move-wrap">' +
        '<div class="fa fa-angle-up"></div><div class="fa fa-angle-down"></div>' +
        '</div>' +
        '<div class="col-1">' +
        '<input name="list-container-content" class="input-text col-1" value="' + value + '">' +
        '</div>' +
        '<div class="fa fa-times remove-list-container flex-center-center"></div>' +
        '</li>',
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        let content = [];
        let value = '';
        $parent.find('.list-container li').each(function () {
          const temp = $(this).find('input').val().trim();
          content.push(temp);
          value += '<li>' + temp + '</li>';
        });
        $gridElem.attr('data-content', JSON.stringify({content: content}));
        return '<ul>' + value + '</ul>';
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        $popup.find('.list-container').empty();
        for (let i in value) {
          $popup.find('.list-container').append(Templates.staticPages.elements.list.item(value[i]))
        }
      }
    },
    radio: {
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        let content = [];
        let value = '';
        const uniqueId = App.common.uniquid();
        $parent.find('.list-container li').each(function () {
          const temp = $(this).find('input').val().trim();
          content.push(temp);
          value += '<li>' +
            '<label class="flex-center-start"><input name="' + uniqueId + '" type="radio">' + temp + '</label>' +
            '</li>';
        });
        $gridElem.attr('data-content', JSON.stringify({content: content}));
        return '<ul>' + value + '</ul>';
      }
    },
    select: {
      /**
       * String content element
       * @returns {string}
       */
      popupTag: () => '<div class="select-container">' +
        '<p>Tag name:</p>' +
        '<div class="row">' +
        '<input name="selectContainerName" class="input-text col-md-90" placeholder="Enter tag name&hellip;">' +
        '</div></div>' +
        '<div class="list-container-wrap">' +
        '<ul class="list-container col-1 overflow-box"></ul>' +
        '<div class="tac"><button name="AddItem" class="button blue">Add Item</button></div>' +
        '</div>',
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        let content = {
          name: App.common.transliteration.str2url($parent.find('input[name="selectContainerName"]').val().trim()),
          options: []
        };
        let value = '';
        $parent.find('.list-container li').each(function () {
          const temp = $(this).find('input').val();
          content.options.push(temp);
          value += '<option>' + temp + '</option>';
        });
        $gridElem.attr('data-content', JSON.stringify({content: content}));
        return '<select>' + value + '</select>';
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        $popup.find('input[name="selectContainerName"]').val(value.name);
        $popup.find('.list-container').empty();
        for (let i in value.options) {
          $popup.find('.list-container').append(Templates.staticPages.elements.list.item(value.options[i]))
        }
      }
    },
    slider: {
      /**
       * String content element
       * @returns {string}
       */
      popupTag: () => '<div class="fileUpload col-1" data-type="slider"></div>',
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        let content = [];
        let value = '<div class="preview-slider">' +
          '<div class="preview-slider-left flex-center-center"><span class="fa fa-angle-left"></span></div>' +
          '<ul class="preview-slider-body">';

        $parent.find('.slider-images-list li').each(function () {
          const uid = $(this).attr('data-uid');
          let data = (typeof $(this).attr('data-info') !== 'undefined')
            ? JSON.parse($(this).attr('data-info'))
            : {
              title: '',
              caption: '',
              description: ''
            };

          const $slide = $parent.find('.slider-preview li[data-uid="' + uid + '"] img');
          if (typeof $slide.attr('data-upload') !== 'undefined') {
            //Uploaded
            data.src = $slide.attr('src');
            data.name = $slide.attr('alt');
          } else {
            //External
            data.src = $slide.attr('alt');
            data.type = 'image';
          }

          value += '<li class="' + ($(this).index() === 0 ? 'active' : '') + '">' +
            '<div class="flex-center-center"><img src="' + data.src + '" alt="">';
          if (data.title || data.caption || data.description) {
            value += '<div class="preview-slider-info">';
            value += (data.title) ? '<p>' + data.title + '</p>' : '';
            value += (data.caption) ? '<p>' + data.caption + '</p>' : '';
            value += (data.description) ? '<p>' + data.description + '</p>' : '';
            value += '</div>';
          }
          value += '</div></li>';

          content.push(data);
        });

        $gridElem.attr('data-content', JSON.stringify({content: content}));

        return value + '</ul>' +
          '<div class="preview-slider-right flex-center-center"><span class="fa fa-angle-right"></span></div>' +
          '</div>';
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        App.modules.file.replace($popup.find('.fileUpload'), value);
      }
    },
    string: {
      /**
       * String content element
       * @param {string} placeholder
       * @returns {string}
       */
      popupTag: placeholder => '<input name="blockValue" class="input-text col-4-5" placeholder="' + placeholder + '&hellip;">',
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        const value = $parent.find('[name="blockValue"]').val().trim();
        $gridElem.attr('data-content', JSON.stringify({content: value}));
        return value;
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => $popup.find('[name="blockValue"]').val(value),
    },
    table: {
      /**
       * String content element
       * @returns {string}
       */
      popupTag: () => '<div class="flex-center-center overflow-box">' +
        '<table class="table-container"><thead><tr>' +
        Templates.staticPages.elements.table.thCell() +
        '<th class="flex-center-center">' +
        '<button class="add-button" type="button"><span class="fa fa-plus-circle"></span></button>' +
        '</th></tr></thead><tbody><tr>' +
        Templates.staticPages.elements.table.tdCell() +
        '<td></td></tr><tr><td colspan="2" class="tac">' +
        '<button class="add-button" type="button"><span class="fa fa-plus-circle"></span></button>' +
        '</td></tr></tbody></table></div>',
      /**
       * Get value from popup
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        let content = {
          head: [],
          body: []
        };
        let value = '<table><thead><tr>';

        $parent.find('.table-container thead th').each(function () {
          if ($(this).find('input[name="tableContainerHead"]').length) {
            const val = $(this).find('input[name="tableContainerHead"]').val().trim();
            content.head.push(val);
            value += '<th>' + val + '</th>';
          }
        });

        value += '</tr></thead><tbody>';

        const rowsCount = $parent.find('.table-container tbody tr').length - 1;
        let i = 0;
        $parent.find('.table-container tbody tr').each(function () {
          if (i < rowsCount) {
            value += '<tr>';
            let rowData = [];
            $(this).find('td').each(function () {
              if ($(this).find('input[name="tableContainerCell"]').length) {
                const val = $(this).find('input[name="tableContainerCell"]').val().trim();
                rowData.push(val);
                value += '<td>' + val + '</td>';
              }
            });
            content.body.push(rowData);
            value += '</tr>';
          }
          i++;
        });

        $gridElem.attr('data-content', JSON.stringify({content: content}));

        return value + '</tbody></table>';
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        const rowsCount = value.body.length - 1;
        const colsCount = value.head.length - 1;

        let rowBody = Templates.staticPages.elements.table.tdCell();

        for (let i = 0; i < colsCount; i++) {
          $popup.find('.table-container thead th:last').before(Templates.staticPages.elements.table.thCell());
          $popup.find('.table-container tbody tr:first td:last').before(Templates.staticPages.elements.table.tdCell());

          rowBody += Templates.staticPages.elements.table.tdCell();
        }

        for (let i = 0; i < rowsCount; i++) {
          $popup.find('.table-container tbody tr:last').before('<tr>' + rowBody + '</tr>')
        }

        $popup.find('.table-container tbody tr:last td').attr('colspan', colsCount + 1);

        $popup.find('.table-container thead th').each(function () {
          if ($(this).find('input[name="tableContainerHead"]').length) {
            $(this).find('input[name="tableContainerHead"]').val(value.head[$(this).index()]);
          }
        });

        $popup.find('.table-container tbody tr').each(function () {
          if ($(this).index() <= rowsCount) {
            const rowIndex = $(this).index();
            $(this).find('td').each(function () {
              if ($(this).index() <= colsCount) {
                $(this).find('input[name="tableContainerCell"]').val(value.body[rowIndex][$(this).index()]);
              }
            })
          }
        });
      },
      /**
       * Table head cell
       * @returns {string}
       */
      thCell: () => '<th><input name="tableContainerHead" class="col-1" placeholder="Enter column caption&hellip;"></th>',
      /**
       * Table body cell
       * @returns {string}
       */
      tdCell: () => '<td><input name="tableContainerCell" class="col-1" placeholder="Enter cell value&hellip;"></td>'
    },
    tabs: {
      /**
       * Text content element
       * @returns {string}
       */
      popupTag: () => '<div class="tabs-container">' +
        '<ul class="tabs-container-list">' +
        Templates.staticPages.elements.tabs.li +
        '<li><div class="flex-center-center">' +
        '<button class="add-button" type="button"><span class="fa fa-plus-circle"></span></button>' +
        '</div></li>' +
        '</ul>' +
        '<p class="tabs-container-caption">Content of tab <span>#1</span>:</p>' +
        '<div class="tabs-container-texts-wrap">' +
        Templates.staticPages.elements.tabs.text +
        '</div>' +
        '</div>',
      /**
       *
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        let content = [];
        let value = '<table>';
        $parent.find('.tabs-container-list li').each(function () {
          if (!$(this).find('.add-button').length) {
            const index = $(this).index();
            const temp = {
              caption: $(this).find('input[name="tabContainerCaption"]').val().trim(),
              text: $(this).closest('.tabs-container').find('.tabs-container-texts-wrap .tabs-container-text:eq(' + index + ') textarea[name="tabContainerText"]').val().trim()
            };

            value += '<tr><td>' + temp.caption + ':</td><td>' + temp.text + '</td></tr>';
            content.push(temp);
          }
        });

        $gridElem.attr('data-content', JSON.stringify({content: content}));

        return value + '</table>';
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        const tabsCount = value.length - 1;

        for (let i = 0; i < tabsCount; i++) {
          $popup.find('.tabs-container-list li:last').before(Templates.staticPages.elements.tabs.li);
          $popup.find('.tabs-container-texts-wrap').append(Templates.staticPages.elements.tabs.text);
        }

        for (let i = 0; i < value.length; i++) {
          $popup.find('.tabs-container-list li:eq(' + i + ') input[name="tabContainerCaption"]').val(value[i].caption);
          $popup.find('.tabs-container-texts-wrap .tabs-container-text:eq(' + i + ') textarea[name="tabContainerText"]').val(value[i].text)
        }

        $popup.find('.tabs-container-list li').removeClass('active');
        $popup.find('.tabs-container-list li:first').addClass('active');
        $popup.find('.tabs-container-texts-wrap .tabs-container-text').removeClass('active');
        $popup.find('.tabs-container-texts-wrap .tabs-container-text:first').addClass('active');
      },
      /**
       * Li element
       */
      li: '<li><input class="input-text-small" name="tabContainerCaption" placeholder="Tab caption&hellip;"></li>',
      /**
       * Text element
       */
      text: '<div class="tabs-container-text"><textarea name="tabContainerText"></textarea></div>'
    },
    text: {
      /**
       * Text content element
       * @returns {string}
       */
      popupTag: () => '<textarea class="textarea-block col-1" name="blockValue"></textarea>'
    },
    textarea: {
      /**
       * String content element
       * @returns {string}
       */
      popupTag: () => '<div class="form-item-container">' +
        '<div class="row">' +
        '<p>Textarea name:</p>' +
        '<input name="textareaContainerName" class="input-text col-1">' +
        '</div>' +
        '<div class="row">' +
        '<p>Textarea placeholder text:</p>' +
        '<input name="textareaContainerPlaceholder" class="input-text col-1">' +
        '</div></div>',
      /**
       *
       * @param $parent
       * @param $gridElem
       * @returns {string}
       */
      getValue: ($parent, $gridElem) => {
        const value = {
          name: App.common.transliteration.str2url($parent.find('input[name="textareaContainerName"]').val().trim()),
          holder: $parent.find('input[name="textareaContainerPlaceholder"]').val().trim()
        };
        $gridElem.attr('data-content', JSON.stringify({content: value}));

        return '<textarea class="textarea-container col-3-4" placeholder="' + value.holder + '"></textarea>';
      },
      /**
       * Set value to content popup
       * @param $popup
       * @param value
       */
      setValue: ($popup, value) => {
        $popup.find('input[name="textareaContainerName"]').val(value.name);
        $popup.find('input[name="textareaContainerPlaceholder"]').val(value.holder);
      }
    }
  },
  /**
   * Popup content resolver
   * @param type
   * @returns {string}
   */
  popup: type => {
    let content = '';

    switch (type) {
      case 'htmlEditor':
        content = Templates.staticPages.elements.htmlEditor.popupTag();
        setTimeout(function () {
          $(document).find('.wysiwyg').each(function () {
            const editorId = $(this).attr('id');
            if (typeof CKEDITOR.instances[editorId] === 'undefined') {
              CKEDITOR.replace(editorId);
            }
          });
        }, 100);
        break;
      case 'list':
      case 'radio':
        content = Templates.staticPages.elements.list.popupTag();
        break;
      case 'string':
        content = Templates.staticPages.elements.string.popupTag('Enter string text');
        break;
      default:
        content = Templates.staticPages.elements[type].popupTag();
    }

    return content;
  },
};