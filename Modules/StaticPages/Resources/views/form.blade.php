@extends('admin.layouts.master')

@section('content')
    @include('admin.layouts.breadcrumbs')

    <div class="page-title">{{ $title }}</div>

    <form class="form-wrap flex-stretch-start"
          data-edit="{{ route('admin.users.edit', 0) }}"
          method="@isset($model){{ 'PUT' }}@else{{ 'POST' }}@endisset"
          action="@isset($model){{ route('api.static_pages.update', $model->id) }}@else{{ route('api.static_pages.store') }}@endisset">

        <div class="form-details active">
            <fieldset>
                <legend>Main data</legend>

                <div class="row">
                    <label>
                        <div class="caption">Title:</div>
                        <input name="title"
                               class="input-text col-1-2"
                               type="text"
                               value="{{ $model->title ?? '' }}"
                               placeholder="Title&hellip;">
                    </label>
                </div>

                <div class="row">
                    <label>
                        <div class="caption">Slug:</div>
                        <input name="slug"
                               class="input-text col-1-2"
                               type="email"
                               value="{{ $model->slug ?? '' }}"
                               placeholder="Slug&hellip;">
                    </label>
                </div>
            </fieldset>

            <fieldset>
                <legend>Content constructor</legend>

                <div class="constructor-wrap flex-stretch-start">
                    <div class="constructor-list-wrap col-1-5">
                        <ul class="construction-elements-list" id="test">
                            <li data-type="formStart">Form start</li>
                            <li data-type="formEnd">Form end</li>
                            <li data-type="tabs">Tabs</li>
                            <li data-type="heading">Heading</li>
                            <li data-type="string">String</li>
                            <li data-type="text">Text</li>
                            <li data-type="htmlEditor">HTML Editor</li>
                            <li data-type="list">List</li>
                            <li data-type="checkbox">Checkbox</li>
                            <li data-type="radio">Radio</li>
                            <li data-type="select">Select</li>
                            <li data-type="input">Input</li>
                            <li data-type="textarea">Textarea</li>
                            <li data-type="table">Table</li>
                            <li data-type="image">Image</li>
                            <li data-type="singleFile">File</li>
                            <li data-type="multipleFiles">Multiple files</li>
                            <li data-type="imageGallery">Image Gallery</li>
                            <li data-type="slider">Slider</li>
                            <li data-type="audioList">Audio List</li>
                            <li data-type="video">Video</li>
                            <li data-type="videoGallery">Video Gallery</li>
                        </ul>
                    </div>

                    <div class="constructor-grid-wrap col-3-4">
                        <div class="grid-controls">
                            <button class="grid-button text-green" name="addRow" type="button" title="Add row">
                                <span class="fa fa-plus-circle"></span>
                            </button>
                        </div>

                        <div class="grid">
                            <div class="control-row">
                                <ul class="grid-content-wrap flex-center-around">
                                    <li class="grid-content-hint">Drag content element here&hellip;</li>
                                </ul>
                                <div class="row-controls">
                                    <button class="grid-button text-red" name="removeRow" type="button" title="Remove row">
                                        <span class="fa fa-minus-circle"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>

        <aside class="form-assistant-wrap">
            <ul class="page-navigation"></ul>

            @if (isset($model))
                <div class="etc-info">
                    <span>Created at:&nbsp;</span>
                    <strong>{{ $model->created_at->format('d/M/Y H:i') }}</strong>
                </div>
                <div class="etc-info">
                    <span>Last update:&nbsp;</span>
                    <strong>{{ $model->updated_at->format('d/M/Y H:i') }}</strong>
                </div>
            @endif

            <div class="save-wrap">
                <button name="save" class="button green" type="submit" title="Save user data">Save</button>
            </div>
        </aside>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/admin/sortable.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/admin/scripts/staticpages.min.js') }}" type="text/javascript"></script>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/admin/content-constructor.css') }}">
@endsection

@section('popups')
    <div class="page-constructor-popup popup-wrap">
        <div class="close">
            <span class="fa fa-times"></span>
        </div>

        <div class="page-constructor-wrap">
            <div class="title">
                Edit section "<span></span>"
            </div>

            <div class="page-constructor-content"></div>
        </div>

        <div class="button-wrap">
            <button class="button green" name="applySection" type="button">Ok</button>
        </div>
    </div>
@endsection