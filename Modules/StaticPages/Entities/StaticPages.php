<?php

namespace Modules\StaticPages\Entities;

use Illuminate\Database\Eloquent\Model;

class StaticPages extends Model
{
    /**
     * Model table
     * @var string
     */
    protected $table = 'static_pages';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    public $fillable = [
        'title',
        'slug',
        'add_to_sitemap'
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'add_to_sitemap' => 'boolean',
    ];

    /**
     * Related content
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function content()
    {
        return $this->hasMany(Content::class, 'static_page_id', 'id')
            ->orderBy('v_pos', 'asc')
            ->orderBy('h_pos', 'asc');
    }
}