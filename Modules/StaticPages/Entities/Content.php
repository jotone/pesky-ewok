<?php

namespace Modules\StaticPages\Entities;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    /**
     * Model table
     * @var string
     */
    protected $table = 'static_page_content';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    public $fillable = [
        'static_page_id',
        'v_pos',
        'h_pos',
        'content',
        'type'
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'content' => 'array',
    ];

    /**
     * Related page
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function staticPage()
    {
        return $this->belongsTo(StaticPages::class, 'static_page_id', 'id');
    }
}