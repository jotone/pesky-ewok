<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticPageContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_page_content', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('static_page_id')->unsigned();
            $table->smallInteger('v_pos')->unsigned();
            $table->smallInteger('h_pos')->unsigned();
            $table->json('content');
            $table->string('type');
            $table->timestamps();
        });

        Schema::table('static_page_content', function (Blueprint $table) {
            $table->foreign('static_page_id')
                ->references('id')
                ->on('static_pages')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_page_content');
    }
}
