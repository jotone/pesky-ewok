<?php

return [
    'name' => 'StaticPages',
    'menu'           => [
        'title'      => 'Static Pages',
        'slug'       => 'static_pages',
        'route'      => 'static_pages',
        'image'      => 'fa-sticky-note-o',
        'position'   => 90,
        'is_section' => false,
        'editable'   => true,
    ],
    'permissions' => [
        'title' => 'Static Pages',
        'route' => 'static_pages'
    ]
];
