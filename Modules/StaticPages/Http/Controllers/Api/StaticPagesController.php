<?php

namespace Modules\StaticPages\Http\Controllers\Api;

use App\Source\Controllers\BasicApiController;
use Illuminate\Http\Request;
use Modules\StaticPages\Http\Controllers\StaticPagesControllerTrait;

class StaticPagesController extends BasicApiController
{
    use StaticPagesControllerTrait;

    /**
     * Get Users collection
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function index(Request $request)
    {
        return response($this->getCollection($request), 200);
    }
}
