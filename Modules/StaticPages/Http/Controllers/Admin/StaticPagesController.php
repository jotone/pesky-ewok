<?php

namespace Modules\StaticPages\Http\Controllers\Admin;

use App\Source\Controllers\BasicAdminController;
use Illuminate\Http\Request;
use Modules\StaticPages\Http\Controllers\StaticPagesControllerTrait;

class StaticPagesController extends BasicAdminController
{
    use StaticPagesControllerTrait;

    /**
     * Controller request properties
     * @var array
     */
    protected $requestProperties = [
        'select' => ['id', 'title', 'slug', 'created_at'],
        'with'   => ['content']
    ];

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index(Request $request)
    {
        return $this->renderIndex($request, [
            //Prevent some components appear
            'prevent'      => [
                'bulkDelete' => true
            ],
            //Page routes
            'routes'       => [
                'create' => route('admin.static_pages.create'),
                'edit'   => route('admin.static_pages.edit', 0),
                'list'   => route('api.static_pages.index'),
                'remove' => route('api.static_pages.destroy', 0)
            ],
            //Page title
            'title'        => 'Static pages list'
        ]);
    }

    /**
     * Show the form for creating a new user resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function create(Request $request)
    {
        return view($this->controllerSettings['module'] . '::form', [
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //Page title
            'title'       => 'Static page create'
        ]);
    }
}