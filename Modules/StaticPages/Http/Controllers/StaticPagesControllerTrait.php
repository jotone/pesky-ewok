<?php

namespace Modules\StaticPages\Http\Controllers;

use Modules\StaticPages\Entities\StaticPages;

trait StaticPagesControllerTrait
{
    /**
     * DataTable columns
     * @var array
     */
    protected $tableBuilder = [
        [
            'id'     => 'id',
            'filter' => 'ID',
            'min'    => 1,
            'name'   => '#',
            'type'   => 'complete'
        ], [
            'name' => '<input name="selectAll" type="checkbox" class="input-checkbox" autocomplete="off"><span class="fa fa-cogs"></span>',
            'type' => 'title'
        ], [
            'id'   => 'title',
            'min'  => 3,
            'name' => 'Title',
            'type' => 'complete'
        ], [
            'id'   => 'slug',
            'min'  => 3,
            'name' => 'Link',
            'type' => 'complete'
        ], [
            'id'   => 'static_pages.type',
            'min'  => 3,
            'name' => 'Content',
            'type' => 'search'
        ], [
            'id'   => 'created_at',
            'name' => 'Created At',
            'type' => 'directions'
        ]
    ];

    /**
     * Controller attributes
     * @var array
     */
    public $controllerSettings = [
        'model'  => StaticPages::class,
        'module' => 'staticpages',
        'table'  => 'static_pages',
    ];
}