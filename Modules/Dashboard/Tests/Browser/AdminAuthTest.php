<?php

namespace Modules\Dashboard\Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\TestCases\DuskTestCaseSingleton;

class AdminAuthTest extends DuskTestCaseSingleton
{
    /**
     * Test Admin authentication page
     * @throws \Throwable
     */
    public function testBrowserAuthorization()
    {
        $function = __FUNCTION__;
        self::getInstance()->browse(function (Browser $browser) use ($function) {
            $this->wrap($function, function () use ($browser) {
                $browser->driver->manage()->deleteAllCookies();

                //Check middleware
                $this->providePermissions(['dashboard' => '0']);

                $browser->visitRoute('admin.auth.index')
                    ->waitForText('Email', 15)
                    ->assertSee('Email')
                    ->click('button[type="submit"]')
                    ->assertRouteIs('admin.auth.index')
                    ->type('input[name="email"]', self::$user->email)
                    ->type('input[name="password"]', env('ADMIN_PSW'))
                    ->click('button[type="submit"]')
                    ->waitForText('403', 15)
                    ->assertSee('403');

                //Check auth
                $this->providePermissions(['dashboard' => '8']);
                $browser->visitRoute('admin.auth.index')
                    ->assertRouteIs('admin.auth.index')
                    ->type('input[name="email"]', self::$user->email)
                    ->type('input[name="password"]', env('ADMIN_PSW'))
                    ->click('button[type="submit"]')
                    ->waitForText('Welcome', 15)
                    ->assertSee('Welcome')
                    ->assertRouteIs('admin.dashboard.index')
                    ->assertAuthenticated()
                    ->assertAuthenticatedAs(self::$user);
            });
        });
    }
}