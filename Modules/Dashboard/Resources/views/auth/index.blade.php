<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <title>Welcome to Pesky Ewok</title>
</head>
<body style="padding: 0; margin: 0;">

<style>
    .wrap {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        position: relative;
        width: 100%;
    }
    .form {
        box-sizing: border-box;
        display: block;
        margin: 0 auto;
        padding: 0 10px;
        position: relative;
        width: 320px;
    }
    .btn-container {
        margin-top: 15px;
        position: relative;
        width: 100%;
    }
    label {
        box-sizing: border-box;
        display: block;
        font-size: 14px;
        padding: 10px 0;
        width: 100%;
    }
    .title {
        color: #222222;
        display: block;
        font-family: Raleway, sans-serif;
        line-height: 32px;
    }
    input {
        border: 1px solid #eeeeee;
        border-radius: 0;
        box-sizing: border-box;
        font-family: Raleway, sans-serif;
        line-height: 20px;
        padding: 10px 15px;
        width: 100%;
    }
    input:focus { border: 1px solid #cccccc; }
    button[type=submit] {
        background-color: #ffffff;
        border: 1px solid #cccccc;
        box-sizing: border-box;
        color: #222222;
        cursor: pointer;
        display: block;
        font-family: Raleway, sans-serif;
        margin: 0 auto;
        padding: 10px;
        width: 50%;
    }
    button[type=submit]:hover { background-color: #eeeeee; }
    .errors-wrap {
        padding: 10px;
        line-height: 16px;
        font-family: Raleway, sans-serif;
        font-size: 12px;
        color: #e00;
    }
</style>

<div class="wrap">
    <form class="form" method="POST" action="{{ route('admin.auth.login') }}">
        {{ csrf_field() }}
        <label>
            <span class="title">Email</span>
            <input name="email" type="email" placeholder="Enter email here&hellip;" required>
        </label>

        <label>
            <span class="title">Password</span>
            <input name="password" type="password" placeholder="Enter password here&hellip;" required>
        </label>

        @if (isset($errors) && !empty($errors->all()))
        <div class="errors-wrap">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
        @endif
        <div class="btn-container">
            <button type="submit">YARRR</button>
        </div>
    </form>
</div>
</body>
</html>
