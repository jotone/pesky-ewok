<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'as'         => 'admin.',
    'namespace'  => 'Admin',
    'prefix'     => '/admin',
], function () {
    Route::get('/login', 'AuthController@index')->name('auth.index');
    Route::post('/auth/login', 'AuthController@login')->name('auth.login');

    Route::get('/auth/user', 'AuthController@user')->name('auth.user');

    Route::group([
        'middleware' => 'admin',
    ], function () {
        Route::get('/', 'DashboardController@index')->name('dashboard.index');
    });
});

Route::any('logout', 'Admin\AuthController@logout')->name('auth.logout');