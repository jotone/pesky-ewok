<?php

namespace Modules\Dashboard\Entities;

use Illuminate\Database\Eloquent\Model;

class AdminMenu extends Model
{
    /**
     * Model table
     * @var string
     */
    protected $table = 'admin_menu';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    public $fillable = [
        'title',
        'slug',
        'route',
        'image',
        'parent',
        'position',
        'is_section',
        'editable',
    ];

    /**
     * Get menu inheritors
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subMenu()
    {
        return $this->hasMany(self::class, 'parent', 'id');
    }

    /**
     * Get menu inheritors cascade
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subMenus()
    {
        return $this->subMenu()->with('subMenus');
    }
}
