<?php

return [
    'name'        => 'Dashboard',
    'menu'        => [
        'title'      => 'Dashboard',
        'slug'       => 'dashboard',
        'route'      => 'dashboard',
        'image'      => 'fa-home',
        'position'   => 0,
        'is_section' => false,
        'editable'   => false,
        'inner'      => null
    ],
    'permissions' => [
        'title' => 'Dashboard Page',
        'route' => 'dashboard',
    ]
];
