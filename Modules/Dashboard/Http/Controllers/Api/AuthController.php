<?php

namespace Modules\Dashboard\Http\Controllers\Api;

use App\Source\Controllers\BasicApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Hash, Validator};
use Modules\User\Entities\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends BasicApiController
{
    /**
     * API Authorization
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $args = $request->only('email', 'password');

        $validator = Validator::make($args, [
            'email'    => 'required|email|exists:users,email',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 400);
        }

        $user = User::where('email', $args['email'])->first();

        if (Hash::check($args['password'], $user->password)) {
            $token = JWTAuth::fromUser($user);

            return response([
                'jwt'  => $token,
                'data' => $user,
                'ttl'  => auth('api')->factory()->getTTL() * 60,
            ]);
        }

        return response([
            'errors' => [
                'Authorization failed.'
            ]
        ], 401);
    }


    /**
     * API Logout. Destroying session and invalidating token
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        //Get JWT Token from the request header key "Authorization"
        $token = preg_replace('/Bearer\s/', '', $request->header('Authorization'));

        //Invalidate the token
        try {
            JWTAuth::invalidate($token);
            return response([], 204);
        } catch (JWTException $e) {
            return response([
                'errors' => [
                    'Failed to logout, please try again.'
                ],
            ], 500);
        }
    }

    /**
     * Refresh token
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response'
     */
    public function refresh()
    {
        return response([
            'jwt' => JWTAuth::fromUser(auth()->user()),
            'ttl' => auth('api')->factory()->getTTL() * 60,
        ]);
    }

    /**
     * Get authorized user data
     * @return \Illuminate\Http\JsonResponse
     */
    public function user()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json($user);
    }
}