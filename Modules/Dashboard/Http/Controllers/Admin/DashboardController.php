<?php

namespace Modules\Dashboard\Http\Controllers\Admin;

use App\Source\Controllers\BasicAdminController;
use Illuminate\Http\Request;

class DashboardController extends BasicAdminController
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index(Request $request)
    {
        return view('dashboard::admin.index', [
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //page title
            'title'       => 'Home page'
        ]);
    }
}
