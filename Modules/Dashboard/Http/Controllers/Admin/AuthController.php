<?php

namespace Modules\Dashboard\Http\Controllers\Admin;

use App\Source\Controllers\BasicController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Cookie, Hash, Redirect, Validator};
use Modules\User\Entities\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends BasicController
{
    /**
     * Render login page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index(Request $request)
    {
        return view('dashboard::auth.index');
    }

    /**
     * Run Authorization
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(Request $request)
    {
        $args = $request->only('email', 'password');

        //Run validation
        $validator = Validator::make($args, [
            'email'    => 'required|email|exists:users,email',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors([
                'validation' => $validator->errors()->all()
            ]);
        }

        $user = User::where('email', $args['email'])->first();

        if (Hash::check($args['password'], $user->password)) {
            Auth::loginUsingId($user->id);
            $token = JWTAuth::fromUser($user);

            Cookie::queue('jwt-token', $token, 525600);

            return (!Auth::check())
                ? Redirect::back()->withErrors([
                    'validation' => 'Cannot create session.'
                ])
                : redirect(route('admin.dashboard.index'));
        } else {
            abort(401);
        }
    }

    /**
     * Invalidate authorization
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        Auth::logout();

        return redirect(route('admin.auth.index'));
    }
}