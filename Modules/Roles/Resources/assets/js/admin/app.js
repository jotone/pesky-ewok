/**
 * Get items list
 * @param collection
 * @returns {string}
 */
App.getList = collection => {
  let body = '';
  for (let i in collection) {
    body += Templates.roles.render(collection[i]);
  }
  return body;
};

/**
 * Submit form
 */
App.form.submit = () => {
  let permissions = {};
  $('table#permissions tbody tr').each(function () {
    let temp = ($(this).find('input[name="read"]').prop('checked'))
        ? '1'
        : '0';
    temp += ($(this).find('input[name="create"]').prop('checked'))
        ? '1'
        : '0';
    temp += ($(this).find('input[name="update"]').prop('checked'))
        ? '1'
        : '0';
    temp += ($(this).find('input[name="delete"]').prop('checked'))
        ? '1'
        : '0';
    permissions[$(this).data('id')] = App.common.bin2hex(temp)
  });

  App.requests.save({
    title: $('input[name="title"]').val().trim(),
    slug: $('input[name="slug"]').val().trim(),
    permissions: permissions
  }).then(App.form.onSuccessSave);
};
/**
 * Form validation rules
 */
App.form.validation = {
  input: {
    title: ['required'],
    slug: ['required']
  }
};

/**
 * Module messages
 * @type {{removeQuestion: string, removed: string, titleField: string, success: string, removeConfirm: string}}
 */
App.modules.messages = {
  success: 'Role :attribute was successfully saved.',
  updated: 'Attribute ":attribute" wa applied to role.',
  removed: 'Role :attribute was successfully removed.',
  removeQuestion: 'Do you really want to remove role :attribute?',
  removeConfirm: 'Please enter role title to remove.',
  titleField: 'title'
};

Templates.roles = {
  /**
   * Render table row
   * @param item
   * @returns {string}
   */
  render: item => {
    let permissionHtml = '';
    const $permEdit = $('.items-list').data('permission');
    for (let i in item.permissions) {
      const value = App.common.hex2bin(item.permissions[i].pivot.value);

      permissionHtml += '<div class="inner-pseudo-table flex-center-between">' +
          '<a class="col-1-3" href="' + App.common.setIdToEditLink($permEdit, item.permissions[i].id) + '">' +
          item.permissions[i].title +
          '</a>' +
          '<span class="' + ((value[0] === '1')
              ? 'text-green'
              : 'text-red') + '">Read</span>' +
          '<span class="' + ((value[1] === '1')
              ? 'text-green'
              : 'text-red') + '">Create</span>' +
          '<span class="' + ((value[2] === '1')
              ? 'text-green'
              : 'text-red') + '">Update</span>' +
          '<span class="' + ((value[3] === '1')
              ? 'text-green'
              : 'text-red') + '">Delete</span>' +
          '</div>';
    }

    return Templates.itemsList.defaultTableWrap(item.id,
        '<td><span>' + (item.title || '') + '</span></td>' +
        '<td><span>' + item.slug + '</span></td>' +
        '<td>' + permissionHtml + '</td>' +
        '<td><span>' + item.created_at + '</span></td>');
  }
};