@extends('admin.layouts.master')

@section('content')
    @inject('helpers', App\Source\Helpers\CommonFunctions)
    @include('admin.layouts.breadcrumbs')

    <div class="page-title">{{ $title }}</div>

    <form class="form-wrap flex-stretch-start"
          data-edit="{{ route('admin.users.roles.edit', 0) }}"
          method="@isset($model){{ 'PUT' }}@else{{ 'POST' }}@endisset"
          action="@isset($model){{ route('api.users.roles.update', $model->id) }}@else{{ route('api.users.roles.store') }}@endisset">

        <div class="form-details active">
            <fieldset>
                <legend>Main data</legend>

                <div class="row">
                    <label>
                        <div class="caption">Title:</div>
                        <input name="title"
                               class="input-text col-1-2"
                               type="text"
                               value="{{ $model->title ?? '' }}"
                               placeholder="Title&hellip;">
                    </label>
                </div>

                <div class="row">
                    <label>
                        <div class="caption">Slug:</div>
                        <input name="slug"
                               class="input-text col-1-2"
                               type="text"
                               data-changed="0"
                               value="{{ $model->slug ?? '' }}"
                               placeholder="Slug&hellip;">
                    </label>
                </div>
            </fieldset>

            <fieldset>
                <legend>Permissions</legend>

                <div class="row">
                    <table class="regular-list" id="permissions">
                        <thead>
                        <tr>
                            <th>
                                <div class="column flex-center-center">Permission</div>
                            </th>
                            <th>
                                <div class="column flex-center-center">Read</div>
                            </th>
                            <th>
                                <div class="column flex-center-center">Create</div>
                            </th>
                            <th>
                                <div class="column flex-center-center">Update</div>
                            </th>
                            <th>
                                <div class="column flex-center-center">Delete</div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($permissions as $id => $title)
                            @php
                                $value = (isset($model))
                                    ? $helpers::hex2bin($model->permissions()->where('permissions.id', $id)->value('value') ?? '0')
                                    : '0000';
                            @endphp
                            <tr data-id="{{ $id }}">
                                <td style="vertical-align: middle">
                                    <span>{{ $title }}</span>
                                </td>
                                <td>
                                    <label>
                                        <input name="read"
                                               @if(isset($value) && $value[0] == '1') checked @endif
                                               type="checkbox">
                                    </label>
                                </td>
                                <td>
                                    <label>
                                        <input name="create"
                                               @if(isset($value) && $value[1] == '1') checked @endif
                                               type="checkbox">
                                    </label>
                                </td>
                                <td>
                                    <label>
                                        <input name="update"
                                               @if(isset($value) && $value[2] == '1') checked @endif
                                               type="checkbox">
                                    </label>
                                </td>
                                <td>
                                    <label>
                                        <input name="delete"
                                               @if(isset($value) && $value[3] == '1') checked @endif
                                               type="checkbox">
                                    </label>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>

        <aside class="form-assistant-wrap">
            <ul class="page-navigation"></ul>

            @if (isset($model))
                <div class="etc-info">
                    <span>Created at:&nbsp;</span>
                    <strong>{{ $model->created_at->format('d/M/Y H:i') }}</strong>
                </div>
                <div class="etc-info">
                    <span>Last update:&nbsp;</span>
                    <strong>{{ $model->updated_at->format('d/M/Y H:i') }}</strong>
                </div>
            @endif

            <div class="save-wrap">
                <button name="save" class="button green" type="submit" title="Save role data">Save</button>
            </div>
        </aside>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('js/admin/scripts/roles.min.js') }}" type="text/javascript"></script>
@endsection