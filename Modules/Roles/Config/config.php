<?php

return [
    'name'           => 'Roles',
    'menu'           => [
        'title'      => 'Roles',
        'slug'       => 'roles',
        'route'      => 'users.roles',
        'image'      => 'fa-users',
        'position'   => 20,
        'is_section' => false,
        'editable'   => true,
        'relate_to'  => 'users_permissions'
    ],
    'permissions'    => [
        'title' => 'Roles Page',
        'route' => 'users.roles'
    ],
    'default_values' => [
        'title' => 'Main Administrator',
        'slug'  => 'root_user',
    ]
];
