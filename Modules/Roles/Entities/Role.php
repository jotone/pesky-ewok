<?php

namespace Modules\Roles\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Permissions\Entities\Permission;
use Modules\User\Entities\User;

class Role extends Model
{
    /**
     * Model table
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    public $fillable = [
        'title',
        'slug',
    ];

    /**
     * Get role permissions
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permissions', 'role_id', 'permission_id')
            ->withPivot('value');
    }

    /**
     * Get role users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_roles', 'role_id', 'user_id');
    }

    /**
     * Model extended behavior
     */
    public static function boot()
    {
        parent::boot();

        static::deleted(function ($model) {
            //Remove user role relations
            $model->users()->detach();
        });
    }
}