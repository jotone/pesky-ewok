<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'as'         => 'api.users.',
    'middleware' => ['api', 'jwt.verify', 'admin'],
    'namespace'  => 'Api',
    'prefix'     => '/users'
], function () {
    Route::resource('/roles', 'RolesController')->except(['create', 'edit']);
});