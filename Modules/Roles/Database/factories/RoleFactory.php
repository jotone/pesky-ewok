<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Source\Helpers\TranslationHelper;
use Modules\Roles\Entities\Role;
use Faker\Generator as Faker;

$factory->define(Role::class, function (Faker $faker) {
    $title = 'test_role_' . uniqid();
    return [
        'title' => $title,
        'slug'  => TranslationHelper::str2url($title)
    ];
});
