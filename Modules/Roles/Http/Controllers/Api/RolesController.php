<?php

namespace Modules\Roles\Http\Controllers\Api;

use App\Source\Controllers\BasicApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Permissions\Entities\Permission;
use Modules\Roles\Http\Controllers\RoleControllerTrait;

class RolesController extends BasicApiController
{
    use RoleControllerTrait;

    /**
     * Get Roles collection
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function index(Request $request)
    {
        return response($this->getCollection($request), 200);
    }

    /**
     * Get Role by ID
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function show(int $id)
    {
        return $this->getModel($id);
    }

    /**
     * Create Role
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function store(Request $request)
    {
        $args = $request->only('title', 'slug', 'permissions');

        //Run validation
        $validator = Validator::make($args, [
            'title'       => 'required|string',
            'slug'        => 'required|string|unique:roles,slug',
            'permissions' => 'nullable|array'
        ]);

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $model = $this->storeModel($args);

        if (isset($args['permissions'])) {
            foreach($args['permissions'] as $id => $permission) {
                $model->permissions()->save(Permission::find($id), ['value' => $permission]);
            }
        }

        return ($model)
            ? response(['model' => $model], 201)
            : response([
                'error' => 'Cannot write to database.'
            ], 500);
    }

    /**
     * Update specified Role
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function update($id, Request $request)
    {
        $model = $this->controllerSettings['model']::find($id);

        if (!$model) {
            return response([], 404);
        }

        $args = $request->only('title', 'slug', 'permissions');

        $rules = [];
        foreach ($args as $key => $val) {
            switch ($key) {
                case 'title':
                    $rules[$key] = 'required|string';
                    $model->$key = $val;
                    break;
                case 'slug':
                    $rules[$key] = 'required|string';
                    if ($this->controllerSettings['model']::where('id', '!=', $id)->where('slug', $args['slug'])->count() > 0) {
                        return response([
                            'errors' => [
                                $this->message(__('validation.unique'), 'slug')
                            ]
                        ]);
                    }
                    $model->$key = $val;
                    break;
                case 'permissions':
                    foreach($args['permissions'] as $id => $permission) {
                        $model->permissions()->detach($id);
                        $model->permissions()->save(Permission::find($id), ['value' => $permission]);
                    }
                    break;
            }
        }

        //Run validation
        $validator = Validator::make($args, $rules);

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $model->save();

        return response([
            'model' => $model
        ]);
    }

    /**
     * Remove Role by ID
     * @param int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function destroy($id)
    {
        return $this->removeModel($id);
    }
}
