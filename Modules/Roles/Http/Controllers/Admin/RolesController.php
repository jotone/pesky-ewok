<?php

namespace Modules\Roles\Http\Controllers\Admin;

use App\Source\Controllers\BasicAdminController;
use Illuminate\Http\Request;
use Modules\Permissions\Entities\Permission;
use Modules\Roles\Http\Controllers\RoleControllerTrait;

class RolesController extends BasicAdminController
{
    use RoleControllerTrait;

    /**
     * Controller request properties
     * @var array
     */
    protected $requestProperties = [
        'select' => ['id', 'title', 'slug', 'created_at'],
        'with'   => ['permissions']
    ];

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index(Request $request)
    {
        return $this->renderIndex($request, [
            //Prevent some components appear
            'prevent' => [
                'bulkDelete' => true
            ],
            //Page routes
            'routes'  => [
                'create'     => route('admin.users.roles.create'),
                'edit'       => route('admin.users.roles.edit', 0),
                'list'       => route('api.users.roles.index'),
                'remove'     => route('api.users.roles.destroy', 0),
                'permission' => route('admin.users.permissions.edit', 0)
            ],
            //Page title
            'title'   => 'Roles list'
        ]);
    }

    /**
     * Show the form for creating a new role resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function create(Request $request)
    {
        return view($this->controllerSettings['module'] . '::form', [
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //Role permissions
            'permissions' => Permission::select('id', 'title')->orderBy('title', 'asc')->pluck('title', 'id'),
            //Page title
            'title'       => 'Role create'
        ]);
    }

    /**
     * Show the form for editing the specified role resource.
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function edit(int $id, Request $request)
    {
        $model = $this->controllerSettings['model']::find($id);

        if (!$model) {
            abort(404);
        }

        return view($this->controllerSettings['module'] . '::form', [
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //Editable model
            'model'       => $model,
            //Role permissions
            'permissions' => Permission::select('id', 'title')->orderBy('title', 'asc')->pluck('title', 'id'),
            //page title
            'title'       => 'Role edit'
        ]);
    }
}
