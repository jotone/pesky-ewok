<?php

namespace Modules\Roles\Http\Controllers;

use Modules\Roles\Entities\Role;

trait RoleControllerTrait
{
    /**
     * DataTable columns
     * @var array
     */
    protected $tableBuilder = [
        [
            'id'     => 'id',
            'filter' => 'ID',
            'min'    => 1,
            'name'   => '#',
            'type'   => 'complete'
        ], [
            'name' => '<input name="selectAll" type="checkbox" class="input-checkbox" autocomplete="off"><span class="fa fa-cogs"></span>',
            'type' => 'title'
        ], [
            'id'   => 'title',
            'min'  => 3,
            'name' => 'Title',
            'type' => 'complete'
        ], [
            'id'   => 'slug',
            'min'  => 3,
            'name' => 'Link',
            'type' => 'complete'
        ], [
            'name' => 'Permissions',
            'type' => 'title'
        ], [
            'id'   => 'created_at',
            'name' => 'Created At',
            'type' => 'directions'
        ]
    ];

    /**
     * Controller attributes
     * @var array
     */
    protected $controllerSettings = [
        'model'  => Role::class,
        'module' => 'roles',
        'table'  => 'roles',
    ];

    /**
     * Store model in database
     * @param $args
     * @return mixed
     */
    protected function storeModel($args)
    {
        return $this->controllerSettings['model']::create([
            'title' => $args['title'],
            'slug'  => $args['slug'],
        ]);
    }
}