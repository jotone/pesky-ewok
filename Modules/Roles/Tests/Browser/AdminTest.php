<?php

namespace Modules\Roles\Tests\Browser;

use Laravel\Dusk\Browser;
use Modules\Permissions\Entities\Permission;
use Modules\Roles\Entities\Role;
use Tests\TestCases\DuskTestCaseSingleton;

class AdminTest extends DuskTestCaseSingleton
{
    /**
     * Testable class
     * @var string
     */
    protected $class = Role::class;

    /**
     * Role "saved" message
     */
    protected $save_message = 'Role :attribute was successfully saved.';

    /**
     * Role remove question
     * @var string
     */
    protected $remove_question = 'Do you really want to remove role :attribute?';

    /**
     * Role remove confirmation
     * @var string
     */
    protected $remove_confirm = 'Please enter role title to remove.';

    /**
     * Test options
     * @var array
     */
    public static $options = [
        'test_field' => 'title',
        'text'       => [
            'create' => 'Role create',
            'edit'   => 'Role edit',
            'index'  => 'Roles list'
        ],
        'route'      => 'users.roles',
        'table'      => 'roles',
        'where'      => ''
    ];

    protected function setUp(): void
    {
        if (empty(self::$options['where'])) {
            self::$options['where'] = function ($q) {
                $q->where('title', 'like', 'test_role%');
            };
        }

        parent::setUp();

        if ($this->class::where(static::$options['where'])->count() < 25) {
            factory($this->class, 25)->create();
        }

        if (
            !empty(self::$user)
            && self::$user->settings()->where('key', 'take')->value('value') != '25'
        ) {
            \DB::table('settings')->where('user_id', self::$user->id)->where('key', 'take')->update(['value' => 25]);
        }
    }

    /**
     * Test roles index page
     */
    public function testBrowserRoleIndexPage()
    {
        $this->runIndex(__FUNCTION__);
    }

    /**
     * Test role create page
     */
    public function testBrowserRoleCreatePage()
    {
        $this->runCreate(__FUNCTION__, function (Browser $browser, $model, $first_model) {
            $browser->type('input[name="title"]', $model->title)
                ->type('input[name="slug"]', $model->slug)
                ->pause(50)
                ->click('button[name="save"]')
                ->waitForText($this->message($this->save_message, $model->title), 20)
                ->assertSee($this->message($this->save_message, $model->title))
                ->visitRoute('admin.users.roles.index')
                ->waitForText('Roles list', 20)
                ->assertSee('Roles list')
                ->waitForText($first_model->title, 20)
                ->assertSee($first_model->title)
                ->click('th#title .search-expand')
                ->pause(750)
                ->type('th#title .search-input', $model->title)
                ->waitForText($model->title, 20)
                ->assertSee($model->title);

            return $browser;
        });
    }

    /**
     * Test role update page
     */
    public function testBrowserRoleUpdatePage()
    {
        $this->runUpdate(__FUNCTION__, function ($browser, $model, $first_model) {
            $browser->type('input[name="title"]', $model->title)
                ->press('Save')
                ->visitRoute('admin.users.roles.index')
                ->waitForText('Roles list', 20)
                ->assertSee('Roles list')
                ->waitForText($first_model->title, 20)
                ->assertSee($first_model->title)
                ->click('th#title .search-expand')
                ->pause(750)
                ->type('th#title .search-input', $model->title)
                ->waitForText($model->title, 20)
                ->assertSee($model->title)
                ->assertSee(self::$model->slug);
        });
    }

    /**
     * Test role destroy page
     */
    public function testBrowserRoleDestroy()
    {
        $this->runDelete(__FUNCTION__, function (Browser $browser, $first_model) {
            $browser->click('.items-list tbody tr .actions .remove')
                ->assertDialogOpened($this->message($this->remove_question, self::$model->title))
                ->acceptDialog()
                ->pause(10)
                ->waitForDialog()
                ->assertDialogOpened($this->message($this->remove_confirm, self::$model->title))
                ->typeInDialog(self::$model->title)
                ->acceptDialog()
                ->waitForText('Roles list', 20)
                ->assertSee('Roles list')
                ->assertSee('Create')
                ->visitRoute('admin.users.roles.index')
                ->waitForText('Roles list', 20)
                ->assertSee('Roles list')
                ->waitForText($first_model->title, 20)
                ->assertSee($first_model->title)
                ->click('th#title .search-expand')
                ->pause(750)
                ->type('th#title .search-input', self::$model->title)
                ->assertDontSee(self::$model->title)
                ->assertDontSeeIn('.items-list tbody td:nth-child(1)', self::$model->id);
        });
    }

    /**
     * Run model search
     * @param Browser $browser
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     */
    protected function search(Browser $browser)
    {
        $browser->type('th#title .search-input', self::$model->title)
            ->waitForText(self::$model->id, 20)
            ->waitForText(self::$model->title, 20)
            ->assertSee(self::$model->title)
            ->assertSee(self::$model->slug)
            ->pause(750);
    }
}