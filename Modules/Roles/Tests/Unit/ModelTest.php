<?php

namespace Modules\Roles\Tests\Unit;

use Modules\Permissions\Entities\Permission;
use Modules\Roles\Entities\Role;
use Modules\User\Entities\User;
use Tests\TestCases\UnitTestCase;

class ModelTest extends UnitTestCase
{
    /**
     * Name of testing class
     * @var string
     */
    protected $class = Role::class;

    /**
     * Name of testing table
     * @var string
     */
    protected $table = 'roles';

    /**
     * Test \Modules\Roles\Entities\Role model creation
     */
    public function testRoleCreate()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $this->assertDatabaseHas($this->table, [
                'title' => self::$model->title,
                'slug'  => self::$model->slug,
            ]);
        });
    }

    /**
     * Test \Modules\Roles\Entities\Role update
     */
    public function testRoleUpdate()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $model = factory($this->class)->make();

            self::$model->title = $model->title;
            self::$model->slug = $model->slug;
            self::$model->save();

            $this->assertDatabaseHas($this->table, [
                'title' => self::$model->title,
                'slug'  => self::$model->slug,
            ]);
        });
    }

    /**
     * Test \Modules\Roles\Entities\Role model permission relation method
     */
    public function testRolePermissionRelation()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $permission = factory(Permission::class)->create();

            self::$model->permissions()->save($permission, ['value' => '0']);

            $this->assertDatabaseHas('role_permissions', [
                'permission_id' => $permission->id,
                'role_id'       => self::$model->id
            ])->assertTrue(in_array($permission->id, self::$model->permissions()->pluck('permissions.id')->toArray()));
        });
    }

    /**
     * Test \Modules\Roles\Entities\Role model user relation method
     */
    public function testRoleUserRelation()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $user = factory(User::class)->create();

            self::$model->users()->attach($user->id);

            $this->assertDatabaseHas('user_roles', [
                'user_id' => $user->id,
                'role_id' => self::$model->id
            ])->assertTrue(in_array($user->id, self::$model->users()->pluck('users.id')->toArray()));

            self::$model->users()->detach($user->id);

            $this->assertDatabaseMissing('user_roles', [
                'user_id' => $user->id,
                'role_id' => self::$model->id
            ]);
        });
    }

    /**
     * Test \Modules\Roles\Entities\Role model delete
     */
    public function testRoleDelete()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $this->runRemove('title', 'test_role_%');
        });
    }
}