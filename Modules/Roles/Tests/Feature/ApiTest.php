<?php

namespace Modules\Roles\Tests\Feature;

use Modules\Roles\Entities\Role;
use Tests\TestCases\ApiTestCase;

class ApiTest extends ApiTestCase
{
    /**
     * Tested model entity
     * @var string
     */
    protected $class = Role::class;

    /**
     * Tested model table
     * @var string
     */
    protected $table = 'roles';

    /**
     * Check if test data present in database
     */
    protected function checkDBMockData()
    {
        if (!$this->class::where('title', 'like', 'test_role_%')->count()) {
            factory($this->class, (int)env('ADMIN_SEEDS'))->create();
        }
    }

    /**
     * Test GET:api/users/roles without filter data
     */
    public function testRolesIndexMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            //Check middleware
            $this->providePermissions(['users.roles' => '0'])
                ->getJson(route('api.users.roles.index'), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            $response = $this->providePermissions(['users.roles' => '8'])
                ->getJson(route('api.users.roles.index'), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'collection',
                    'total'
                ]);

            $content = json_decode($response->content());

            $key = mt_rand(0, count($content->collection) - 1);

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->collection[$key]->id,
                'title' => $content->collection[$key]->title,
                'slug'  => $content->collection[$key]->slug
            ]);
        });
    }

    /**
     * Test GET:api/users/roles with filters
     */
    public function testRolesIndexFilters()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$model) {
                self::$model = factory($this->class)->create();
            }

            $response = $this->providePermissions(['users.roles' => '8'])
                ->getJson(route('api.users.roles.index', [
                    'order_by'     => 'id',
                    'order_dir'    => 'asc',
                    'search'       => ['title' => self::$model->title],
                    'select'       => ['id', 'title', 'slug'],
                ]), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'collection',
                    'total'
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->collection[0]->id,
                'title' => $content->collection[0]->title,
                'slug'  => $content->collection[0]->slug
            ]);
        });
    }

    /**
     * Test GET:api/users/roles/{id}
     */
    public function testRoleShowMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$model) {
                self::$model = factory($this->class)->create();
            }

            $response = $this->providePermissions(['users.roles' => '8'])
                ->getJson(route('api.users.roles.show', self::$model->id), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'title',
                        'slug',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->model->id,
                'title' => $content->model->title,
                'slug'  => $content->model->slug,
            ]);
        });
    }

    /**
     * Test POST:api/users/roles
     */
    public function testRoleStoreMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $model = factory($this->class)->make();

            //Check middleware
            $this->providePermissions(['users.roles' => '0'])
                ->post(route('api.users.roles.store'), [
                    'title' => $model->title,
                    'slug'  => $model->slug,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            $response = $this->providePermissions(['users.roles' => '4'])
                ->post(route('api.users.roles.store'), [
                    'title' => $model->title,
                    'slug'  => $model->slug,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertCreated()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'title',
                        'slug',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->model->id,
                'title' => $content->model->title,
                'slug'  => $content->model->slug,
            ]);
            self::$model = $this->class::find($content->model->id);
        });
    }

    /**
     * Test PATCH:api/users/roles/{id}
     */
    public function testRoleUpdateMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$model) {
                self::$model = factory($this->class)->create();
            }
            $model = factory($this->class)->make();

            //Check middleware
            $this->providePermissions(['users.roles' => '0'])
                ->patch(route('api.users.roles.update', self::$model->id), [
                    'title' => $model->title,
                    'slug'  => $model->slug,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            $response = $this->providePermissions(['users.roles' => '2'])
                ->patch(route('api.users.roles.update', self::$model->id), [
                    'title' => $model->title,
                    'slug'  => $model->slug,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'title',
                        'slug',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => self::$model->id,
                'title' => $model->title,
                'slug'  => $model->slug,
            ]);

            self::$model = $this->class::find($content->model->id);
        });
    }

    /**
     * Test DELETE:api/users/roles/{id}
     */
    public function testRoleDestroyMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $model_ids = $this->class::where('title', 'like', 'test_role_%')->pluck('id')->toArray();

            if (!$model_ids) {
                $model = factory($this->class)->create();
                $model_ids = [$model->id];
            }

            //Check middleware
            $this->providePermissions(['users.roles' => '0'])
                ->patch(route('api.users.roles.destroy', $model_ids[0]), [], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            foreach ($model_ids as $model_id) {
                $this->providePermissions(['users.roles' => '1'])
                    ->delete(route('api.users.roles.destroy', $model_id), [], [
                        'Authorization' => 'Bearer ' . self::$token
                    ])->assertNoContent();

                $this->assertDatabaseMissing($this->table, [
                    'id' => $model_id
                ]);
            }
        });
        self::$token = null;
        self::$model = null;
    }
}