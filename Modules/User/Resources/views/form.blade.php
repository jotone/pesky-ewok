@extends('admin.layouts.master')

@section('content')
    @include('admin.layouts.breadcrumbs')

    <div class="page-title">{{ $title }}</div>

    <form class="form-wrap flex-stretch-start"
          data-edit="{{ route('admin.users.edit', 0) }}"
          method="@isset($model){{ 'PUT' }}@else{{ 'POST' }}@endisset"
          action="@isset($model){{ route('api.users.update', $model->id) }}@else{{ route('api.users.store') }}@endisset">

        <div class="form-details active">
            <fieldset>
                <legend>Main data</legend>

                <div class="row">
                    <label>
                        <div class="caption">Name:</div>
                        <input name="name"
                               class="input-text col-1-2"
                               type="text"
                               value="{{ $model->name ?? '' }}"
                               placeholder="Name&hellip;">
                    </label>
                </div>

                <div class="row">
                    <label>
                        <div class="caption">Email:</div>
                        <input name="email"
                               class="input-text col-1-2"
                               type="email"
                               value="{{ $model->email ?? '' }}"
                               placeholder="Email&hellip;">
                    </label>
                </div>
            </fieldset>

            <fieldset>
                <legend>Password</legend>

                <div class="row">
                    <label>
                        <div class="caption">Password:</div>
                        <input name="password"
                               class="input-text col-1-2"
                               type="password"
                               placeholder="Password&hellip;">
                    </label>
                </div>

                <div class="row">
                    <label>
                        <div class="caption">Password confirmation:</div>
                        <input name="confirmation"
                               class="input-text col-1-2"
                               type="password"
                               placeholder="Password confirmation&hellip;">
                    </label>
                </div>
            </fieldset>

            <fieldset>
                <legend>Role</legend>

                <div class="row">
                    <label>
                        <div class="caption">Related role:</div>
                        @include('admin.layouts.dropdown-exclude-list', [
                            'list' => $roles,
                            'selected' => (isset($model))
                                ? $model->roles()->pluck('title', 'roles.id')->toArray()
                                : []
                         ])
                    </label>
                </div>
            </fieldset>

            <fieldset>
                <legend>Image</legend>

                <div class="fileUpload col-1"
                     data-type="image"
                     data-stub="{{ asset('img/admin/user-stub.png') }}"
                     data-file="@isset($model->files[0]){{ json_encode($model->files[0]) }}@endisset">
                </div>
            </fieldset>

            <fieldset>
                <legend>Other data</legend>

                <div class="row">
                    <label>
                        <div class="caption">About:</div>
                        <textarea name="about" class="textarea-block col-1">{{ $model->about ?? '' }}</textarea>
                    </label>
                </div>
            </fieldset>
        </div>

        <aside class="form-assistant-wrap">
            <ul class="page-navigation"></ul>

            @if (isset($model))
                <div class="etc-info">
                    <span>Created at:&nbsp;</span>
                    <strong>{{ $model->created_at->format('d/M/Y H:i') }}</strong>
                </div>
                <div class="etc-info">
                    <span>Last update:&nbsp;</span>
                    <strong>{{ $model->updated_at->format('d/M/Y H:i') }}</strong>
                </div>
            @endif

            <div class="save-wrap">
                <button name="save" class="button green" type="submit" title="Save user data">Save</button>
            </div>
        </aside>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('js/admin/scripts/user.min.js') }}" type="text/javascript"></script>
@endsection