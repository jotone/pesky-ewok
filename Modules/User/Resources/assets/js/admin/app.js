/**
 * Get items list
 * @param collection
 * @returns {string}
 */
App.getList = collection => {
  let body = '';
  for (let i in collection) {
    body += Templates.users.render(collection[i]);
  }
  return body;
};

App.modules.user = {
  init: () => {
    App.modules.file.replace('.fileUpload');
  },
  /**
   * Get form action url
   * @param {string} method
   * @param {int} id
   * @returns {*}
   */
  getUrl: (method, id) => ($('.form-wrap').attr('method') !== method)
      ? $('.form-wrap').attr('action') + '/' + id
      : $('.form-wrap').attr('action'),
  /**
   * @param $image
   * @param $parent
   * @param response
   * @param resolve
   * @param userData
   */
  resolveFileSaving: ($image, $parent, response, resolve, userData) => {
    if (response.length) {
      App.modules.user.storeMessages(response);
      const method = 'PUT';
      const url = App.modules.user.getUrl(method, userData.id);

      App.requests.save({img_url: response[0].data.url}, url, method).then(() => {
        App.modules.user.setUserImage($image, $parent, response[0].data);
        resolve(userData);
      });
    } else {
      resolve(userData);
    }
  },
  /**
   * Set uploaded image url
   * @param $image
   * @param $parent
   * @param data
   */
  setUserImage: ($image, $parent, data) => {
    $image.attr('src', data.url).attr('alt', data.url).removeAttr('data-upload');
    $parent.attr('data-id', data.id);
  },
  /**
   * Seve messages from response to local storage
   * @param response
   */
  storeMessages: response => {
    for (let i in response) {
      App.storage.addMessages([{
        [response[i].status]: (response[i].status === 'success')
            ? App.common.prepareMessage(App.modules.messages.fileSaved, App.common.basename(response[i].data.url))
            : response[i].data
      }]);
    }
  },
};
/**
 * Submit form
 */
App.form.submit = () => {
  let roles = [];
  $('.dropdown-selected-list li').each(function () {
    roles.push($(this).attr('data-id'));
  });
  const $image = $('.file-preview .file-wrap img');

  App.requests.save({
    name: $('input[name="name"]').val().trim(),
    email: $('input[name="email"]').val().trim(),
    password: $('input[name="password"]').val().trim(),
    confirmation: $('input[name="confirmation"]').val().trim(),
    about: $('textarea[name="about"]').val().trim(),
    img_url: ($image.attr('alt') !== 'stub')
        ? $image.attr('alt')
        : '',
    roles: roles
  }).then((response, status, request) => {
    const userData = response.model;
    const userSavingStatus = request.status;
    const $parent = $image.closest('.file-preview');

    const imageSave = new Promise(resolve => {
      if ($image.attr('alt') !== 'stub') {
        let data = {
          title: $parent.find('input[name="img_alt"]').val(),
          caption: $parent.find('input[name="img_caption"]').val(),
          description: $parent.find('textarea[name="img_description"]').val(),
        };
        //Check if image is uploaded
        if (typeof $image.attr('data-upload') !== 'undefined') {
          //Uploaded
          data.src = $image.attr('src');
          data.name = $image.attr('alt');
        } else {
          //External
          data.src = $image.attr('alt');
          data.type = 'image';
        }

        if (typeof $parent.attr('data-id') !== 'undefined') {
          App.modules.file.requests.update(data, $parent.data('id')).then(
              data => App.modules.user.resolveFileSaving($image, $parent, data, resolve, userData)
          );
        } else {
          App.modules.file.requests.create(data, 'users', response.model.id).then(
              data => App.modules.user.resolveFileSaving($image, $parent, data, resolve, userData)
          );
        }
      } else {
        if (typeof $parent.attr('data-id') !== 'undefined') {
          App.modules.file.requests.remove($parent.attr('data-id')).then(() => {
            const method = 'PUT';
            const url = App.modules.user.getUrl(method, userData.id);
            App.requests.save({img_url: ''}, url, method).then(() => resolve(userData));
          })
        } else {
          resolve(userData)
        }
      }
    });

    if (userSavingStatus === 201) {
      imageSave.then(userData => {
        window.location = App.common.setIdToEditLink($('.form-wrap').data('edit'), userData.id);
      });
    }
  });
};

/**
 * Form validation rules
 */
App.form.validation = {
  input: {
    email: ['required'],
    confirmation: ['same:input[name="password"]'],
  }
};

/**
 * Module messages
 * @type {{removeQuestion: string, removed: string, titleField: string, success: string, removeConfirm: string}}
 */
App.modules.messages = {
  fileSaved: 'File :attribute was successfully saved.',
  success: 'User ":attribute" was successfully saved.',
  updated: 'Attribute ":attribute" was applied to user.',
  removed: 'User ":attribute" was successfully removed.',
  removeQuestion: 'Do you really want to remove user ":attribute"?',
  removeConfirm: 'Please enter user\'s email to remove.',
  titleField: 'email'
};

Templates.users = {
  /**
   * Render table row
   * @param item
   * @returns {string}
   */
  render: item => {
    const edit = App.common.setIdToEditLink($('.items-list').data('edit'), item.id);
    const remove = App.common.setIdToRemoveLink($('.items-list').data('remove'), item.id);

    const img = (item.img_url !== null)
        ? '<img class="preview" src="' + item.img_url + '" alt="">'
        : '';

    let roles = '';
    for (let i in item.roles) {
      roles += '<li><a href="' + App.common.setIdToEditLink($('.items-list').data('roles'), item.roles[i].id) + '">' +
          item.roles[i].title +
          '</a></li>';
    }

    return '<tr><td><span>' + item.id + '</span></td>' +
        '<td><div class="actions flex-center-around">' +
        '<div><a class="fa fa-pencil edit" href="' + edit + '" title="Edit"></a></div>' +
        '<div><a class="fa fa-times remove" href="' + remove + '" title="Remove"></a></div>\n' +
        '</div></td>' +
        '<td><span>' + (item.name || '') + '</span></td>' +
        '<td><span>' + item.email + '</span></td>' +
        '<td>' + img + '</td>' +
        '<td><ul>' + roles + '</ul></td>' +
        '<td><span>' + item.last_seen_at + '</span></td>' +
        '<td><span>' + item.created_at + '</span></td>' +
        '</tr>';
  }
};