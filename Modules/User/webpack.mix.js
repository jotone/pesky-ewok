const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.combine(__dirname + '/Resources/assets/js/app.js', 'js/admin/user.js');

if (mix.inProduction()) {
    mix.version();
}