<?php

namespace Modules\User\Http\Controllers\Api;

use App\Source\Controllers\BasicApiController;
use App\Source\Helpers\FilesHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\User\Http\Controllers\UserControllerTrait;

class UserController extends BasicApiController
{
    use UserControllerTrait;

    /**
     * Get Users collection
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function index(Request $request)
    {
        return response($this->getCollection($request), 200);
    }

    /**
     * Get User by ID
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function show(int $id)
    {
        return $this->getModel($id);
    }

    /**
     * Create User
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function store(Request $request)
    {
        $args = $request->only('name', 'email', 'password', 'confirmation', 'about', 'img_url', 'roles');

        //Run validation
        $validator = Validator::make($args, [
            'name'         => 'nullable|string',
            'email'        => 'required|email|unique:users,email',
            'password'     => 'required|string|min:' . env('MIN_PSW_LEN'),
            'confirmation' => 'required|string|min:' . env('MIN_PSW_LEN') . '|same:password'
        ]);

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $model = $this->storeModel($args);

        //Set role to user
        if (isset($data['roles']) && !empty($data['roles'])) {
            foreach ($data['roles'] as $role_id) {
                $model->roles()->attach($role_id);
            }
        }

        return ($model)
            ? response(['model' => $model], 201)
            : response([
                'error' => 'Cannot write to database.'
            ], 500);
    }

    /**
     * Update specified User
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function update($id, Request $request)
    {
        $model = $this->controllerSettings['model']::find($id);

        if (!$model) {
            return response([], 404);
        }

        $args = $request->only('name', 'email', 'password', 'confirmation', 'about', 'img_url', 'roles');

        $rules = [];
        foreach ($args as $key => $val) {
            switch ($key) {
                case 'about':
                case 'img_url':
                case 'name':
                    $rules[$key] = 'nullable|string';
                    $model->$key = $val;
                    break;
                case 'email':
                    $rules[$key] = 'required|email';
                    //Check the email is unique
                    if ($this->controllerSettings['model']::where('id', '!=', $id)->where('email', $args['email'])->count() > 0) {
                        return response([
                            'errors' => [
                                $this->message(__('validation.unique'), 'email')
                            ]
                        ]);
                    }
                    $model->$key = $val;
                    break;
                case 'password':
                    $rules[$key] = 'nullable|string|min:' . env('MIN_PSW_LEN') . '|same:confirmation';
                    if ($val) {
                        $model->$key = bcrypt($val);
                    }
                    break;
                case 'confirmation':
                    $rules[$key] = 'nullable|string|min:' . env('MIN_PSW_LEN') . '|same:password';
                    break;
                case 'roles':
                    //Set role to user
                    $model->roles()->detach();
                    foreach ($val as $role_id) {
                        $model->roles()->attach($role_id);
                    }
                    break;
            }
        }

        //Run validation
        $validator = Validator::make($args, $rules);

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $model->save();

        return response([
            'model' => $model
        ]);
    }

    /**
     * Remove User by ID
     * @param int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function destroy($id)
    {
        return $this->removeModel($id);
    }
}