<?php

namespace Modules\User\Http\Controllers;

use Modules\User\Entities\User;

trait UserControllerTrait
{
    /**
     * DataTable columns
     * @var array
     */
    protected $tableBuilder = [
        [
            'id'     => 'id',
            'filter' => 'ID',
            'min'    => 1,
            'name'   => '#',
            'type'   => 'complete'
        ], [
            'name' => '<span class="fa fa-cogs"></span>',
            'type' => 'title'
        ], [
            'id'     => 'name',
            'min'    => 3,
            'name'   => 'Name',
            'type'   => 'complete'
        ], [
            'id'     => 'email',
            'min'    => 3,
            'name'   => 'Email',
            'type'   => 'complete'
        ], [
            'name' => 'Image',
            'type' => 'title'
        ], [
            'id'     => 'roles.title',
            'min'    => 3,
            'name'   => 'Roles',
            'type'   => 'search'
        ], [
            'id'   => 'last_seen_at',
            'name' => 'Last Seen',
            'type' => 'directions'
        ], [
            'id'   => 'created_at',
            'name' => 'Created At',
            'type' => 'directions'
        ]
    ];

    /**
     * Controller attributes
     * @var array
     */
    protected $controllerSettings = [
        'model'  => User::class,
        'module' => 'user',
        'table'  => 'users',
    ];

    /**
     * Store model in database
     * @param $args
     * @return mixed
     */
    protected function storeModel($args)
    {
        if (isset($args['img_url']) && is_array($args['img_url'])) {
            $args['img_url'] = $args['img_url'][0];
        }

        return $this->controllerSettings['model']::create([
            'name'     => $args['name'],
            'email'    => $args['email'],
            'password' => bcrypt($args['password']),
            'about'    => $args['about'] ?? null,
            'img_url'  => $args['img_url'] ?? null,
        ]);
    }
}