<?php

namespace Modules\User\Http\Controllers\Admin;

use App\Source\Controllers\BasicAdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Settings\Entities\Settings;
use Modules\User\Http\Controllers\UserControllerTrait;
use Modules\Roles\Entities\Role;

class UserController extends BasicAdminController
{
    use UserControllerTrait;

    /**
     * Controller request properties
     * @var array
     */
    protected $requestProperties = [
        'select' => ['id', 'name', 'email', 'img_url', 'last_seen_at', 'created_at'],
        'with'   => ['roles']
    ];

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index(Request $request)
    {
        return $this->renderIndex($request, [
            //Prevent some components appear
            'prevent'      => [
                'bulkDelete' => true
            ],
            //Page routes
            'routes'       => [
                'create' => route('admin.users.create'),
                'edit'   => route('admin.users.edit', 0),
                'list'   => route('api.users.index'),
                'remove' => route('api.users.destroy', 0),
                'roles'  => route('admin.users.roles.edit', 0)
            ],
            //Page title
            'title'        => 'Users list'
        ]);
    }

    /**
     * Show the form for creating a new user resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function create(Request $request)
    {
        return view($this->controllerSettings['module'] . '::form', [
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //Roles list
            'roles'       => Role::orderBy('title', 'asc')->pluck('title', 'id')->toArray(),
            //Page title
            'title'       => 'User create'
        ]);
    }

    /**
     * Show the form for editing the specified user resource.
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function edit(int $id, Request $request)
    {
        $model = $this->controllerSettings['model']::find($id);

        if (!$model) {
            abort(404);
        }

        return view($this->controllerSettings['module'] . '::form', [
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //Editable model
            'model'       => $model,
            //Roles list
            'roles'       => Role::orderBy('title', 'asc')->pluck('title', 'id')->toArray(),
            //Page title
            'title'       => 'User edit'
        ]);
    }
}
