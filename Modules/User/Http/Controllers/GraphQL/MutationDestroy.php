<?php

namespace Modules\User\Http\Controllers\GraphQL;

use Closure;
use Modules\User\Entities\User;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class MutationDestroy extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name'  => 'destroy',
        'model' => User::class
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return GraphQL::type('user');
    }

    /**
     * @return array
     */
    public function args(): array
    {
        return [
            'id' => [
                'name'  => 'id',
                'type'  => Type::nonNull(Type::int()),
                'rules' => ['required', 'integer', 'exists:users,id']
            ],
        ];
    }

    /**
     * Remove user
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $resolveInfo
     * @param Closure $getSelectFields
     * @return User
     */
    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $this->attributes['model']::destroy($args['id']);

        return null;
    }
}