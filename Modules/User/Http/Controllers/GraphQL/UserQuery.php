<?php

namespace Modules\User\Http\Controllers\GraphQL;

use App\Source\Controllers\GraphQlFilters;
use Modules\User\Entities\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class UserQuery extends Query
{
    use GraphQlFilters;

    /**
     * Class attributes
     * @var array
     */
    protected $attributes = [
        'name'  => 'Users query',
        'model' => User::class,
    ];

    /**
     * Attach class type
     * @return Type
     */
    public function type(): Type
    {
        return GraphQL::paginate('user');
    }

    /**
     * Set query arguments
     * @return array
     */
    public function args(): array
    {
        return array_merge($this->filterArgs(), [
            'email' => [
                'name' => 'email',
                'type' => Type::string()
            ]
        ]);
    }

    /**
     * Run query
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $resolveInfo
     * @param Closure $getSelectFields
     * @return User|mixed
     */
    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $this->buildFilters($args);

        $where = function ($query) use ($args) {
            if (isset($args['id'])) {
                $query->where('id', $args['id']);
            }

            if (isset($args['email'])) {
                $query->where('email', $args['email']);
            }
        };

        $fields = $getSelectFields()->getSelect();
        $relations = $getSelectFields()->getRelations();

        return $this->getCollection($where, $relations, $fields);
    }
}