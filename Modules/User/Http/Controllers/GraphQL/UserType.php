<?php

namespace Modules\User\Http\Controllers\GraphQL;

use Modules\User\Entities\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name'        => 'user',
        'description' => 'A user',
        'model'       => User::class,
    ];

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            'id'         => [
                'type'        => Type::nonNull(Type::int()),
                'description' => 'The id of the user',
            ],
            'name'       => [
                'type'        => Type::string(),
                'description' => 'The name of user'
            ],
            'email'      => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'The email of user',
            ],
            'about'      => [
                'type'        => Type::string(),
                'description' => 'The information about user'
            ],
            'img_url'    => [
                'type'        => Type::string(),
                'description' => 'User image'
            ],
            'created_at' => [
                'type'        => Type::string(),
                'description' => 'User date of creation'
            ],
            'updated_at' => [
                'type'        => Type::string(),
                'description' => 'User date of modification'
            ]
        ];
    }
}