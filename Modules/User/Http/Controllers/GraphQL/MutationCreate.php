<?php

namespace Modules\User\Http\Controllers\GraphQL;

use Closure;
use Modules\User\Entities\User;
use Modules\User\Http\Controllers\UserControllerTrait;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class MutationCreate extends Mutation
{
    use UserControllerTrait;

    /**
     * @var array
     */
    protected $attributes = [
        'name'  => 'create',
        'model' => User::class
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return GraphQL::type('user');
    }

    /**
     * @return array
     */
    public function args(): array
    {
        return [
            'email'        => [
                'name'  => 'email',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['required', 'email', 'unique:users,email']
            ],
            'password'     => [
                'name'  => 'password',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['required', 'string', 'min:' . env('MIN_PSW_LEN')]
            ],
            'confirmation' => [
                'name'  => 'confirmation',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['required', 'string', 'min:' . env('MIN_PSW_LEN'), 'same:password']
            ],
            'name'         => [
                'name'  => 'name',
                'type'  => Type::string(),
                'rules' => ['nullable', 'string']
            ],
            'about'        => [
                'name'  => 'about',
                'type'  => Type::string(),
                'rules' => ['nullable', 'string']
            ],
            'img_url'      => [
                'name'  => 'img_url',
                'type'  => Type::string(),
                'rules' => ['nullable', 'string']
            ]
        ];
    }

    /**
     * Run user store
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $resolveInfo
     * @param Closure $getSelectFields
     * @return mixed
     */
    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return $this->storeModel($args);
    }
}