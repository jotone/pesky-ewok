<?php

namespace Modules\User\Http\Controllers\GraphQL;

use Closure;
use Modules\User\Entities\User;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class MutationUpdatePassword extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name'  => 'updatePassword',
        'model' => User::class
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return GraphQL::type('user');
    }

    /**
     * @return array
     */
    public function args(): array
    {
        return [
            'id'           => [
                'name'  => 'id',
                'type'  => Type::nonNull(Type::int()),
                'rules' => ['required', 'integer', 'exists:users,id']
            ],
            'password'     => [
                'name'  => 'password',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['required', 'string', 'min:' . env('MIN_PSW_LEN'), 'same:confirmation']
            ],
            'confirmation' => [
                'name'  => 'confirmation',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['required', 'string', 'min:' . env('MIN_PSW_LEN'), 'same:password']
            ]
        ];
    }

    /**
     * Run user update
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $resolveInfo
     * @param Closure $getSelectFields
     * @return User
     */
    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $model = $this->attributes['model']::find($args['id']);
        $model->password = bcrypt($args['password']);
        $model->save();

        return $model;
    }
}