<?php

namespace Modules\User\Http\Controllers\GraphQL;

use Closure;
use Modules\User\Entities\User;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class MutationUpdate extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name'  => 'update',
        'model' => User::class
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return GraphQL::type('user');
    }

    /**
     * @return array
     */
    public function args(): array
    {
        return [
            'id'         => [
                'name'  => 'id',
                'type'  => Type::nonNull(Type::int()),
                'rules' => ['required', 'integer', 'exists:users,id']
            ],
            'email'      => [
                'name'  => 'email',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['required', 'email']
            ],
            'name'       => [
                'name'  => 'name',
                'type'  => Type::string(),
                'rules' => ['nullable', 'string']
            ],
            'about'      => [
                'name'  => 'about',
                'type'  => Type::string(),
                'rules' => ['nullable', 'string']
            ],
            'img_url'    => [
                'name'  => 'img_url',
                'type'  => Type::string(),
                'rules' => ['nullable', 'string']
            ]
        ];
    }

    /**
     * Run user update
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $resolveInfo
     * @param Closure $getSelectFields
     * @return User
     */
    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $model = $this->attributes['model']::find($args['id']);

        if (!$model) {
            return null;
        }

        foreach ($args as $key => $val) {
            switch ($key) {
                case 'about':
                case 'img_url':
                case 'name':
                    $model->$key = $val;
                    break;
                case 'email':
                    //Check the email is unique
                    if ($this->attributes['model']::where('id', '!=', $args['id'])->where('email', $args['email'])->count() > 0) {
                        return null;
                    }
                    $model->$key = $val;
                    break;
            }
        }

        $model->save();

        return $model;
    }
}