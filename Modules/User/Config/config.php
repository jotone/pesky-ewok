<?php

return [
    'name'        => 'User',
    'menu'        => [
        'title'      => 'Users and Permissions',
        'slug'       => 'users_permissions',
        'route'      => '#users',
        'image'      => 'fa-user-plus',
        'position'   => 100,
        'is_section' => true,
        'editable'   => true,
        'inner'      => [
            [
                'title'      => 'Users',
                'slug'       => 'users',
                'route'      => 'users',
                'image'      => 'fa-user',
                'position'   => 10,
                'is_section' => false,
                'editable'   => true,
                'inner'      => null
            ]
        ]
    ],
    'permissions' => [
        'title' => 'Users Page',
        'route' => 'users'
    ]
];
