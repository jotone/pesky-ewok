<?php

namespace Modules\User\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Files\Entities\File;
use Modules\Roles\Entities\Role;
use Modules\Settings\Entities\Settings;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'about',
        'img_url',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get user files
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(File::class, 'entity_id', 'id')
            ->where('entity_type', self::class);
    }

    /**
     * Get user roles
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    }

    /**
     * Get user settings
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function settings()
    {
        return $this->hasMany(Settings::class, 'user_id', 'id');
    }

    /**
     * @return string
     */
    public function getMorphClass()
    {
        return 'user';
    }

    /**
     * Model extended behavior
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->last_seen_at = now();
        });

        static::created(function ($model) {
            Settings::create([
                'user_id' => $model->id,
                'key'     => 'sidemenu',
                'value'   => 0,
                'type'    => 'bool',
                'global'  => false
            ]);

            Settings::create([
                'user_id' => $model->id,
                'key'     => 'take',
                'value'   => 25,
                'type'    => 'bool',
                'global'  => false
            ]);
        });

        static::updating(function ($model) {
            $model->last_seen_at = now();
        });

        static::deleting(function ($model) {
            //Remove image
            if (!empty($model->img_url) && file_exists(public_path($model->img_url))) {
                unlink(public_path($model->img_url));
            }
            //Remove roles relations
            $model->roles()->detach();
            //Remove admin settings relation
            $model->settings()->delete();
        });
    }
}