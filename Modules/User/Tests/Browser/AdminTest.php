<?php

namespace Modules\User\Tests\Browser;

use Laravel\Dusk\Browser;
use Modules\User\Entities\User;
use Tests\TestCases\DuskTestCaseSingleton;

class AdminTest extends DuskTestCaseSingleton
{
    /**
     * Testable class
     * @var string
     */
    protected $class = User::class;

    /**
     * User "saved" message
     */
    protected $save_message = 'User ":attribute" was successfully saved.';

    /**
     * User remove question
     * @var string
     */
    protected $remove_question = 'Do you really want to remove user ":attribute"?';

    /**
     * User remove confirmation
     * @var string
     */
    protected $remove_confirm = 'Please enter user\'s email to remove.';

    /**
     * Test options
     * @var array
     */
    public static $options = [
        'test_field' => 'email',
        'text'       => [
            'create' => 'User create',
            'edit'   => 'User edit',
            'index'  => 'Users list'
        ],
        'route'      => 'users',
        'table'      => 'users',
        'where'      => ''
    ];

    protected function setUp(): void
    {
        if (empty(self::$options['where'])) {
            self::$options['where'] = function ($q) {
                $q->where('remember_token', env('APP_KEY'));
            };
        }

        parent::setUp();

        if ($this->class::where(static::$options['where'])->count() < 25) {
            factory($this->class, 25)->create();
        }

        if (
            !empty(self::$user)
            && self::$user->settings()->where('key', 'take')->value('value') != '25'
        ) {
            \DB::table('settings')->where('user_id', self::$user->id)->where('key', 'take')->update(['value' => 25]);
        }
    }

    /**
     * Test user index page
     * @throws \Throwable
     */
    public function testBrowserUserIndexPage()
    {
        $this->runIndex(__FUNCTION__);
    }

    /**
     * Test user create page
     * @throws \Throwable
     */
    public function testBrowserUserCreatePage()
    {
        $this->runCreate(__FUNCTION__, function ($browser, $model, $first_model) {
            $browser->type('input[name="name"]', $model->name)
                ->type('input[name="email"]', $model->email)
                ->type('input[name="password"]', env('ADMIN_PSW'))
                ->type('input[name="confirmation"]', env('ADMIN_PSW'))
                ->pause(50)
                ->click('button[name="save"]')
                ->waitForText($this->message($this->save_message, $model->email), 20)
                ->assertSee($this->message($this->save_message, $model->email))
                ->visitRoute('admin.users.index')
                ->waitForText('Users list', 20)
                ->assertSee('Users list')
                ->waitForText($first_model->email, 20)
                ->assertSee($first_model->email)
                ->click('th#email .search-expand')
                ->pause(750)
                ->type('th#email .search-input', $model->email)
                ->waitForText($model->email, 20)
                ->assertSee($model->email);

            $user = $this->class::where('email', $model->email)->first();
            $user->remember_token = env('APP_KEY');
            $user->save();

            return $browser;
        });
    }

    /**
     * Test user update page
     * @throws \Throwable
     */
    public function testBrowserUserUpdatePage()
    {
        $this->runUpdate(__FUNCTION__, function ($browser, $model, $first_model) {
            $browser->type('input[name="name"]', $model->name)
                ->press('Save')
                ->visitRoute('admin.users.index')
                ->waitForText('Users list', 20)
                ->assertSee('Users list')
                ->waitForText($first_model->email, 20)
                ->assertSee($first_model->email)
                ->click('th#email .search-expand')
                ->pause(750)
                ->type('th#email .search-input', self::$model->email)
                ->waitForText($model->name, 20)
                ->assertSee($model->name)
                ->assertSee(self::$model->email);
        });
    }

    /**
     * Test remove user
     * @throws \Throwable
     */
    public function testBrowserUserDestroy()
    {
        $this->runDelete(__FUNCTION__, function (Browser $browser, $first_model) {
            $browser->click('.items-list tbody tr .actions .remove')
                ->assertDialogOpened($this->message($this->remove_question, self::$model->email))
                ->acceptDialog()
                ->pause(10)
                ->waitForDialog()
                ->assertDialogOpened($this->message($this->remove_confirm, self::$model->email))
                ->typeInDialog(self::$model->email)
                ->acceptDialog()
                ->waitForText('Users list', 20)
                ->assertSee('Users list')
                ->assertSee('Create')
                ->visitRoute('admin.users.index')
                ->waitForText('Users list', 20)
                ->assertSee('Users list')
                ->waitForText($first_model->email, 20)
                ->assertSee($first_model->email)
                ->click('th#email .search-expand')
                ->pause(750)
                ->type('th#email .search-input', self::$model->email)
                ->assertDontSee(self::$model->name)
                ->assertDontSeeIn('.items-list tbody td:nth-child(1)', self::$model->id);
        });
    }

    /**
     * Run model search
     * @param Browser $browser
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     */
    protected function search(Browser $browser)
    {
        $browser->type('th#email .search-input', self::$model->email)
            ->waitForText(self::$model->id, 20)
            ->waitForText(self::$model->name, 20)
            ->assertSee(self::$model->name)
            ->assertSee(self::$model->email)
            ->pause(750);
    }
}