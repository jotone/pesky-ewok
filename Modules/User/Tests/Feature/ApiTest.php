<?php

namespace Modules\User\Tests\Feature;

use Modules\User\Entities\User;
use Tests\TestCases\ApiTestCase;

class ApiTest extends ApiTestCase
{
    /**
     * Tested model entity
     * @var string
     */
    protected $class = User::class;

    /**
     * Tested model table
     * @var string
     */
    protected $table = 'users';

    /**
     * Check if test data present in database
     */
    protected function checkDBMockData()
    {
        if (!$this->class::where('remember_token', env('APP_KEY'))->count()) {
            factory($this->class, (int)env('ADMIN_SEEDS'))->create();
        }
    }

    /**
     * Test GET:api/users without filter data
     */
    public function testUsersIndexMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            //Check middleware
            $this->providePermissions(['users' => '0'])
                ->getJson(route('api.users.index'), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            $response = $this->providePermissions(['users' => '8'])
                ->getJson(route('api.users.index'), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'collection',
                    'total'
                ]);

            $content = json_decode($response->content());

            $key = mt_rand(0, count($content->collection) - 1);

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->collection[$key]->id,
                'email' => $content->collection[$key]->email
            ]);
        });
    }

    /**
     * Test GET:api/users with filters
     */
    public function testUsersIndexFilters()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $response = $this->providePermissions(['users' => '8'])
                ->getJson(route('api.users.index', [
                    'order_by'  => 'id',
                    'order_dir' => 'asc',
                    'search'    => ['email' => self::$user->email],
                    'select'    => ['id', 'email', 'name'],
                ]), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'collection',
                    'total'
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->collection[0]->id,
                'email' => $content->collection[0]->email,
                'name'  => $content->collection[0]->name
            ]);
        });
    }

    /**
     * Test GET:api/users/{id}
     */
    public function testUserShowMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $response = $this->providePermissions(['users' => '8'])
                ->getJson(route('api.users.show', self::$user->id), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'name',
                        'email',
                        'about',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->model->id,
                'name'  => $content->model->name,
                'email' => $content->model->email,
                'about' => $content->model->about,
            ]);
        });
    }

    /**
     * Test POST:api/users
     */
    public function testUserStoreMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $model = factory($this->class)->make();

            $response = $this->providePermissions(['users' => '4'])
                ->post(route('api.users.store'), [
                    'name'         => $model->name,
                    'email'        => $model->email,
                    'password'     => $model->password,
                    'confirmation' => $model->password,
                    'about'        => $model->about,
                ])->assertCreated()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'name',
                        'email',
                        'about',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->model->id,
                'name'  => $content->model->name,
                'email' => $content->model->email,
                'about' => $content->model->about,
            ]);
            self::$model = $this->class::find($content->model->id);
            self::$model->remember_token = env('APP_KEY');
            self::$model->save();
        });
    }

    /**
     * Test PATCH:api/users/{id}
     */
    public function testUserUpdateMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $model = factory($this->class)->make();

            //Check middleware
            $this->providePermissions(['users' => '0'])
                ->patch(route('api.users.update', self::$model->id), [
                    'name'  => $model->name,
                    'email' => $model->email,
                    'about' => $model->about,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            $response = $this->providePermissions(['users' => '2'])
                ->patch(route('api.users.update', self::$model->id), [
                    'name'         => $model->name,
                    'email'        => $model->email,
                    'password'     => $model->password,
                    'confirmation' => $model->password,
                    'about'        => $model->about,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'name',
                        'email',
                        'about',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => self::$model->id,
                'name'  => $model->name,
                'email' => $model->email,
                'about' => $model->about
            ]);

            self::$model = $this->class::find($content->model->id);
        });
    }

    /**
     * Test DELETE:api/users/{id}
     */
    public function testUserDestroyMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $model_ids = $this->class::where('remember_token', env('APP_KEY'))->pluck('id')->toArray();

            if (!$model_ids) {
                $model = factory($this->class)->create();
                $model_ids = [$model->id];
            }

            //Check middleware
            $this->providePermissions(['users' => '0'])
                ->patch(route('api.users.destroy', $model_ids[0]), [], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            foreach ($model_ids as $model_id) {
                $this->providePermissions(['users' => '1'])
                    ->delete(route('api.users.destroy', $model_id), [], [
                        'Authorization' => 'Bearer ' . self::$token
                    ])->assertNoContent();

                $this->assertDatabaseMissing($this->table, [
                    'id' => $model_id
                ]);
            }
        });
        self::$token = null;
        self::$user = null;
    }
}