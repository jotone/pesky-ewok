<?php

namespace Tests\Feature;

use Modules\User\Entities\User;
use Illuminate\Support\Facades\Hash;
use Tests\TestCases\ApiTestCase;

class GraphQlTest extends ApiTestCase
{
    /**
     * Testable class
     * @var string
     */
    protected $class = User::class;

    /**
     * Check if test data present in database
     */
    protected function checkDBMockData()
    {
        if (!$this->class::where('remember_token', env('APP_KEY'))->count()) {
            factory($this->class, (int)env('ADMIN_SEEDS'))->create();
        }
    }

    /**
     * Test GraphQL users query
     */
    public function testGraphQlUsersList()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $request = $this->post(route('graphql.user.post', 'user'), [
                'query' => '{user(per_page: 10) {data {id, email}, per_page}}'
            ], [
                'Authorization' => 'Bearer ' . self::$token
            ])->assertJsonStructure([
                'data' => [
                    'user' => [
                        'data',
                        'per_page'
                    ]
                ]
            ]);

            $content = json_decode($request->content())->data->user;

            $this->assertTrue(10 >= count($content->data)
                && !is_null($content->data)
                && $content->per_page == 10);
        });
    }

    /**
     * Test GraphQL user query
     */
    public function testGraphQlUserShow()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$token) {
                $this->createToken();
            }

            $request = $this->post(route('graphql.user.post', 'user'), [
                'query' => sprintf('{user(id: %u) {data {id, email, name}}}', self::$user->id)
            ], [
                'Authorization' => 'Bearer ' . self::$token
            ])->assertJsonStructure([
                'data' => [
                    'user' => [
                        'data'
                    ]
                ]
            ]);

            $content = json_decode($request->content())->data->user->data;

            $this->assertTrue(isset($content[0])
                && $content[0]->id == self::$user->id
                && $content[0]->email == self::$user->email
                && $content[0]->name == self::$user->name);
        });
    }

    /**
     * Test GraphQL user creation
     */
    public function testGraphQlUserCreate()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$token) {
                $this->createToken();
            }

            $user = factory($this->class)->make();

            $request = $this->post(route('graphql.user.post', 'user'), [
                'query' => sprintf(
                    'mutation {create (email: "%s", password: "%s", confirmation: "%s", name: "%s") {id, email, name}}',
                    $user->email,
                    env('ADMIN_PSW'),
                    env('ADMIN_PSW'),
                    $user->name
                )
            ], [
                'Authorization' => 'Bearer ' . self::$token
            ])->assertJsonStructure([
                'data' => [
                    'create' => [
                        'id',
                        'email',
                        'name'
                    ]
                ]
            ]);

            $content = json_decode($request->content())->data->create;

            $this->assertDatabaseHas('users', [
                'id'    => $content->id,
                'email' => $user->email,
                'name'  => $user->name
            ]);

            $this->class::where('id', $content->id)->update([
                'remember_token' => env('APP_KEY')
            ]);
        });
    }

    /**
     * Test GraphQl user update
     */
    public function testGraphQlUserUpdate()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$token) {
                $this->createToken();
            }

            $user = factory($this->class)->make();

            $this->post(route('graphql.user.post', 'user'), [
                'query' => sprintf(
                    'mutation {update (id: %u, name: "%s", email: "%s", img_url: "%s", about: "%s") {id, email, about, img_url}}',
                    self::$user->id,
                    $user->name,
                    $user->email,
                    $user->img_url,
                    $user->about
                )
            ], [
                'Authorization' => 'Bearer ' . self::$token
            ])->assertJsonStructure([
                'data' => [
                    'update' => [
                        'id',
                        'email',
                        'about',
                        'img_url'
                    ]
                ]
            ]);

            $this->assertDatabaseHas('users', [
                'id'    => self::$user->id,
                'name'  => $user->name,
                'email' => $user->email
            ]);
        });
    }

    /**
     * Test GraphQl user update password
     */
    public function testGraphQlUserUpdatePassword()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$token) {
                $this->createToken();
            }

            $this->post(route('graphql.user.post', 'user'), [
                'query' => sprintf(
                    'mutation {updatePassword (id: %u, password: "%s", confirmation: "%s") {id, email}}',
                    self::$user->id,
                    env('ADMIN_USER'),
                    env('ADMIN_USER')
                )
            ], [
                'Authorization' => 'Bearer ' . self::$token
            ])->assertJsonStructure([
                'data' => [
                    'updatePassword' => [
                        'id',
                        'email',
                    ]
                ]
            ]);

            self::$user = $this->class::find(self::$user->id);

            $this->assertFalse(Hash::check(env('ADMIN_PSW'), self::$user->password));
            $this->assertTrue(Hash::check(env('ADMIN_USER'), self::$user->password));
            self::$user = null;
            self::$token = null;
        });
    }

    /**
     * Test GraphQl user destroy
     */
    public function testGraphQlUserDestroy()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$token) {
                $this->createToken();
            }

            $model_ids = $this->class::where('remember_token', env('APP_KEY'))->pluck('id')->toArray();

            if (!$model_ids) {
                $model = factory($this->class)->create();
                $model_ids = [$model->id];
            }

            foreach ($model_ids as $model_id) {
                $response = $this->post(route('graphql.user.post', 'user'), [
                    'query' => 'mutation {destroy(id: ' . $model_id . ') {id}}'
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertJsonStructure([
                    'data' => [
                        'destroy'
                    ]
                ]);

                $this->assertNull(json_decode($response->content())->data->destroy);
            };
        });
    }
}