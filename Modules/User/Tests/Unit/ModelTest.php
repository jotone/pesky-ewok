<?php

namespace Modules\User\Tests\Unit;

use Modules\Roles\Entities\Role;
use Modules\User\Entities\User;
use Tests\TestCases\UnitTestCase;

class ModelTest extends UnitTestCase
{
    /**
     * Name of testing class
     * @var string
     */
    protected $class = User::class;

    /**
     * Name of testing table
     * @var string
     */
    protected $table = 'users';

    /**
     * Test Modules\User\Entities\User model creation
     */
    public function testUserCreate()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $this->assertDatabaseHas($this->table, [
                'name'              => self::$model->name,
                'email'             => self::$model->email,
                'remember_token'    => env('APP_KEY'),
                'about'             => self::$model->about,
                'email_verified_at' => self::$model->email_verified_at,
            ]);
        });
    }

    /**
     * Test Modules\User\Entities\User model update
     */
    public function testUserUpdate()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $model = factory($this->class)->make();

            self::$model->email = $model->email;
            self::$model->name = $model->name;
            self::$model->about = $model->about;
            self::$model->save();

            $this->assertDatabaseHas($this->table, [
                'name'  => self::$model->name,
                'email' => self::$model->email,
                'about' => self::$model->about,
            ]);
        });
    }

    /**
     * Test Modules\User\Entities\User model to role relation methods
     */
    public function testUserRoleRelation()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $role = factory(Role::class)->create();

            self::$model->roles()->attach($role->id);

            $this->assertDatabaseHas('user_roles', [
                'user_id' => self::$model->id,
                'role_id' => $role->id
            ])->assertTrue(in_array($role->id, self::$model->roles()->pluck('roles.id')->toArray()));

            self::$model->roles()->detach($role->id);

            $this->assertDatabaseMissing('user_roles', [
                'user_id' => self::$model->id,
                'role_id' => $role->id
            ]);
        });
    }

    /**
     * Test Modules\User\Entities\User model delete
     */
    public function testUserDelete()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $this->runRemove('remember_token', env('APP_KEY'));
        });
    }
}
