<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Modules\User\Entities\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $email = explode('@', $faker->email);
    $email[0] .= uniqid();

    return [
        'name'              => $faker->name,
        'email'             => implode('@', $email),
        'password'          => bcrypt(env('ADMIN_PSW')),
        'img_url'           => null,
        'about'             => $faker->jobTitle . ' in ' . $faker->company,
        'remember_token'    => env('APP_KEY'),
        'email_verified_at' => now(),
    ];
});
