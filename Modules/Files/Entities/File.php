<?php

namespace Modules\Files\Entities;

use App\Source\Helpers\CommonFunctions;
use App\Source\Helpers\FilesHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Modules\Settings\Entities\Settings;

class File extends Model
{
    /**
     * Model table
     * @var string
     */
    protected $table = 'files';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    public $fillable = [
        'url',
        'title',
        'caption',
        'description',
        'entity_type',
        'entity_id',
        'type',
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            if ($model->type == 'image' && file_exists(public_path($model->url))) {
                $saving_options = Settings::select('key', 'value')
                    ->whereNull('section')
                    ->whereNotNull('admin_menu_id')
                    ->pluck('value', 'key')
                    ->toArray();

                $file_data = pathinfo($model->url);
                $absolute_file_path = public_path($model->url);
                $absolute_dir_path = Str::finish(public_path($file_data['dirname']), '/');

                //Open file with Imagemagick
                $img = new \Imagick($absolute_file_path);
                //Remove EXIF
                if (isset($saving_options['files_remove_exif']) && $saving_options['files_remove_exif']) {
                    //Save image ICC profiles
                    $profiles = $img->getImageProfiles("icc", true);
                    //Delete EXIF data
                    $img->stripImage();
                    //Reset back ICC profiles
                    if (!empty($profiles)) {
                        $img->profileImage("icc", $profiles['icc']);
                    }
                    //Save image
                    $img->writeImage($absolute_file_path);
                }

                //Create WEBP image
                if (isset($saving_options['files_webp_on']) && $saving_options['files_webp_on']) {
                    $img->setImageFormat('webp');
                    $img->setImageAlphaChannel(1);
                    $img->setBackgroundColor(new \ImagickPixel('transparent'));

                    $dirname = $absolute_dir_path . 'webp/';

                    if (!is_dir($dirname)) {
                        mkdir($dirname, 0777, true);
                    }

                    $webp_file = $dirname . $file_data['filename'] . '.webp';
                    $img->writeImage($webp_file);
                }

                //Create THUMB images
                if (isset($saving_options['files_thumbs_on']) && $saving_options['files_thumbs_on']) {
                    //Save small thumbnail
                    FilesHelper::saveThumb($file_data, 'files_thumbs_on_small', 'small');
                    //Save large thumbnail
                    FilesHelper::saveThumb($file_data, 'files_thumbs_on_large', 'large', 2);
                }

                if (isset($saving_options['files_img_resize']) && $saving_options['files_img_resize']) {
                    $dimensions = (
                        isset($saving_options['files_img_resize_dimensions'])
                        && CommonFunctions::isAssoc($saving_options['files_img_resize_dimensions'])
                    )
                        ? $saving_options['files_img_resize_dimensions']
                        : ['width' => env('RESIZE_WIDTH'), 'height' => env('RESIZE_HEIGHT')];

                    $img = Image::make($absolute_file_path)
                        ->resize($dimensions['width'], $dimensions['height'], function ($constraint) {
                            $constraint->aspectRatio();
                        });

                    $img->save();
                }
            }
        });

        static::deleting(function ($model) {
            //Remove file
            if (!empty($model->url) && file_exists(public_path($model->url)) && !is_dir(public_path($model->url))) {
                unlink(public_path($model->url));
            }
        });
    }
}