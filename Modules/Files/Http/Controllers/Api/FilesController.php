<?php

namespace Modules\Files\Http\Controllers\Api;

use App\Source\Controllers\BasicApiController;
use App\Source\Helpers\CommonFunctions;
use App\Source\Helpers\FilesHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Files\Entities\File;
use Modules\Files\Http\Controllers\FilesControllerTrait;

class FilesController extends BasicApiController
{
    use FilesControllerTrait;

    /**
     * Create files
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function store(Request $request)
    {
        $args = $request->only('files', 'entity', 'entity_id');

        $entity = $this->getEntityModel($args['entity']);

        if (!$entity) {
            return response([], 400);
        }

        if (CommonFunctions::isAssoc($args['files'])) {
            $args['files'] = [$args['files']];
        }

        $result = [];
        foreach ($args['files'] as $i => $file) {
            if (!isset($file['title'])) {
                $file['title'] = '';
            }
            if (!isset($file['caption'])) {
                $file['caption'] = '';
            }
            if (!isset($file['description'])) {
                $file['description'] = '';
            }

            if (is_string($file['src'])) {
                if (filter_var($file['src'], FILTER_VALIDATE_URL)) {
                    $file_data = [
                        'status'   => 'success',
                        'data'     => $file['src'],
                        'external' => true
                    ];
                } else {
                    $file_data = FilesHelper::saveBlob($file['src'], $file['name'], $args['entity'], $args['entity_id']);
                }
            } else {
                $file_data = FilesHelper::saveFile($file['src'], $args['entity'], $args['entity_id']);
            }

            if ($file_data['status'] === 'success') {
                $type = (!$file_data['external'])
                    ? FilesHelper::getFileType($file_data['data'])
                    : $file['type'];

                $data = [
                    'url'         => $file_data['data'],
                    'title'       => $file['title'] ?? null,
                    'caption'     => $file['caption'] ?? null,
                    'description' => $file['description'] ?? null,
                    'entity_type' => $entity,
                    'entity_id'   => $args['entity_id'],
                    'type'        => $type
                ];

                //Run validation
                $validator = Validator::make($data, [
                    'url'         => 'required|string',
                    'title'       => 'nullable|string',
                    'caption'     => 'nullable|string',
                    'description' => 'nullable|string',
                    'type'        => 'required|string',
                    'entity_type' => 'required|string',
                    'entity_id'   => 'required|numeric',
                ]);

                if ($validator->fails()) {
                    return $this->validationErrorResponse($validator);
                }

                $result[] = [
                    'data'   => File::create($data),
                    'status' => 'success'
                ];
            } else {
                $result[] = $file_data;
            }
        }

        return response($result, 201);
    }

    /**
     * Update file
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function update(int $id, Request $request)
    {
        $model = $this->controllerSettings['model']::find($id);

        if (!$model) {
            return response([], 404);
        }

        $args = $request->only('src', 'name', 'title', 'caption', 'description', 'type');

        $rules = [];
        foreach ($args as $key => $val) {
            switch ($key) {
                case 'type':
                    $rules[$key] = 'required|string';
                    $model->$key = $val;
                    break;
                case 'caption':
                case 'description':
                case 'title':
                    $rules[$key] = 'nullable|string';
                    $model->$key = $val;
                    break;
                case 'src':
                    if (is_string($args['src']) && !empty($args['src'])) {
                        if (filter_var($args['src'], FILTER_VALIDATE_URL) || !isset($args['name'])) {
                            $model->url = $args['src'];
                            $model->type = $args['type'] ?? FilesHelper::getFileType($args['src']);
                        } else {
                            $file = FilesHelper::saveBlob(
                                $args['src'],
                                $args['name'],
                                $this->getModelEntity($model->entity_type),
                                $model->entity_id
                            );
                            $model->url = $file['data'];
                            $model->type = FilesHelper::getFileType($file['data']);
                        }
                    } else {
                        $file = FilesHelper::saveFile(
                            $args['src'],
                            $this->getModelEntity($model->entity_type),
                            $model->entity_id
                        );
                        $model->url = $file['data'];
                        $model->type = FilesHelper::getFileType($file['data']);
                    }
                    break;
            }
        }

        //Run validation
        $validator = Validator::make($args, $rules);

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $model->save();

        $result[] = [
            'data'   => $model,
            'status' => 'success'
        ];

        return response($result);
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function destroy(int $id)
    {
        return $this->removeModel($id);
    }
}
