<?php

namespace Modules\Files\Http\Controllers;

use Modules\Files\Entities\File;
use Modules\StaticPages\Entities\StaticPages;
use Modules\User\Entities\User;

trait FilesControllerTrait
{
    /**
     * Controller attributes
     * @var array
     */
    protected $controllerSettings = [
        'model' => File::class,
    ];

    /**
     * Get model by entity
     * @param string $entity
     * @return string|null
     */
    protected function getEntityModel(string $entity)
    {
        switch ($entity) {
            case 'static_page':
            case 'static_pages':
                return StaticPages::class;
                break;
            case 'users':
                return User::class;
        }

        return null;
    }

    /**
     * Get entity by model
     * @param string $entity
     * @return string|null
     */
    protected function getModelEntity(string $entity)
    {
        if ($entity === User::class) {
            return 'users';
        }

        return null;
    }
}