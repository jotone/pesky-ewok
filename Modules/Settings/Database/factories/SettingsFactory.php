<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Modules\Settings\Entities\Settings;
use Modules\User\Entities\User;
use Faker\Generator as Faker;

$factory->define(Settings::class, function (Faker $faker) {
    return [
        'user_id' => User::where('remember_token', env('APP_KEY'))->inRandomOrder()->value('id'),
        'caption' => $faker->city . ' ' . $faker->country,
        'key'     => $faker->country . uniqid(),
        'value'   => env('APP_KEY'),
        'type'    => 'text',
        'global'  => true,
    ];
});