<?php


namespace Modules\Settings\Tests\Feature;


use Modules\Settings\Entities\Settings;
use Tests\TestCases\ApiTestCase;

class GraphQLTest extends ApiTestCase
{
    /**
     * Testable class
     * @var string
     */
    protected $class = Settings::class;

    /**
     * Check if test data present in database
     */
    protected function checkDBMockData()
    {
        if (!$this->class::where('value', env('APP_KEY'))->count()) {
            factory($this->class, (int)env('ADMIN_SEEDS'))->create();
        }
    }

    /**
     * Test GraphQL settings query
     */
    public function testGraphQlSettingsList()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $request = $this->post(route('graphql.settings.post', 'settings'), [
                'query' => '{settings(per_page: 10) {data {id, key, value, caption}, per_page}}'
            ], [
                'Authorization' => 'Bearer ' . self::$token
            ])->assertJsonStructure([
                'data' => [
                    'settings' => [
                        'data',
                        'per_page'
                    ]
                ]
            ]);

            $content = json_decode($request->content())->data->settings;

            $this->assertTrue(10 >= count($content->data)
                && !is_null($content->data)
                && $content->per_page == 10);
        });
    }

    /**
     * Test GraphQL settings query
     */
    public function testGraphQlSettingsShow()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$token) {
                $this->createToken();
            }

            self::$model = $this->class::where('value', env('APP_KEY'))->inRandomOrder()->first();

            $request = $this->post(route('graphql.settings.post', 'settings'), [
                'query' => sprintf('{settings(id: %u) {data {id, key, value, caption}}}', self::$model->id)
            ], [
                'Authorization' => 'Bearer ' . self::$token
            ])->assertJsonStructure([
                'data' => [
                    'settings' => [
                        'data'
                    ]
                ]
            ]);

            $content = json_decode($request->content())->data->settings->data;

            $this->assertTrue(isset($content[0])
                && $content[0]->id == self::$model->id
                && $content[0]->key == self::$model->key
                && $content[0]->value == self::$model->value
                && $content[0]->caption == self::$model->caption);
        });
    }
}