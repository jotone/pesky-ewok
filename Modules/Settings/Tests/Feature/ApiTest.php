<?php

namespace Modules\Settings\Tests\Feature;

use Modules\Settings\Entities\Settings;
use Tests\TestCases\ApiTestCase;

class ApiTest extends ApiTestCase
{
    /**
     * Tested model entity
     * @var string
     */
    protected $class = Settings::class;

    /**
     * Tested model table
     * @var string
     */
    protected $table = 'settings';

    /**
     * Check if test data present in database
     */
    protected function checkDBMockData()
    {
        if (!$this->class::where('value', env('APP_KEY'))->count()) {
            factory($this->class, (int)env('ADMIN_SEEDS'))->create();
        }
    }

    /**
     * Test GET:api/settings without filter data
     */
    public function testSettingsIndexMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            //Check middleware
            $this->providePermissions(['settings' => '0'])
                ->getJson(route('api.settings.index'), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            $response = $this->providePermissions(['settings' => '8'])
                ->getJson(route('api.settings.index'), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'collection',
                    'total'
                ]);

            $content = json_decode($response->content());

            $key = mt_rand(0, count($content->collection));

            $this->assertDatabaseHas($this->table, [
                'id'  => $content->collection[$key]->id,
                'key' => $content->collection[$key]->key
            ]);
        });
    }

    /**
     * Test GET:api/settings/admin
     */
    public function testSettingsAdminMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $this->providePermissions(['settings' => '0'])
                ->getJson(route('api.settings.index.admin'), [
                    'Authorization'         => 'Bearer ' . self::$token,
                    'HTTP_X-Requested-With' => 'XMLHttpRequest'
                ])->assertOk()
                ->assertJsonStructure([
                    'sidemenu'
                ]);
        });
    }

    /**
     * Test GET:api/settings with filters
     */
    public function testSettingsIndexFilters()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            self::$model = Settings::where('value', env('APP_KEY'))->inRandomOrder()->first();
            $response = $this->providePermissions(['settings' => '8'])
                ->getJson(route('api.settings.index', [
                    'order_by'     => 'id',
                    'order_dir'    => 'asc',
                    'search'       => ['key' => self::$model->key],
                    'select'       => ['id', 'key', 'value'],
                ]), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'collection',
                    'total'
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->collection[0]->id,
                'key'   => $content->collection[0]->key,
                'value' => $content->collection[0]->value
            ]);
        });
    }

    /**
     * Test GET:api/settings/{id}
     */
    public function testSettingShowMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $setting = self::$user->settings()->inRandomOrder()->first();
            $response = $this->providePermissions(['settings' => '8'])
                ->getJson(route('api.settings.show', $setting->id), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'user_id',
                        'caption',
                        'key',
                        'value',
                        'type',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'      => $content->model->id,
                'user_id' => $content->model->user_id,
                'caption' => $content->model->caption,
                'key'     => $content->model->key,
                'value'   => $content->model->value
            ]);
        });
    }

    /**
     * Test POST:api/settings
     */
    public function testSettingStoreMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $model = factory($this->class)->make();

            //Check middleware
            $this->providePermissions(['settings' => '0'])
                ->post(route('api.settings.store', [
                    'caption'     => $model->caption,
                    'key'         => $model->key,
                    'value'       => $model->value,
                    'type'        => $model->type,
                    'description' => $model->description,
                ]), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(401);

            $response = $this->providePermissions(['settings' => '4'])
                ->post(route('api.settings.store'), [
                    'caption'     => $model->caption,
                    'key'         => $model->key,
                    'value'       => $model->value,
                    'type'        => $model->type,
                    'description' => $model->description,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertCreated()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'caption',
                        'key',
                        'value',
                        'type',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'      => $content->model->id,
                'caption' => $content->model->caption,
                'key'     => $content->model->key,
                'value'   => $content->model->value
            ]);
            self::$model = $this->class::find($content->model->id);
        });
    }

    /**
     * Test PATCH:api/settings/{id}
     */
    public function testSettingUpdateMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $model = factory($this->class)->make();

            //Check middleware
            $this->providePermissions(['settings' => '0'])
                ->patch(route('api.settings.update', self::$model->id), [
                    'caption'     => $model->caption,
                    'key'         => $model->key,
                    'value'       => $model->value,
                    'type'        => $model->type,
                    'description' => $model->description,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            $response = $this->providePermissions(['settings' => '2'])
                ->patch(route('api.settings.update', self::$model->id), [
                    'caption'     => $model->caption,
                    'key'         => $model->key,
                    'value'       => $model->value,
                    'type'        => $model->type,
                    'description' => $model->description,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'caption',
                        'key',
                        'value',
                        'type',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'      => self::$model->id,
                'caption' => $model->caption,
                'key'     => $model->key,
                'value'   => $model->value
            ]);

            self::$model = $this->class::find($content->model->id);
        });
    }

    /**
     * Test DELETE:api/settings/{id}
     */
    public function testSettingsDestroyMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $model_ids = $this->class::where('value', env('APP_KEY'))->pluck('id')->toArray();

            if (!$model_ids) {
                $model = factory($this->class)->create();
                $model_ids = [$model->id];
            }

            //Check middleware
            $this->providePermissions(['settings' => '0'])
                ->patch(route('api.settings.destroy', $model_ids[0]), [], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            foreach ($model_ids as $model_id) {
                $this->providePermissions(['settings' => '1'])
                    ->delete(route('api.settings.update', $model_id), [], [
                        'Authorization' => 'Bearer ' . self::$token
                    ])->assertNoContent();

                $this->assertDatabaseMissing($this->table, [
                    'id' => $model_id
                ]);
            }
        });
    }
}