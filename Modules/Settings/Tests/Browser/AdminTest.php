<?php

namespace Modules\Settings\Tests\Browser;

use Laravel\Dusk\Browser;
use Modules\Settings\Entities\Settings;
use Tests\TestCases\DuskTestCaseSingleton;

class AdminTest extends DuskTestCaseSingleton
{
    /**
     * Testable class
     * @var string
     */
    protected $class = Settings::class;

    /**
     * User "saved" message
     */
    protected $save_message = 'Settings option :attribute was successfully saved.';

    /**
     * User remove question
     * @var string
     */
    protected $remove_question = 'Do you really want to remove settings option :attribute?';

    /**
     * User remove confirmation
     * @var string
     */
    protected $remove_confirm = 'Please enter settings option key to remove.';

    /**
     * Test options
     * @var array
     */
    public static $options = [
        'test_field' => 'key',
        'text'       => [
            'create' => 'Settings option create',
            'edit'   => 'Settings option edit',
            'index'  => 'Settings list'
        ],
        'route'      => 'settings',
        'table'      => 'settings',
        'where'      => '',
        'condition'  => ''
    ];

    protected function setUp(): void
    {
        if (empty(self::$options['where'])) {
            self::$options['where'] = function ($q) {
                $q->whereNull('admin_menu_id')->where('global', true)->where('value', env('APP_KEY'));
            };

            self::$options['condition'] = function ($q) {
                $q->whereNull('admin_menu_id')->where('global', true);
            };
        }
        parent::setUp();

        if ($this->class::where(static::$options['where'])->count() < 30) {
            factory($this->class, 30)->create();
        }

        if (
            !empty(self::$user)
            && self::$user->settings()->where('key', 'take')->value('value') != '25'
        ) {
            \DB::table('settings')->where('user_id', self::$user->id)->where('key', 'take')->update(['value' => 25]);
        }
    }

    /**
     * Test settings index page
     * @throws \Throwable
     */
    public function testBrowserSettingsIndexPage()
    {
        $this->runIndex(__FUNCTION__);
    }

    /**
     * Test settings create page
     * @throws \Throwable
     */
    public function testBrowserSettingsCreatePage()
    {
        $this->runCreate(__FUNCTION__,
            function ($browser, $model, $first_model) {
                $browser->type('input[name="caption"]', $model->caption)
                    ->type('input[name="key"]', $model->key)
                    ->select('select[name="type"]', 'string')
                    ->type('input[name="value"]', $model->value)
                    ->pause(50)
                    ->click('button[name="save"]')
                    ->waitForText($this->message($this->save_message, $model->key), 20)
                    ->assertSee($this->message($this->save_message, $model->key))
                    ->visitRoute('admin.settings.index')
                    ->waitForText('Settings list', 20)
                    ->assertSee('Settings list')
                    ->waitForText($first_model->key, 20)
                    ->assertSee($first_model->key)
                    ->click('th#key .search-expand')
                    ->pause(750)
                    ->type('th#key .search-input', $model->key)
                    ->waitForText($model->key, 20)
                    ->assertSee($model->key);
            });
    }

    /**
     * Test settings update page
     * @throws \Throwable
     */
    public function testBrowserSettingsUpdatePage()
    {
        $this->runUpdate(__FUNCTION__,
            function ($browser, $model, $first_model) {
                $browser->type('input[name="caption"]', $model->caption)
                    ->type('input[name="key"]', $model->key)
                    ->press('Save')
                    ->waitForText($this->message($this->save_message, $model->key), 20)
                    ->assertSee($this->message($this->save_message, $model->key))
                    ->visitRoute('admin.settings.index')
                    ->waitForText('Settings list', 20)
                    ->assertSee('Settings list')
                    ->waitForText($first_model->key, 20)
                    ->assertSee($first_model->key)
                    ->click('th#key .search-expand')
                    ->pause(750)
                    ->type('th#key .search-input', $model->key)
                    ->waitForText($model->caption, 20)
                    ->assertSee($model->caption)
                    ->assertSee($model->key);
            });
    }

    /**
     * Test settings destroy page
     * @throws \Throwable
     */
    public function testBrowserSettingsDestroy()
    {
        $this->runDelete(__FUNCTION__,
            function ($browser, $first_model) {
                $browser->click('.items-list tbody tr .actions .remove')
                    ->assertDialogOpened($this->message($this->remove_question, self::$model->key))
                    ->acceptDialog()
                    ->pause(10)
                    ->waitForDialog()
                    ->assertDialogOpened($this->message($this->remove_confirm, self::$model->key))
                    ->typeInDialog(self::$model->key)
                    ->acceptDialog()
                    ->waitForText('Settings list', 20)
                    ->assertSee('Settings list')
                    ->assertSee('Create')
                    ->visitRoute('admin.settings.index')
                    ->waitForText('Settings list', 20)
                    ->assertSee('Settings list')
                    ->waitForText($first_model->key, 20)
                    ->assertSee($first_model->key)
                    ->click('th#key .search-expand')
                    ->pause(750)
                    ->type('th#key .search-input', self::$model->key)
                    ->assertDontSee(self::$model->key)
                    ->assertDontSeeIn('.items-list tbody td:nth-child(1)', self::$model->id);
            });
    }

    /**
     * Run model search
     * @param Browser $browser
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     */
    protected function search(Browser $browser)
    {
        $browser->type('th#key .search-input', self::$model->key)
            ->waitForText(self::$model->id, 20)
            ->waitForText(self::$model->key, 20)
            ->assertSee(self::$model->key)
            ->assertSee(self::$model->caption)
            ->pause(750);
    }
}