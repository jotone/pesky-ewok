<?php

namespace Modules\Settings\Tests\Unit;

use Modules\Settings\Entities\Settings;
use Tests\TestCases\UnitTestCase;

class ModelTest extends UnitTestCase
{
    /**
     * Name of testing class
     * @var string
     */
    protected $class = Settings::class;

    /**
     * Name of testing table
     * @var string
     */
    protected $table = 'settings';

    /**
     * Test Modules\Settings\Entities\Settings model creation
     */
    public function testSettingsCreate()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $this->assertDatabaseHas($this->table, [
                'caption' => self::$model->caption,
                'key'     => self::$model->key,
                'value'   => self::$model->value,
                'type'    => self::$model->type,
                'global'  => self::$model->global,
            ]);
        });
    }

    /**
     * Test Modules\Settings\Entities\Settings model update
     */
    public function testSettingsUpdate()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $model = factory($this->class)->make();

            self::$model->caption = $model->caption;
            self::$model->key = $model->key;
            self::$model->value = $model->value;
            self::$model->save();

            $this->assertDatabaseHas($this->table, [
                'id'      => self::$model->id,
                'caption' => self::$model->caption,
                'key'     => self::$model->key,
                'value'   => self::$model->value,
            ]);
        });
    }

    /**
     * Test Modules\Settings\Entities\Settings value getter
     */
    public function testSettingsValueGetterSetter()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            self::$model->value = 'string';
            self::$model->save();

            $this->assertDatabaseHas($this->table, [
                'id'    => self::$model->id,
                'value' => 'string'
            ]);

            self::$model->value = ['a' => 1];
            self::$model->save();

            $this->assertDatabaseHas($this->table, [
                'id'    => self::$model->id,
                'value' => json_encode(['a' => 1])
            ]);
        });
    }

    /**
     * Test Modules\Settings\Entities\Settings model delete
     */
    public function testSettingsDelete()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $this->runRemove('key', 'test_key_%');
        });
    }
}