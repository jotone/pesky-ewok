<?php

namespace Modules\Settings\Http\Controllers;

use Modules\Settings\Entities\Settings;

trait SettingsControllerTrait
{
    /**
     * DataTable columns
     * @var array
     */
    protected $tableBuilder = [
        [
            'id'     => 'id',
            'filter' => 'ID',
            'min'    => 1,
            'name'   => '#',
            'type'   => 'complete'
        ], [
            'name' => '<input name="selectAll" type="checkbox" class="input-checkbox" autocomplete="off"><span class="fa fa-cogs"></span>',
            'type' => 'title'
        ], [
            'id'   => 'caption',
            'min'  => 3,
            'name' => 'Caption',
            'type' => 'complete'
        ], [
            'id'   => 'key',
            'min'  => 3,
            'name' => 'Key',
            'type' => 'complete'
        ], [
            'id'   => 'value',
            'min'  => 3,
            'name' => 'Value',
            'type' => 'complete'
        ], [
            'id'   => 'description',
            'min'  => 3,
            'name' => 'Description',
            'type' => 'complete'
        ], [
            'id'   => 'type',
            'min'  => 3,
            'name' => 'Type',
            'type' => 'complete'
        ], [
            'id'   => 'created_at',
            'name' => 'Created At',
            'type' => 'directions'
        ]
    ];

    /**
     * Controller attributes
     * @var array
     */
    public $controllerSettings = [
        'custom_collection' => true,
        'model'             => Settings::class,
        'module'            => 'settings',
        'table'             => 'settings',
    ];

    /**
     * Store model in database
     * @param $args
     * @return mixed
     */
    protected function storeModel($args)
    {
        return $this->controllerSettings['model']::create([
            'caption'     => $args['caption'],
            'key'         => $args['key'],
            'value'       => $args['value'],
            'type'        => $args['type'],
            'description' => $args['description'],
            'global'      => true,
        ]);
    }

    /**
     * Custom collection request
     * @param $select
     * @return mixed
     */
    protected function customCollection($select)
    {
        return $this->controllerSettings['model']::select($select)->where('global', true)->whereNull('admin_menu_id');
    }
}