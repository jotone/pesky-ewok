<?php

namespace Modules\Settings\Http\Controllers\GraphQL;

use Modules\Settings\Entities\Settings;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class SettingsType extends GraphQLType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name'        => 'settings',
        'description' => 'The setting option',
        'model'       => Settings::class,
    ];

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            'id'         => [
                'type'        => Type::nonNull(Type::int()),
                'description' => 'The id of the user',
            ],
            'user_id'    => [
                'type'        => Type::int(),
                'description' => 'The ID of related user'
            ],
            'caption'    => [
                'type'        => Type::string(),
                'description' => 'The setting caption',
            ],
            'key'        => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'The setting key'
            ],
            'value'      => [
                'type'        => Type::string(),
                'description' => 'The setting value'
            ],
            'type'       => [
                'type'        => Type::string(),
                'description' => 'The setting value type'
            ],
            'created_at' => [
                'type'        => Type::string(),
                'description' => 'User date of creation'
            ],
            'updated_at' => [
                'type'        => Type::string(),
                'description' => 'User date of modification'
            ]
        ];
    }
}