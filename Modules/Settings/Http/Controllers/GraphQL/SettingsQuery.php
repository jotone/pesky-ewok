<?php

namespace Modules\Settings\Http\Controllers\GraphQL;

use App\Source\Controllers\GraphQlFilters;
use Modules\Settings\Entities\Settings;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class SettingsQuery extends Query
{
    use GraphQlFilters;

    /**
     * Class attributes
     * @var array
     */
    protected $attributes = [
        'name'  => 'Settings query',
        'model' => Settings::class,
    ];

    /**
     * Attach class type
     * @return Type
     */
    public function type(): Type
    {
        return GraphQL::paginate('settings');
    }

    /**
     * Set query arguments
     * @return array
     */
    public function args(): array
    {
        return array_merge($this->filterArgs(), [
            'key' => [
                'name' => 'key',
                'type' => Type::string()
            ],
            'global' => [
                'name' => 'global',
                'type' => Type::boolean()
            ]
        ]);
    }

    /**
     * Run query
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $resolveInfo
     * @param Closure $getSelectFields
     * @return Settings|mixed
     */
    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $this->buildFilters($args);

        $where = function ($query) use ($args) {
            if (isset($args['id'])) {
                $query->where('id', $args['id']);
            }

            if (isset($args['key'])) {
                $query->where('key', $args['key']);
            }

            if (isset($args['global'])) {
                $query->where('global', $args['global']);
            }
        };

        $fields = $getSelectFields()->getSelect();
        $relations = $getSelectFields()->getRelations();

        return $this->getCollection($where, $relations, $fields);
    }
}