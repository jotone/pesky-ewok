<?php

namespace Modules\Settings\Http\Controllers\Admin;

use App\Source\Controllers\BasicAdminController;
use Illuminate\Http\Request;
use Modules\Dashboard\Entities\AdminMenu;
use Modules\Settings\Entities\Settings;
use Modules\Settings\Http\Controllers\SettingsControllerTrait;

class SettingsController extends BasicAdminController
{
    use SettingsControllerTrait;

    /**
     * Controller request properties
     * @var array
     */
    protected $requestProperties = [
        'select' => ['id', 'caption', 'key', 'value', 'description', 'type', 'global', 'admin_menu_id', 'created_at'],
        'with'   => []
    ];

    /**
     * Render page with files saving behavior setting
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filesBehavior(Request $request)
    {
        return $this->renderTabForm($request, [
            //Page title
            'title' => 'File Saving Behavior Settings'
        ]);
    }

    /**
     * Render page with file types settings
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fileTypes(Request $request)
    {
        return $this->renderTabForm($request, [
            //Page title
            'title' => 'File Types Settings'
        ]);
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index(Request $request)
    {
        return $this->renderIndex($request, [
            //Prevent some components appear
            'prevent' => [
                'bulkDelete' => true
            ],
            //Page routes
            'routes'  => [
                'create' => route('admin.settings.create'),
                'edit'   => route('admin.settings.edit', 0),
                'list'   => route('api.settings.index'),
                'remove' => route('api.settings.destroy', 0),
            ],
            //Page title
            'title'   => 'Settings list'
        ]);
    }

    /**
     * Show the form for creating a new settings resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function create(Request $request)
    {
        return view($this->controllerSettings['module'] . '::form', [
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //Page title
            'title'       => 'Settings option create'
        ]);
    }

    /**
     * Show the form for editing the specified settings resource.
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function edit(int $id, Request $request)
    {
        $model = $this->controllerSettings['model']::find($id);

        if (!$model) {
            abort(404);
        }

        return view($this->controllerSettings['module'] . '::form', [
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //Editable model
            'model'       => $model,
            //page title
            'title'       => 'Settings option edit'
        ]);
    }

    /**
     * Get list of default settings
     * @param Request $request
     * @param array $shared
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function renderTabForm(Request $request, array $shared)
    {
        $menu_route = preg_replace('/(^admin\.|\.index$)/', '', $request->route()->getName());

        $settings = Settings::where('admin_menu_id', AdminMenu::where('route', $menu_route)->value('id'))
            ->where('type', 'bool');

        $sections = array_diff(array_values(array_unique($settings->pluck('section')->toArray())), ['', null]);

        return view('settings::tab-form', array_merge([
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //Settings list
            'collection'  => $settings->orderBy('caption', 'asc')->get()->map(function ($value) {
                $items = Settings::select('id', 'caption', 'key', 'value')
                    ->where('key', 'like', $value->key . '_%')
                    ->get();

                $value->items = ($items->count())
                    ? $items
                    : [];

                return $value;
            }),
            //Tab sections list
            'sections'    => $sections,
        ], $shared));
    }
}