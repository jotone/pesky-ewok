<?php

namespace Modules\Settings\Http\Controllers\Api;

use App\Source\Controllers\BasicApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Settings\Http\Controllers\SettingsControllerTrait;

class SettingsController extends BasicApiController
{

    use SettingsControllerTrait;

    /**
     * Get Settings collection
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function index(Request $request)
    {
        return response($this->getCollection($request), 200);
    }

    /**
     * Get Setting by ID
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function show(int $id)
    {
        return $this->getModel($id);
    }

    /**
     * Get admin settings list
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function admin()
    {
        return response(
            array_merge([
                'user_id' => auth()->user()->id,
                'links'   => [
                    'files' => [
                        'list'    => route('api.file.index'),
                        'show'    => route('api.file.show', 0),
                        'store'   => route('api.file.store'),
                        'update'  => route('api.file.update', 0),
                        'destroy' => route('api.file.destroy', 0)
                    ]
                ]
            ], $this->controllerSettings['model']::select('key', 'value')
                ->whereNull('user_id')
                ->orWhere('user_id', auth()->user()->id)
                ->pluck('value', 'key')
                ->toArray()));
    }

    /**
     * Create Setting
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function store(Request $request)
    {
        $args = $request->only('caption', 'key', 'value', 'description', 'type');

        $validator = Validator::make($args, [
            'key'         => 'required|string',
            'value'       => 'required',
            'type'        => 'required|string',
            'caption'     => 'nullable|string',
            'description' => 'nullable|string'
        ]);

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $model = $this->storeModel($args);

        return ($model)
            ? response(['model' => $model], 201)
            : response([
                'error' => 'Cannot write to database.'
            ], 500);
    }

    /**
     * Update Setting by ID
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function update(int $id, Request $request)
    {
        $model = $this->controllerSettings['model']::find($id);

        if (!$model) {
            return response([], 404);
        }

        $args = $request->only('caption', 'key', 'value', 'description', 'type');

        $rules = [];
        foreach ($args as $key => $val) {
            switch ($key) {
                case 'key':
                case 'type':
                    $rules[$key] = 'required|string';
                    break;
                case 'value':
                    $rules[$key] = 'required';
                    break;
                case 'caption':
                case 'description':
                    $rules[$key] = 'nullable|string';
                    break;
            }
            $model->$key = $val;
        }

        //Run validation
        $validator = Validator::make($args, $rules);

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $model->save();

        return response([
            'model' => $model
        ]);
    }

    /**
     * Set specified value to user option
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function upgrade($id, Request $request)
    {
        $args = $request->only('key', 'value');

        $validator = Validator::make($args, [
            'key'   => 'required|string',
            'value' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $model = $this->controllerSettings['model']::where('user_id', $id)->where('key', $args['key'])->first();

        if (!$model) {
            return response([], 404);
        }

        $model->value = $args['value'];
        $model->save();

        return response([
            'model' => $model
        ]);
    }

    /**
     * Remove Setting by ID
     * @param int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function destroy($id)
    {
        return $this->removeModel($id);
    }
}
