<?php

return [
    'name'           => 'Settings',
    'menu'           => [
        'title'      => 'Site Settings',
        'slug'       => 'settings_menu',
        'route'      => '#settings',
        'image'      => 'fa-cogs',
        'position'   => 200,
        'is_section' => true,
        'editable'   => true,
        'inner'      => [
            [
                'title'      => 'File System Behavior',
                'slug'       => 'settings-file-behavior',
                'route'      => 'settings.files_behavior',
                'image'      => 'fa-floppy-o',
                'position'   => 0,
                'is_section' => false,
                'editable'   => true,
                'inner'      => null
            ], [
                'title'      => 'File Types',
                'slug'       => 'settings-file-types',
                'route'      => 'settings.file_types',
                'image'      => 'fa-files-o',
                'position'   => 0,
                'is_section' => false,
                'editable'   => true,
                'inner'      => null
            ], [
                'title'      => 'Settings',
                'slug'       => 'settings',
                'route'      => 'settings',
                'image'      => 'fa-wrench',
                'position'   => 0,
                'is_section' => false,
                'editable'   => true,
                'inner'      => null
            ]
        ],
    ],
    'permissions'    => [
        [
            'title' => 'Settings Page',
            'route' => 'settings'
        ], [
            'title' => 'File Saving Behavior',
            'route' => 'settings.files_behavior'
        ], [
            'title' => 'File Types',
            'route' => 'settings.file_types'
        ]
    ],
    'default_values' => [
        'settings-file-behavior' => [
            'none' => [
                [
                    'caption' => 'Save user files to date folders',
                    'key'     => 'files_to_datefolders',
                    'about'   => 'Will create folder named as creation date and put file into it',
                    'value'   => false
                ], [
                    'caption' => 'Use thumbnails preview pictures for images',
                    'key'     => 'files_thumbs_on',
                    'about'   => 'Will create a <strong>thumb</strong> file on image saving',
                    'value'   => true
                ], [
                    'caption' => '<strong>Small</strong> thumbnail image dimensions',
                    'key'     => 'files_thumbs_on_small',
                    'about'   => '',
                    'value'   => '{"width": 150, "height": 90}'
                ], [
                    'caption' => '<strong>Large</strong> thumbnail image dimensions',
                    'key'     => 'files_thumbs_on_large',
                    'about'   => '',
                    'value'   => '{"width": 400, "height": 300}'
                ], [
                    'caption' => 'Resize image',
                    'key'     => 'files_img_resize',
                    'about'   => 'Will resize an image after saving',
                    'value'   => true
                ], [
                    'caption' => 'Image dimensions for resizing',
                    'key'     => 'files_img_resize_dimensions',
                    'about'   => '',
                    'value'   => '{"width": 800, "height": 600}'
                ], [
                    'caption' => 'Create webp image on image upload',
                    'key'     => 'files_webp_on',
                    'about'   => 'Will create a <strong>webp</strong> file on image saving',
                    'value'   => true
                ], [
                    'caption' => 'Remove Image meta-data and EXIF information',
                    'key'     => 'files_remove_exif',
                    'about'   => 'Will reset all EXIF meta-data from file on image saving',
                    'value'   => true
                ], [
                    'caption' => 'File browser',
                    'key'     => 'filebrowser',
                    'about'   => 'Choose files through the file browser',
                    'value'   => false,
                ]
            ]

        ],
        'settings-file-types'    => [
            'audio'    => [
                [
                    'caption' => '.aac',
                    'key'     => 'audio/aac',
                    'about'   => 'Advanced Audio Coding. Audio coding standard for lossy digital audio compression.',
                    'value'   => false
                ], [
                    'caption' => '.mp4',
                    'key'     => 'audio/mp4',
                    'about'   => 'MPEG-4 Part 14 or MP4. Digital multimedia container format.',
                    'value'   => false
                ], [
                    'caption' => '.mp3',
                    'key'     => 'audio/mpeg',
                    'about'   => 'MPEG-1 Audio Layer III or MPEG-2 Audio Layer III.',
                    'value'   => true
                ], [
                    'caption' => '.ogg',
                    'key'     => 'audio/ogg',
                    'about'   => 'open standard multimedia container format, which is the main file and stream format for multimedia codecs.',
                    'value'   => true
                ], [
                    'caption' => '.wav',
                    'key'     => 'audio/x-wav',
                    'about'   => 'Waveform Audio File Format.',
                    'value'   => true
                ], [
                    'caption' => '.webm',
                    'key'     => 'audio/webm',
                    'about'   => 'Audiovisual media file format.',
                    'value'   => true
                ]
            ],
            'image'    => [
                [
                    'caption' => '.apng',
                    'key'     => 'image/apng',
                    'about'   => 'The Animated Portable Network Graphics. It allows for animated PNG files that work similarly to animated GIF files, while supporting 24-bit images and 8-bit transparency not available for GIFs.',
                    'value'   => false,
                ], [
                    'caption' => '.bmp',
                    'key'     => 'image/bmp',
                    'about'   => 'Bitmap Image. Raster graphics image file format used to store bitmap digital images, independently of the display device (such as a graphics adapter), especially on Microsoft Windows[1] and OS/2[2] operating systems.',
                    'value'   => false,
                ], [
                    'caption' => '.gif',
                    'key'     => 'image/gif',
                    'about'   => 'The Graphics Interchange Format. Able to store compressed data without loss of quality in a format of no more than 256 colors.',
                    'value'   => true,
                ], [
                    'caption' => '.ico, .cur',
                    'key'     => 'image/x-icon',
                    'about'   => 'The ICO file format is an image file format for computer icons.',
                    'value'   => true,
                ], [
                    'caption' => '.jpg, .jpeg, .jfif, .pjpeg, .pjp',
                    'key'     => 'image/jpeg',
                    'about'   => 'The Joint Photographic Experts Group. Commonly used method of lossy compression for digital images, particularly for those images produced by digital photography.',
                    'value'   => true,
                ], [
                    'caption' => '.png',
                    'key'     => 'image/png',
                    'about'   => 'Portable Network Graphics. Raster-graphics file-format that supports lossless data compression.',
                    'value'   => true,
                ], [
                    'caption' => '.svg',
                    'key'     => 'image/svg+xml',
                    'about'   => 'Scalable Vector Graphics. XML-based vector image format for two-dimensional graphics with support for interactivity and animation.',
                    'value'   => true,
                ], [
                    'caption' => '.tiff, .tif',
                    'key'     => 'image/tiff',
                    'about'   => 'Tagged Image File Format. Computer file format for storing raster graphics images. TIFF is widely supported by scanning, faxing, word processing, optical character recognition, image manipulation, desktop publishing, and page-layout applications',
                    'value'   => false,
                ], [
                    'caption' => '.webp',
                    'key'     => 'image/webp',
                    'about'   => 'Image format employing both lossy and lossless compression.',
                    'value'   => true,
                ],
            ],
            'video'    => [
                [
                    'caption' => '.mpg, .mpeg',
                    'key'     => 'video/mpeg',
                    'about'   => 'The Moving Picture Experts Group. One of a number of file extensions for MPEG-1 or MPEG-2 audio and video compression',
                    'value'   => false,
                ], [
                    'caption' => '.mp4',
                    'key'     => 'video/mp4',
                    'about'   => 'MPEG-4 Part 14 or MP4. Digital multimedia container format most commonly used to store video and audio.',
                    'value'   => false,
                ], [
                    'caption' => '.webm',
                    'key'     => 'video/webm',
                    'about'   => 'Audiovisual media file format.',
                    'value'   => true
                ]
            ],
            'archive'  => [
                [
                    'caption' => '.7z, .s7z',
                    'key'     => 'application/x-7z-compressed',
                    'about'   => 'Open source file format. Used by 7-Zip.',
                    'value'   => false
                ], [
                    'caption' => '.apk',
                    'key'     => 'application/vnd.android.package-archive',
                    'about'   => 'Android Package. The package file format used by the Android operating system for distribution and installation of mobile apps and middleware.',
                    'value'   => false
                ], [
                    'caption' => '.dmg',
                    'key'     => 'application/x-apple-diskimage',
                    'about'   => 'Apple Disk Image. Disk image format commonly used by the macOS operating system.',
                    'value'   => false
                ], [
                    'caption' => '.iso',
                    'key'     => 'application/x-iso9660-image',
                    'about'   => 'An archive format originally used mainly for archiving and distribution of the exact, nearly-exact, or custom-modified contents of an optical storage medium such as a CD-ROM or DVD-ROM.',
                    'value'   => false
                ], [
                    'caption' => '.jar',
                    'key'     => 'application/java-archive',
                    'about'   => 'A package file format typically used to aggregate many Java class files and associated metadata and resources (text, images, etc.) into one file for distribution.',
                    'value'   => false
                ], [
                    'caption' => '.rar',
                    'key'     => 'application/x-rar-compressed',
                    'about'   => 'RAR is a proprietary archive file format that supports data compression, error recovery and file spanning.',
                    'value'   => false
                ], [
                    'caption' => '.tar',
                    'key'     => 'application/x-tar',
                    'about'   => 'Bitstream or archive file format on Unix systems.',
                    'value'   => false
                ], [
                    'caption' => '.tar.gz, .tgz, .tar.bz2',
                    'key'     => 'application/x-gtar',
                    'about'   => 'The "tarball" format combines tar archives with a file-based compression scheme (usually gzip). Commonly used for source and binary distribution on Unix-like platforms, widely available elsewhere.',
                    'value'   => false
                ], [
                    'caption' => '.zip, .zipx',
                    'key'     => 'application/zip',
                    'about'   => 'The most widely used compression format on Microsoft Windows. Commonly used on Macintosh and Unix systems as well.',
                    'value'   => false
                ]
            ],
            'document' => [
                [
                    'caption' => '.doc',
                    'key'     => 'application/msword',
                    'about'   => 'Word document.',
                    'value'   => false
                ], [
                    'caption' => '.pdf',
                    'key'     => 'application/pdf',
                    'about'   => 'Acrobat file.',
                    'value'   => false
                ], [
                    'caption' => '.rtf',
                    'key'     => 'application/rtf',
                    'about'   => 'Rich text format file.',
                    'value'   => false
                ], [
                    'caption' => '.xls, .xlm, .xlc, .xla, .xlw, .xlt',
                    'key'     => 'application/vnd.ms-excel',
                    'about'   => 'Excel document.',
                    'value'   => false
                ], [
                    'caption' => '.txt',
                    'key'     => 'text/plain',
                    'about'   => 'Text data.',
                    'value'   => false
                ], [
                    'caption' => '.xml',
                    'key'     => 'text/xml',
                    'about'   => 'Extensible Markup Language (XML) File',
                    'value'   => false
                ]
            ]
        ]
    ]
];
