/**
 * Get items list
 * @param collection
 * @returns {string}
 */
App.getList = collection => {
  let body = '';
  for (let i in collection) {
    body += Templates.settings.render(collection[i]);
  }
  return body;
};

/**
 * Submit form
 */
App.form.submit = () => {
  App.requests.save({
    caption: $('input[name="caption"]').val(),
    key: $('input[name="key"]').val(),
    value: (() => {
      switch ($('select[name="type"]').val()) {
        case 'bool':
          return $('input[name="value"]').prop('checked')
              ? 1
              : 0;
        case 'string':
          return $('input[name="value"]').val();
        case 'text':
          return $('textarea[name="value"]').val();
      }
    })(),
    description: $('textarea[name="description"]').val(),
    type: $('select[name="type"]').val()
  }).then(App.form.onSuccessSave);
};

/**
 * Form validation rules
 */
App.form.validation = {
  input: {
    key: ['required'],
  },
  select: {
    type: ['required']
  }
};

App.modules.settings = {
  /**
   * Get value container by type
   * @param {string} elem
   * @returns {string}
   */
  buildValueContainer: elem => {
    switch (elem) {
      case 'bool':
        return Templates.settings.checkboxValue();
      case 'string':
        return Templates.settings.stringValue();
      case 'text':
        return Templates.settings.textareaValue();
    }

    return '';
  },
  /**
   * Send saving request
   * @param {int} id
   * @param data
   */
  changeOptionValue: (id, data) => {
    const url = App.common.setIdToRemoveLink($('.form-wrap').attr('action'), id);
    const method = $('.form-wrap').attr('method');
    App.requests.save(data, url, method);
  },
  /**
   * Set selected value container
   * @param elem
   */
  selectType: elem => {
    const html = App.modules.settings.buildValueContainer(elem.val());
    $('#valueField').html(html);
    const hiddenVal = $('input[name="hiddenValue"]');

    if (hiddenVal.length) {
      const value = hiddenVal.val();
      const $input = $('[name="value"]');

      switch (elem.val()) {
        case 'bool':
          $input.prop('checked', (value === '1'));
          break;
        case 'string':
        case 'text':
          $input.val(value);
          break;
      }
    }
  }
};

App.modules.settings.events = {
  /**
   * Save settings option
   */
  '.form-wrap[data-placement="tabs-form"]': [{
    action: 'on.change',
    item: 'input[type="checkbox"]',
    callback: function () {
      $(this).closest('.tab-form-row').find('.tab-form-misc-warp').toggleClass('active');

      App.modules.settings.changeOptionValue($(this).data('id'), {
        key: $(this).attr('name'),
        value: ($(this).prop('checked') === true)
            ? 1
            : 0,
      });
    }
  }, {
    action: 'on.keyup',
    item: 'input.tab-form-input',
    debounce: 1000,
    callback: function () {
      const $name = $(this).attr('name');

      if ($name.indexOf('.') > 0) {
        const [settingKey,] = $name.split('.');
        let value = {};

        $(this).closest('.tab-form-misc-warp').find('label').each(function () {
          const input = $(this).find('.tab-form-input');
          const [, key] = input.attr('name').split('.');

          value[key] = input.val();
        });

        App.modules.settings.changeOptionValue($(this).data('id'), {
          key: settingKey,
          value: JSON.stringify(value)
        });
      }
    }
  }],
  /**
   * Change settings option value
   */
  '.form-details': [{
    action: 'on.change',
    item: 'select[name="type"]',
    callback: function () {
      App.modules.settings.selectType($(this));
    }
  }]
};

/**
 * Module messages
 * @type {{removeQuestion: string, removed: string, titleField: string, success: string, removeConfirm: string}}
 */
App.modules.messages = {
  success: 'Settings option :attribute was successfully saved.',
  updated: 'Attribute ":attribute" wa applied to setting option.',
  removed: 'Settings option :attribute was successfully removed.',
  removeQuestion: 'Do you really want to remove settings option :attribute?',
  removeConfirm: 'Please enter settings option key to remove.',
  titleField: 'key'
};

Templates.settings = {
  /**
   * Boolean value
   * @returns {string}
   */
  checkboxValue: () => '<input name="value" type="checkbox"><span class="caption" style="padding-left: 15px;">Default value</span>',
  /**
   * String input
   * @returns {string}
   */
  stringValue: () => '<input name="value" class="input-text col-1-2">',
  /**
   * Text area block
   * @returns {string}
   */
  textareaValue: () => '<textarea class="textarea-block col-1" name="value"></textarea>',
  /**
   * Item table row
   * @param item
   * @returns {string}
   */
  render: (item) => Templates.itemsList.defaultTableWrap(item.id,
        '<td><span>' + (item.caption || '') + '</span></td>' +
        '<td><span>' + item.key + '</span></td>' +
        '<td><span>' + (item.value || '') + '</span></td>' +
        '<td><span>' + (item.description || '') + '</span></td>' +
        '<td><span>' + item.type + '</span></td>' +
        '<td><span>' + item.created_at + '</span></td>')
};

$(document).ready(() => App.modules.settings.selectType($('select[name="type"]')));