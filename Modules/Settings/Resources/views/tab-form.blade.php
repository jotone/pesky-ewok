@extends('admin.layouts.master')

@section('content')
    @include('admin.layouts.breadcrumbs')

    <div class="page-title">{{ $title }}</div>

    @if (!empty($sections))
        <ul class="tabs-wrap">
            @foreach($sections as $section)
                <li id="{{ $section }}" @if($loop->first) class="active" @endif>
                    @php
                        $section_name = explode('_', $section);
                        $section_name[0] = ucfirst($section_name[0]);
                    @endphp
                    {{ implode(' ', $section_name) }}
                </li>
            @endforeach
        </ul>
    @endif

    <form class="form-wrap flex-stretch-start"
          method="PATCH"
          action="{{ route('api.settings.update', 0) }}"
          data-placement="tabs-form">

        {{ csrf_field() }}

        @if (!empty($sections))
            @foreach($sections as $section)
                <div class="form-details @if($loop->first) active @endif" id="{{ $section }}">
                    @foreach($collection as $model)
                        @if ($model->section == $section)
                            @include('settings::partials.' . $model->type)
                        @endif
                    @endforeach
                </div>
            @endforeach
        @else
            <div class="form-details active">
                @foreach($collection as $model)
                    @include('settings::partials.' . $model->type)
                @endforeach
            </div>
        @endif
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('js/admin/scripts/settings.min.js') }}" type="text/javascript"></script>
@endsection