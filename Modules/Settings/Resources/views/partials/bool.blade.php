<fieldset>
    <legend>{{ $model->caption }}</legend>

    <div class="row tab-form-row">
        <label class="flex-center-start">
            <input name="{{ $model->key }}"
                   type="checkbox"
                   @if($model->value == 1) checked @endif
                   data-id="{{ $model->id }}">
            @if ($model->description)
                <span class="caption" style="padding-left: 15px">{!! $model->description !!}</span>
            @endif
        </label>

        <div class="tab-form-misc-warp @if($model->value == 1) active @endif">
            @if (!empty($model->items))
                @foreach($model->items as $item)
                    @if (is_array($item->value))
                        <div class="flex-center-start">
                            @foreach($item->value as $key => $val)
                                <label>
                                    <input name="{{ $item->key }}.{{ $key }}"
                                           class="tab-form-input"
                                           value="{{ $val }}"
                                           title="{{ mb_convert_case($key, MB_CASE_TITLE) }}"
                                           placeholder="{{ mb_convert_case($key, MB_CASE_TITLE) }}&hellip;"
                                           data-id="{{ $item->id }}">
                                </label>
                            @endforeach
                            <p class="field-description">{!! $item->caption !!}</p>
                        </div>
                    @else

                    @endif
                @endforeach
            @endif
        </div>
    </div>
</fieldset>