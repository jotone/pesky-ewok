@extends('admin.layouts.master')

@section('content')
    @include('admin.layouts.breadcrumbs')

    <div class="page-title">{{ $title }}</div>

    <form class="form-wrap flex-stretch-start"
          data-edit="{{ route('admin.settings.edit', 0) }}"
          method="@isset($model){{ 'PUT' }}@else{{ 'POST' }}@endisset"
          action="@isset($model){{ route('api.settings.update', $model->id) }}@else{{ route('api.settings.store') }}@endisset">

        {{ csrf_field() }}

        @isset($model)
            <input name="hiddenValue" type="hidden" value="{{ $model->value }}">
        @endisset

        <div class="form-details active">
            <fieldset>
                <legend>Main data</legend>

                <div class="row">
                    <label>
                        <div class="caption">Caption:</div>
                        <input name="caption"
                               class="input-text col-1-2"
                               type="text"
                               value="{{ $model->caption ?? '' }}"
                               placeholder="Caption&hellip;">
                    </label>
                </div>

                <div class="row">
                    <label>
                        <div class="caption">Key:</div>
                        <input name="key"
                               class="input-text col-1-2"
                               type="text"
                               data-changed="0"
                               value="{{ $model->key ?? '' }}"
                               placeholder="Key&hellip;">
                    </label>
                </div>

                <div class="row">
                    <label>
                        <div class="caption">Type:</div>
                        <select name="type" class="col-1-2">
                            <option value="bool" @if(isset($model) && $model->type == 'bool') selected @endif>
                                Boolean
                            </option>
                            <option value="text" @if(isset($model) && $model->type == 'text') selected @endif>
                                Text
                            </option>
                            <option value="string" @if(isset($model) && $model->type == 'string') selected
                                    @elseif(!isset($model)) selected @endif>
                                String
                            </option>
                        </select>
                    </label>
                </div>

                <div class="row">
                    <label>
                        <div class="caption">Value:</div>
                        <div id="valueField"></div>
                    </label>
                </div>
            </fieldset>

            <fieldset>
                <legend>Description</legend>
                <div class="row">
                    <textarea class="textarea-block col-1" name="description">{{ $model->description ?? '' }}</textarea>
                </div>
            </fieldset>
        </div>

        <aside class="form-assistant-wrap">
            <ul class="page-navigation"></ul>

            @if (isset($model))
                <div class="etc-info">
                    <span>Created at:&nbsp;</span>
                    <ins>{{ $model->created }}</ins>
                </div>
                <div class="etc-info">
                    <span>Last update:&nbsp;</span>
                    <ins>{{ $model->updated }}</ins>
                </div>
            @endif

            <div class="save-wrap">
                <button name="save" class="button green" type="submit" title="Save settings data">Save</button>
            </div>
        </aside>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('js/admin/scripts/settings.min.js') }}" type="text/javascript"></script>
@endsection