<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'as'         => 'admin.',
    'middleware' => ['admin'],
    'namespace'  => 'Admin',
    'prefix'     => 'admin'
], function () {
    Route::get('/settings/files_behavior', 'SettingsController@filesBehavior')->name('settings.files_behavior.index');
    Route::get('/settings/file_types', 'SettingsController@fileTypes')->name('settings.file_types.index');
    Route::resource('/settings', 'SettingsController')->only(['index', 'create', 'edit']);
});
