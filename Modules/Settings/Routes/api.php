<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'as'         => 'api.',
    'middleware' => ['api', 'jwt.verify', 'admin'],
    'namespace'  => 'Api',
], function () {
    Route::get('/settings/admin', 'SettingsController@admin')->name('settings.index.admin');
    Route::patch('/settings/{id}/upgrade', 'SettingsController@upgrade')->name('settings.update.user');
    Route::resource('/settings', 'SettingsController')->except(['create', 'edit']);
});