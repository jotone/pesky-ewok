<?php

namespace Modules\Settings\Entities;

use App\Source\Helpers\CommonFunctions;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /**
     * Model table
     * @var string
     */
    protected $table = 'settings';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    public $fillable = [
        'user_id',
        'caption',
        'key',
        'value',
        'description',
        'type',
        'global',
        'section',
        'admin_menu_id'
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'global' => 'boolean',
    ];

    /**
     * Get value attribute
     * @param string $value
     * @return mixed
     */
    public function getValueAttribute(string $value)
    {
        return (CommonFunctions::isJson($value))
            ? json_decode($value, true)
            : $value;
    }

    /**
     * Set value attribute
     * @param $value
     * @return void
     */
    public function setValueAttribute($value): void
    {
        $this->attributes['value'] = is_array($value)
            ? json_encode($value)
            : $value;
    }

    /**
     * Get related User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(\Modules\User\Entities\User::class, 'user_id', 'id');
    }
}
