<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Modules\Permissions\Entities\Permission;
use Faker\Generator as Faker;

$factory->define(Permission::class, function (Faker $faker) {
    $routeCollection = \Route::getRoutes();
    $routes = [];
    foreach ($routeCollection as $value) {
        if (
            strpos($value->uri, '_ignition') === false
            && isset($value->action['as'])
            && strpos($value->action['as'], '.index') !== false
        ) {
            $routes[] = $value->action['as'];
        }
    }
    return [
        'title' => 'test_permission_' . uniqid(),
        'route' => $routes[array_rand($routes)]
    ];
});
