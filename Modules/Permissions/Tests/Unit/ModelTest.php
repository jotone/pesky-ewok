<?php

namespace Tests\Unit;

use Modules\Permissions\Entities\Permission;
use Modules\Roles\Entities\Role;
use Tests\TestCases\UnitTestCase;

class ModelTest extends UnitTestCase
{
    /**
     * Name of testing class
     *
     * @var string
     */
    protected $class = Permission::class;

    /**
     * Name of testing table
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * Test \Modules\Permissions\Entities\Permission model creation
     */
    public function testPermissionCreate()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $this->assertDatabaseHas($this->table, [
                'title' => self::$model->title,
                'route' => self::$model->route,
            ]);
        });
    }

    /**
     * Test \Modules\Permissions\Entities\Permission model update
     */
    public function testPermissionUpdate()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $model = factory($this->class)->make();

            self::$model->title = $model->title;
            self::$model->route = $model->route;
            self::$model->save();

            $this->assertDatabaseHas($this->table, [
                'title' => self::$model->title,
                'route' => self::$model->route,
            ]);
        });
    }

    /**
     * Test \Modules\Permissions\Entities\Permission model role relation methods
     */
    public function testPermissionRoleRelation()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $role = factory(Role::class)->create();

            self::$model->roles()->save($role, ['value' => '0']);

            $this->assertDatabaseHas('role_permissions', [
                'permission_id' => self::$model->id,
                'role_id'       => $role->id
            ])->assertTrue(in_array($role->id, self::$model->roles()->pluck('roles.id')->toArray()));

            self::$model->roles()->detach($role->id);

            $this->assertDatabaseMissing('role_permissions', [
                'permission_id' => self::$model->id,
                'role_id'       => $role->id
            ]);
        });
    }

    /**
     * Test \Modules\Permissions\Entities\Permission model delete
     */
    public function testPermissionDelete()
    {
        $this->wrapUnit(__FUNCTION__, function () {
            $this->runRemove('title', 'test_permission_%');
        });
    }
}