<?php

namespace Modules\Permissions\Tests\Feature;

use Modules\Permissions\Entities\Permission;
use Tests\TestCases\ApiTestCase;

class ApiTest extends ApiTestCase
{
    /**
     * Tested model entity
     * @var string
     */
    protected $class = Permission::class;

    /**
     * Tested model table
     * @var string
     */
    protected $table = 'permissions';

    /**
     * Check if test data present in database
     */
    protected function checkDBMockData()
    {
        if (!$this->class::where('title', 'like', 'test_permission_%')->count()) {
            factory($this->class, (int)env('ADMIN_SEEDS'))->create();
        }
    }

    /**
     * Test GET:api/users/permissions without filter data
     */
    public function testPermissionsIndexMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            //Check middleware
            $this->providePermissions(['users.permissions' => '0'])
                ->getJson(route('api.users.permissions.index'), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            $response = $this->providePermissions(['users.permissions' => '8'])
                ->getJson(route('api.users.permissions.index'), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'collection',
                    'total'
                ]);

            $content = json_decode($response->content());

            $key = mt_rand(0, count($content->collection) - 1);

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->collection[$key]->id,
                'title' => $content->collection[$key]->title,
                'route' => $content->collection[$key]->route
            ]);
        });
    }

    /**
     * Test GET:api/users/permissions with filters
     */
    public function testPermissionsIndexFilters()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$model) {
                self::$model = factory($this->class)->create();
            }

            $response = $this->providePermissions(['users.permissions' => '8'])
                ->getJson(route('api.users.permissions.index', [
                    'order_by'  => 'id',
                    'order_dir' => 'asc',
                    'search'    => ['title' => self::$model->title],
                    'select'    => ['id', 'title', 'route'],
                ]), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'collection',
                    'total'
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->collection[0]->id,
                'title' => $content->collection[0]->title,
                'route' => $content->collection[0]->route
            ]);
        });
    }

    /**
     * Test GET:api/users/permissions/{id}
     */
    public function testPermissionShowMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$model) {
                self::$model = factory($this->class)->create();
            }

            $response = $this->providePermissions(['users.permissions' => '8'])
                ->getJson(route('api.users.permissions.show', self::$model->id), [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'title',
                        'route',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->model->id,
                'title' => $content->model->title,
                'route' => $content->model->route,
            ]);
        });
    }

    /**
     * Test POST:api/users/permissions
     */
    public function testPermissionStoreMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $model = factory($this->class)->make();

            //Check middleware
            $this->providePermissions(['users.permissions' => '0'])
                ->post(route('api.users.permissions.store'), [
                    'title' => $model->title,
                    'route' => $model->route,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            $response = $this->providePermissions(['users.permissions' => '4'])
                ->post(route('api.users.permissions.store'), [
                    'title' => $model->title,
                    'route' => $model->route,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertCreated()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'title',
                        'route',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => $content->model->id,
                'title' => $content->model->title,
                'route' => $content->model->route,
            ]);
            self::$model = $this->class::find($content->model->id);
        });
    }

    /**
     * Test PATCH:api/users/permissions/{id}
     */
    public function testPermissionUpdateMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            if (!self::$model) {
                self::$model = factory($this->class)->create();
            }
            $model = factory($this->class)->make();

            //Check middleware
            $this->providePermissions(['users.permissions' => '0'])
                ->patch(route('api.users.permissions.update', self::$model->id), [
                    'title' => $model->title,
                    'route' => $model->route,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            $response = $this->providePermissions(['users.permissions' => '2'])
                ->patch(route('api.users.permissions.update', self::$model->id), [
                    'title' => $model->title,
                    'route' => $model->route,
                ], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertOk()
                ->assertJsonStructure([
                    'model' => [
                        'id',
                        'title',
                        'route',
                    ]
                ]);

            $content = json_decode($response->content());

            $this->assertDatabaseHas($this->table, [
                'id'    => self::$model->id,
                'title' => $model->title,
                'route' => $model->route,
            ]);

            self::$model = $this->class::find($content->model->id);
        });
    }

    /**
     * Test DELETE:api/users/permissions/{id}
     */
    public function testPermissionDestroyMethod()
    {
        $this->wrapApiTest(__FUNCTION__, function () {
            $model_ids = $this->class::where('title', 'like', 'test_permission_%')->pluck('id')->toArray();

            if (!$model_ids) {
                $model = factory($this->class)->create();
                $model_ids = [$model->id];
            }

            //Check middleware
            $this->providePermissions(['users.permissions' => '0'])
                ->patch(route('api.users.permissions.destroy', $model_ids[0]), [], [
                    'Authorization' => 'Bearer ' . self::$token
                ])->assertStatus(403);

            foreach ($model_ids as $model_id) {
                $this->providePermissions(['users.permissions' => '1'])
                    ->delete(route('api.users.permissions.destroy', $model_id), [], [
                        'Authorization' => 'Bearer ' . self::$token
                    ])->assertNoContent();

                $this->assertDatabaseMissing($this->table, [
                    'id' => $model_id
                ]);
            }
        });
        self::$token = null;
        self::$model = null;
    }
}