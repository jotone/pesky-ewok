@extends('admin.layouts.master')

@section('content')
    @include('admin.layouts.breadcrumbs')

    <div class="page-title">{{ $title }}</div>

    <form class="form-wrap flex-stretch-start"
          data-edit="{{ route('admin.users.permissions.edit', 0) }}"
          method="@isset($model){{ 'PUT' }}@else{{ 'POST' }}@endisset"
          action="@isset($model){{ route('api.users.permissions.update', $model->id) }}@else{{ route('api.users.permissions.store') }}@endisset">

        <div class="form-details active">
            <fieldset>
                <legend>Main data</legend>

                <div class="row">
                    <label>
                        <div class="caption">Title:</div>
                        <input name="title"
                               class="input-text col-1-2"
                               type="text"
                               value="{{ $model->title ?? '' }}"
                               placeholder="Title&hellip;">
                    </label>
                </div>
            </fieldset>

            <fieldset>
                <legend>Route</legend>
                <div class="row">
                    <label>
                        <div class="caption">Permission route:</div>
                        <select name="route" class="col-1-4">
                            @foreach($routes as $route => $link)
                                <option value="{{ $route }}" @if(isset($model) && $model->route == $route) selected @endisset>
                                    /{{ $link }}
                                </option>
                            @endforeach
                        </select>
                    </label>
                </div>
            </fieldset>
        </div>

        <aside class="form-assistant-wrap">
            <ul class="page-navigation"></ul>

            @if (isset($model))
                <div class="etc-info">
                    <span>Created at:&nbsp;</span>
                    <strong>{{ $model->created_at->format('d/M/Y H:i') }}</strong>
                </div>
                <div class="etc-info">
                    <span>Last update:&nbsp;</span>
                    <strong>{{ $model->updated_at->format('d/M/Y H:i') }}</strong>
                </div>
            @endif

            <div class="save-wrap">
                <button name="save" class="button green" type="submit" title="Save permission data">Save</button>
            </div>
        </aside>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('js/admin/scripts/permissions.min.js') }}" type="text/javascript"></script>
@endsection