/**
 * Get items list
 * @param collection
 * @returns {string}
 */
App.getList = collection => {
  let body = '';
  for (let i in collection) {
    body += Templates.permissions.render(collection[i]);
  }
  return body;
};
/**
 * Submit form
 */
App.form.submit = () => {
  App.requests.save({
    title: $('input[name="title"]').val().trim(),
    route: $('select[name="route"]').val().trim(),
  }).then(App.form.onSuccessSave);
};
/**
 * Form validation rules
 */
App.form.validation = {
  input: {
    title: ['required'],
  },
  select: {
    route: ['required']
  }
};

/**
 * Module messages
 * @type {{removeQuestion: string, removed: string, titleField: string, success: string, removeConfirm: string}}
 */
App.modules.messages = {
  success: 'Permission :attribute was successfully saved.',
  updated: 'Attribute ":attribute" was applied to permission.',
  removed: 'Permission :attribute was successfully removed.',
  removeQuestion: 'Do you really want to remove permission :attribute?',
  titleField: 'title'
};

Templates.permissions = {
  /**
   * Render table row
   * @param item
   * @returns {string}
   */
  render: item => Templates.itemsList.defaultTableWrap(item.id,
      '<td><span>' + (item.title || '') + '</span></td>' +
      '<td><span>' + item.route + '</span></td>' +
      '<td><span>' + item.created_at + '</span></td>')
};