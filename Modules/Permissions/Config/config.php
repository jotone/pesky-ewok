<?php

return [
    'name'        => 'Permissions',
    'menu'        => [
        'title'      => 'Permissions',
        'slug'       => 'permissions',
        'route'      => 'users.permissions',
        'image'      => 'fa-user-secret',
        'position'   => 30,
        'is_section' => false,
        'editable'   => true,
        'relate_to'  => 'users_permissions'
    ],
    'permissions' => [
        'title' => 'Permissions Page',
        'route' => 'users.permissions'
    ]
];
