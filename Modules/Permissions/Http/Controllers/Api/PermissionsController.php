<?php

namespace Modules\Permissions\Http\Controllers\Api;

use App\Source\Controllers\BasicApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Permissions\Http\Controllers\PermissionControllerTrait;

class PermissionsController extends BasicApiController
{
    use PermissionControllerTrait;

    /**
     * Get Permissions collection
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function index(Request $request)
    {
        return response($this->getCollection($request), 200);
    }

    /**
     * Get Permission by ID
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function show(int $id)
    {
        return $this->getModel($id);
    }

    /**
     * Create Permission
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function store(Request $request)
    {
        $args = $request->only('title', 'route');

        //Run validation
        $validator = Validator::make($args, [
            'title' => 'required|string',
            'route' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $model = $this->storeModel($args);

        return ($model)
            ? response(['model' => $model], 201)
            : response([
                'error' => 'Cannot write to database.'
            ], 500);
    }

    /**
     * Update specified Permission
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function update($id, Request $request)
    {
        $model = $this->controllerSettings['model']::find($id);

        if (!$model) {
            return response([], 404);
        }

        $args = $request->only('title', 'route');

        $rules = [];
        foreach ($args as $key => $val) {
            switch ($key) {
                case 'title':
                case 'route':
                    $rules[$key] = 'required|string';
                    $model->$key = $val;
                    break;
            }
        }

        //Run validation
        $validator = Validator::make($args, $rules);

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $model->save();

        return response([
            'model' => $model
        ]);
    }

    /**
     * Remove Permission by ID
     * @param int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function destroy($id)
    {
        return $this->removeModel($id);
    }
}