<?php

namespace Modules\Permissions\Http\Controllers\Admin;

use App\Source\Controllers\BasicAdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Permissions\Http\Controllers\PermissionControllerTrait;

class PermissionsController extends BasicAdminController
{
    use PermissionControllerTrait;

    /**
     * Controller request properties
     * @var array
     */
    protected $requestProperties = [
        'select' => ['id', 'title', 'route', 'created_at'],
        'with'   => []
    ];

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index(Request $request)
    {
        return $this->renderIndex($request, [
            //Page routes
            'routes'       => [
                'create' => route('admin.users.permissions.create'),
                'edit'   => route('admin.users.permissions.edit', 0),
                'list'   => route('api.users.permissions.index'),
                'remove' => route('api.users.permissions.destroy', 0),
            ],
            //Page title
            'title'        => 'Permissions list'
        ]);
    }

    /**
     * Show the form for creating a new permission resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function create(Request $request)
    {
        return view($this->controllerSettings['module'] . '::form', [
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //Routes list
            'routes'      => $this->buildRoute(),
            //Page title
            'title'       => 'Permission create'
        ]);
    }

    /**
     * Show the form for editing the specified permission resource.
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function edit(int $id, Request $request)
    {
        $model = $this->controllerSettings['model']::find($id);

        if (!$model) {
            abort(404);
        }

        return view($this->controllerSettings['module'] . '::form', [
            //Page breadcrumbs array
            'breadcrumbs' => $this->buildBreadcrumbs($request),
            //Editable model
            'model'       => $model,
            //Routes list
            'routes'      => $this->buildRoute(),
            //page title
            'title'       => 'Permission edit'
        ]);
    }

    /**
     * Get list of route
     * @return array
     */
    protected function buildRoute(): array
    {
        $route_collection = Route::getRoutes();
        $route = [];
        foreach ($route_collection as $value) {
            if (
                strpos($value->uri, '_ignition/') === false
                && isset($value->action['as'])
                && strpos($value->action['as'], '.index') !== false
            ) {
                $route_name = explode('.', $value->action['as']);

                if (in_array('index', $route_name)) {
                    $route[$value->action['as']] = $value->uri;
                }
            }
        }
        return $route;
    }
}
