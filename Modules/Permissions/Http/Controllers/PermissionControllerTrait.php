<?php

namespace Modules\Permissions\Http\Controllers;

use Modules\Permissions\Entities\Permission;

trait PermissionControllerTrait
{
    /**
     * DataTable columns
     * @var array
     */
    protected $tableBuilder = [
        [
            'id'     => 'id',
            'filter' => 'ID',
            'min'    => 1,
            'name'   => '#',
            'type'   => 'complete'
        ], [
            'name' => '<input name="selectAll" type="checkbox" class="input-checkbox" autocomplete="off"><span class="fa fa-cogs"></span>',
            'type' => 'title'
        ], [
            'id'   => 'title',
            'min'  => 3,
            'name' => 'Title',
            'type' => 'complete'
        ], [
            'id'   => 'route',
            'min'  => 3,
            'name' => 'Route',
            'type' => 'complete'
        ], [
            'id'   => 'created_at',
            'name' => 'Created At',
            'type' => 'directions'
        ]
    ];

    /**
     * Controller attributes
     * @var array
     */
    public $controllerSettings = [
        'model'  => Permission::class,
        'module' => 'permissions',
        'table'  => 'permissions',
    ];

    /**
     * Store model in database
     * @param $args
     * @return mixed
     */
    protected function storeModel($args)
    {
        return $this->controllerSettings['model']::create([
            'title' => $args['title'],
            'route' => $args['route'],
        ]);
    }
}