<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Roles\Entities\Role;

class Permission extends Model
{
    /**
     * Model table
     * @var string
     */
    protected $table = 'permissions';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    public $fillable = [
        'title',
        'route',
    ];

    /**
     * Get permission roles
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_permissions', 'permission_id', 'role_id')
            ->withPivot('value');
    }

    /**
     * Model extended behavior
     */
    public static function boot()
    {
        parent::boot();

        static::deleted(function ($model) {
            //Remove permission to role relations
            $model->roles()->detach();
        });
    }
}
