include .env

dc-up:
	@docker-compose up -d
	@sudo chmod -R 777 storage

dc-down:
	@docker-compose down

dc-build:
	@if [ -d "bootstrap/cache" ]; then sudo chown -R ${USER}:${USER} bootstrap/cache; fi
	@sudo chown -R ${USER}:${USER} app
	@sudo chown -R ${USER}:${USER} resources
	@sudo chmod -R 777 storage
	@docker-compose build
	@docker-compose up

dc-i:
	@composer update
	@php artisan key:generate
	@php artisan jwt:secret
	@php artisan migrate:reset
	@php artisan migrate
	@php artisan app:install
	@npm install
	@npm run dev

perm:
	@if [ -d "bootstrap/cache" ]; then sudo chown -R ${USER}:${USER} bootstrap/cache; fi
	@sudo chown -R ${USER}:${USER} storage
	@sudo chown -R ${USER}:${USER} app
	@sudo chown -R ${USER}:${USER} resources
	@sudo chmod -R 755 bootstrap
	@sudo chmod -R 777 storage

art:
	@docker exec pesky-ewok_php-fpm_1 php artisan ${ARGS}

reset:
	@php artisan migrate:reset
	@php artisan migrate
	@php artisan app:install
	@npm run dev

prod-i:
	@psql -U postgres -c "CREATE DATABASE ${DB_DATABASE};"
	@psql -U postgres -c "CREATE USER ${DB_USERNAME} WITH ENCRYPTED PASSWORD '${DB_PASSWORD}';"
	@psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE ${DB_DATABASE} TO ${DB_USERNAME};"
	@composer update
	@php artisan key:generate
	@php artisan jwt:secret
	@php artisan migrate:reset
	@php artisan migrate
	@php artisan app:install