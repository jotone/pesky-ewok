<div class="error-popup-wrap popup-wrap">
    <div class="close">
        <span class="fa fa-times"></span>
    </div>

    <div class="error-popup-content">
        <div class="error-title">
            <span class="error-method"></span>
            <span class="error-url"></span>
            <div class="error-status">
                Status: <span></span>
            </div>
        </div>

        <div class="error-content">
            <div class="error-file"></div>
            <div class="error-line"></div>
            <div class="error-text"></div>

            <ul class="error-messages-wrap"></ul>
        </div>
    </div>
</div>