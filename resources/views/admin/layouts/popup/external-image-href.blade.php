<div class="external-image-link-popup popup-wrap">
    <div class="close">
        <span class="fa fa-times"></span>
    </div>

    <div class="external-link-wrap">
        <div class="title">
            External image link
        </div>

        <input name="externalLink" class="input-text col-1" placeholder="Insert link here&hellip;">

        <div class="button-wrap"><button class="button green">OK</button></div>
    </div>
</div>