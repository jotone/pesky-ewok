<div class="column flex-center-between">
    <div class="caption">
        {!! $name !!}
    </div>
    <div class="directions">
        <div class="fa fa-sort-asc asc flex-center-center"></div>
        <div class="fa fa-sort-desc desc flex-center-center"></div>
    </div>
</div>