<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="jwt-token"
          content="{{ \Illuminate\Support\Facades\Cookie::has('jwt-token') ? \Illuminate\Support\Facades\Cookie::get('jwt-token') : '' }}">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{ asset('css/admin/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/datepicker.css') }}">
    @yield('styles')

    <script src="{{ asset('js/admin/libs.min.js') }}"></script>
    <title>Document</title>
</head>
<body>

@include('admin.layouts.header')

<div class="body-wrap flex-stretch-start">
    <nav class="side-menu col-md-15">
        <ul>
            @foreach($side_menu as $menu_item)
                @include('admin.layouts.side-menu', ['menu_item' => $menu_item])
            @endforeach
        </ul>
    </nav>

    <div class="content-wrap">
        @yield('content')
    </div>
</div>

<footer class="flex-center-end">
    <div class="messages-list col-1-5">
        <ul></ul>
    </div>
</footer>

<div class="overlay">
    @include('admin.layouts.popup.error')
    @include('admin.layouts.popup.external-image-href')
    @yield('popups')
</div>

<div class="preloader-wrap" style="display: none">
    <img src="{{ asset('img/admin/preload.gif') }}" alt="Подождите немного&hellip;">
</div>

<script src="{{ asset('js/admin/app.js') }}"></script>
<script src="{{ asset('js/admin/components.min.js') }}"></script>
@yield('scripts')
</body>
</html>