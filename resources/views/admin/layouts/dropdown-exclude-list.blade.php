<div class="dropdown-exclude-list col-1-2">
    <div class="dropdown-wrap">
        <input class="dropdown-field" type="text">
        <button class="dropdown-button" type="button">
            <span class="fa fa-caret-down"></span>
        </button>
        <ul>
            @foreach(array_diff($list, $selected) as $key => $value)
                <li data-id="{{ $key }}">{{ $value }}</li>
            @endforeach
        </ul>
        <button class="add-button" type="button">
            <span class="fa fa-plus-circle"></span>
        </button>
    </div>
    <ul class="dropdown-selected-list flex-center-start">
        @foreach($selected as $key => $value)
            <li data-id="{{ $key }}">
                <div class="drop fa fa-times-circle-o"></div>
                <span>{{ $value }}</span>
            </li>
        @endforeach
    </ul>
</div>