<header class="flex-center-between">
    <div class="logo col-md-15"></div>

    <div class="flex-center-between col-1">
        <button class="burger-button" name="menuExpand" type="button">
            <span class="fa fa-align-justify"></span>
        </button>

        <div class="welcome-wrap">
            <span>Welcome, {{ \Auth::user()->name ?? \Auth::user()->email }}</span>
            <a href="{{ route('auth.logout') }}">Log out</a>
        </div>
    </div>
</header>