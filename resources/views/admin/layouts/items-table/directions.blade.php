<div class="column">
    <div class="caption flex-center-around">{!! $name !!}</div>
    <div class="column-controls">
        <div class="directions">
            <div class="asc fa fa-sort-asc"></div>
            <div class="desc fa fa-sort-desc"></div>
        </div>
    </div>
</div>