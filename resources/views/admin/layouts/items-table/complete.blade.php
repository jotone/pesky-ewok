<div class="column">
    <div class="caption flex-center-around">{!! $name !!}</div>
    <div class="column-controls">
        <div class="search-wrap">
            <input class="search-input"
                   min="{{ $min ?? 0 }}"
                   value="@isset($filters['search'][$id]){{ $filters['search'][$id] }}@endisset"
                   placeholder="{!! $filter ?? 'Filter by ' . $name !!}&hellip;">
            <button class="search-expand" type="button">
                <span class="fa fa-search"></span>
            </button>
        </div>
        <div class="directions">
            <div class="asc fa fa-sort-asc"></div>
            <div class="desc fa fa-sort-desc"></div>
        </div>
    </div>
</div>