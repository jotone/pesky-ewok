@extends('admin.layouts.master')

@section('content')
    @include('admin.layouts.breadcrumbs')

    <div class="page-title">{{ $title }}</div>

    <div class="page-controls-wrap flex-center-between">
        <div class="flex-center-start buttons-wrap">
            @if (!isset($prevent['create']))
                <a class="button green" href="{{ $routes['create'] }}">Create</a>
            @endif
            @if (!isset($prevent['bulkDelete']))
                <button class="button red" name="bulkDelete" disabled>Bulk Delete</button>
            @endif
        </div>

        <div class="pagination-wrap">
            <ul class="flex-center-end"></ul>
        </div>
    </div>

    <table class="items-list"
           @foreach($routes as $key => $val)
           data-{{ $key }}="{{ $val }}"
           @endforeach
           data-filters="{{ json_encode($filters) }}">

        <thead>
        <tr>
            @foreach($tableBuilder as $col)
                <th @isset($col['id']) id="{{ $col['id'] }}" @endisset>
                    @include('admin.layouts.items-table.' . $col['type'], [
                        'id' => $col['id'] ?? null,
                        'filter' => $col['filter'] ?? null,
                        'name' => $col['name'],
                        'min' => $col['min'] ?? 1
                    ])
                </th>
            @endforeach
        </tr>
        </thead>

        <tbody>
        </tbody>
    </table>

    <div class="bottom-controls-wrap flex-center-between">
        <select name="perPage" title="Show items per page.">
            <option value="10" @if($filters['take'] == 10) selected="selected" @endif>10</option>
            <option value="25" @if($filters['take'] == 25) selected="selected" @endif>25</option>
            <option value="50" @if($filters['take'] == 50) selected="selected" @endif>50</option>
            <option value="75" @if($filters['take'] == 75) selected="selected" @endif>75</option>
            <option value="100" @if($filters['take'] == 100) selected="selected" @endif>100</option>
        </select>
        <div class="pagination-wrap">
            <ul class="flex-center-end"></ul>
        </div>
    </div>
@endsection