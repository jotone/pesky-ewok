<div class="breadcrumbs">
    <ul>
        @if (isset($breadcrumbs))
            @foreach($breadcrumbs as $item)
                @if ($item['name'])
                    <li>
                        @if ($item['route'])
                            <a href="{{ $item['route'] }}">{{ $item['name'] }}</a>
                        @else
                            <span>{{ $item['name'] }}</span>
                        @endif
                    </li>
                @endif
            @endforeach
        @endif
    </ul>
</div>