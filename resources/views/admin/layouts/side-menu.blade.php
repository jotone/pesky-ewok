<li>
    <a href="{{ ($menu_item['route'][0] != '#') ? route('admin.' . $menu_item['route']. '.index') : $menu_item['route'] }}"
       @if(($menu_item['route'][0] == '#')) class="arrow-right" @endif>
        @if ($menu_item['image'])
            <span class="fa {{ $menu_item['image'] }}"></span>
        @endif
        <span class="caption">{{ $menu_item['title'] }}</span>
    </a>

    @if(!empty($menu_item['sub_menus']))
        <ul>
            @foreach($menu_item['sub_menus'] as $menu)
                @include('admin.layouts.side-menu', ['menu_item' => $menu])
            @endforeach
        </ul>
    @endif
</li>
