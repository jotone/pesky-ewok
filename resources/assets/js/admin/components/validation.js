App.validation = {
  /**
   * Run validation with the response errors
   * @param errors
   */
  handle: errors => {
    for (let tag in errors) {
      $('form [name="' + tag + '"]').closest('.row').append(App.validation.render(errors[tag]))
    }
  },
  /**
   * Convert messages array to HTML
   * @param messages
   * @returns {string}
   */
  getMessages: messages => {
    let message = '';
    for (let i in messages) {
      message += '<p>' + messages[i] + '</p>';
    }
    return message;
  },
  /**
   * Validation message HTML
   * @param messages
   * @returns {string}
   */
  render: messages => '<div class="validation-error-wrap">' +
      '<div class="close"><span class="fa fa-times"></span></div>' +
      '<div class="validation-message-wrap">' +
      '<div class="messages">' + App.validation.getMessages(messages) + '</div>' +
      '</div>' +
      '</div>',
  /**
   * Run validation
   * @param validationRules
   * @param callback
   */
  run: (validationRules, callback) => {
    $(document).find('.validation-error-wrap').each(function () {
      $(this).remove();
    });
    let preventSubmit = false;

    for (let tag in validationRules) {
      for (let tagName in validationRules[tag]) {
        const value = $(tag + '[name="' + tagName + '"]').val().trim();

        const rules = validationRules[tag][tagName];
        let messages = [];

        for (let i in rules) {
          const rule = rules[i].split(':');

          switch (rule[0]) {
            case 'required':
              if (value === '' || value == null) {
                //showMessage
                messages.push('This field is required.');
              }
              break;
            case 'same':
              if (value !== $(rule[1]).val().trim() && value !== '') {
                //showMessage
                const str = $(rule[1]).closest('label').find('.caption').text();
                messages.push('This field must be same as ' + str.substring(0, str.length))
              }
              break;
            case 'format':
              const regexp = new RegExp(rule[1]);

              if (!regexp.test(value) && value !== '') {
                //showMessage
                messages.push('This field must have format <strong>' + rule[2] + '</strong>')
              }
              break;
          }
        }

        let message = App.validation.render(messages);

        if (messages.length) {
          preventSubmit = true;
          const $node = $(tag + '[name="' + tagName + '"]');

          if ($node.closest('.row').length) {
            $node.closest('.row').append(message);
            App.common.goto($node);
          }
        }
      }
    }

    if (!preventSubmit) {
      callback();
    }
  }
};

$(document).ready(() => {
  $('form').on('click', '.validation-error-wrap .close', function () {
    $(this).closest('.validation-error-wrap').remove();
  });
});
