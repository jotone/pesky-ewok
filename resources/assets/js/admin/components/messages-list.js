App.storage = {
  /**
   * Set messages to local storage
   * @param messages
   */
  addMessages: messages => {
    let messageList = (localStorage.getItem('messages') && App.common.isJson(localStorage.getItem('messages')))
        ? JSON.parse(localStorage.getItem('messages'))
        : [];

    messageList = JSON.stringify(messageList.concat(messages));

    localStorage.setItem('messages', messageList);
  },
  /**
   * Get messages, then remove them from storage
   * @returns {any | []}
   */
  flushMessages: () => {
    let messageList = (App.common.isJson(localStorage.getItem('messages')))
        ? JSON.parse(localStorage.getItem('messages'))
        : [];

    localStorage.removeItem('messages');

    return messageList;
  }
};

App.modules.messagesList = {
  /**
   * Get and show messages from local storage
   */
  applyMessages: () => {
    const messagesList = App.storage.flushMessages();
    for (let i in messagesList) {
      App.modules.messagesList.run(messagesList[i])
    }
  },
  /**
   * Hide messages
   */
  hideMessages: () => {
    $('.messages-list ul li').each(function () {
      $(this).show(500).delay(6000).hide(500, function () {
        $(this).remove()
      });
    })
  },
  /**
   * View message
   * @param data
   */
  run: data => {
    for (let type in data) {
      $('.messages-list ul').append(App.modules.messagesList.render(type, data[type]));
      App.modules.messagesList.hideMessages();
    }
  },
  //Create message html
  render: (type, message) => '<li class="message-' + type + '">' +
      '<span>' + message + '</span>' +
      '<div class="close flex-center-center"><span class="fa fa-times"></span></div>' +
      '</li>'
};

App.modules.messagesList.events = {
  '.messages-list': [
    {
      /**
       * Hide message
       */
      action: 'on.click',
      item: 'li .close',
      callback: function () {
        $(this).closest('li').remove();
      }
    }
  ]
};

$(document).ready(() => {
  App.modules.messagesList.applyMessages();
});