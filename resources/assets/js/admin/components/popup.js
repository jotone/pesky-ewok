App.modules.popup = {
  /**
   * Open error popup
   * @param url
   * @param method
   * @param status
   * @param response
   */
  error: (url, method, status, response = {}) => {
    $('.error-popup-wrap .error-method').text(method);
    $('.error-popup-wrap .error-url').text(url);
    $('.error-popup-wrap .error-status span').text(status);

    if (typeof response.file != 'undefined') {
      $('.error-popup-wrap .error-file').text(response.file)
    }

    if (typeof response.line != 'undefined') {
      $('.error-popup-wrap .error-line').text(response.line)
    }

    if (typeof response.message != 'undefined') {
      $('.error-popup-wrap .error-message').text(response.message)
    }

    if (typeof response.trace != 'undefined') {
      $('.error-popup-wrap .error-messages-wrap').html(App.modules.popup.render(response.trace));
    }

    App.modules.popup.open('.error-popup-wrap');
  },
  /**
   * Close popup
   * @param $this
   */
  close: $this => {
    $this.closest('.popup-wrap').hide();
    if (!$('.popup-wrap:visible').length) {
      $('.overlay').hide();
    }
  },
  /**
   * Open popup
   * @param {string} popup
   */
  open: popup => {
    $('.popup-wrap').hide();
    App.modules.popup.openNext(popup);
  },
  openNext: popup => {
    $('.overlay').show();
    $(popup).show();
  },
  /**
   * Create popup message item
   * @param messages
   * @returns {string}
   */
  render: messages => {
    let text = '';
    for (let i in messages) {
      let message = '';
      if (typeof messages[i].class != 'undefined') {
        message += '<p><strong>class</strong>: ' + messages[i].class + '</p>';
      }

      if (typeof messages[i].file != 'undefined') {
        message += '<p><strong>file</strong>: ' + messages[i].file + '</p>';
      }

      if (typeof messages[i].function != 'undefined') {
        message += '<p><strong>function</strong>: ' + messages[i].function + '</p>';
      }

      if (typeof messages[i].line != 'undefined') {
        message += '<p><strong>line</strong>: ' + messages[i].line + '</p>';
      }

      text += '<li class="error-message">' + message + '</li>';
    }

    return text;
  }
};

$(document).ready(() => {
  /**
   * Close popup
   */
  $('.popup-wrap').on('click', '.close', function () {
    App.modules.popup.close($(this))
  });
});