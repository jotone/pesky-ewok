App.modules.dropdownExcludeList = {
  hideList: () => $('.dropdown-exclude-list .dropdown-wrap ul').fadeOut(100)
};

App.modules.dropdownExcludeList.events = {
  '.dropdown-exclude-list': [{
    /**
     * Show list
     */
    action: 'on.click',
    item: 'button.dropdown-button',
    callback: function () {
      const $list = $(this).closest('.dropdown-wrap').find('ul');
      if ($list.is(':visible')) {
        $list.fadeOut(100);
      } else {
        $list.fadeIn(200);
      }
    }
  }, {
    /**
     * Apply item
     */
    action: 'on.click',
    item: '.dropdown-wrap li',
    callback: function () {
      $(this).closest('.dropdown-exclude-list').find('input.dropdown-field').val($(this).text())
             .attr('data-id', $(this).attr('data-id'));
      App.modules.dropdownExcludeList.hideList();
    }
  }, {
    /**
     * Add item to list
     */
    action: 'on.click',
    item: 'button.add-button',
    callback: function () {
      const $parent = $(this).closest('.dropdown-exclude-list');
      const $input = $parent.find('input.dropdown-field');
      if ($input.attr('data-id') !== 'undefined') {
        const item = {
          id: $input.data('id'),
          text: $parent.find('.dropdown-wrap ul li[data-id="' + $input.attr('data-id') + '"]').text()
        };

        $parent.find('.dropdown-selected-list').append(Templates.dropDown.selected(item));
        $parent.find('.dropdown-wrap ul li[data-id="' + $input.attr('data-id') + '"]').remove();
        $input.val('').attr('data-id', '');
      }
    }
  }],
  '.dropdown-selected-list': [{
    /**
     * Remove item from list
     */
    action: 'on.click',
    item: 'li .drop',
    callback: function () {
      const $parent = $(this).closest('.dropdown-exclude-list');
      const item = {
        id: $(this).closest('li').data('id'),
        text: $(this).next('span').text()
      };

      $parent.find('.dropdown-wrap ul').append(Templates.dropDown.row(item));
      $(this).closest('li').remove();
    }
  }]
};

Templates.dropDown = {
  row: item => '<li data-id="' + item.id + '">' + item.text + '</li>',
  selected: item => '<li data-id="' + item.id + '">' +
      '<div class="drop fa fa-times-circle-o"></div><span>' + item.text + '</span>' +
      '</li>'
};

$(document).on('click', e => {
  if (!$(e.target).closest('.dropdown-exclude-list').length) {
    App.modules.dropdownExcludeList.hideList();
  }
});