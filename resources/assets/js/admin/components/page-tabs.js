App.modules.pageTabs = {
  events: {
    '.tabs-wrap': [{
      /**
       * Click on tab
       */
      action: 'on.click',
      item: 'li',
      callback: function () {
        if (!$(this).hasClass('active')) {
          //Remove active classes from menu items
          $(this).closest('.tabs-wrap').find('li').removeClass('active');
          //Remove active classes from forms
          $('.form-wrap .form-details').removeClass('active');
          //Tab ID
          const $tab = $(this).attr('id');
          //Show current tab
          $(this).addClass('active');
          //Show current form
          $('.form-wrap .form-details#' + $tab).addClass('active')
        }
      }
    }]
  }
};