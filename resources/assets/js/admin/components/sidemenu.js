App.modules.sideMenu = {
  /**
   * Collapse or expand sidebar menu
   */
  collapseExpandMenu: () => {
    let collapse;
    //Toggle menu .collapse class
    if ($('body').hasClass('collapse')) {
      collapse = 0;
      $('body').removeClass('collapse');
    } else {
      collapse = 1;
      $('body').addClass('collapse');
    }

    App.requests.custom('/api/settings/' + App.siteSettings.user_id + '/upgrade', 'PATCH', {
      key: 'sidemenu',
      value: collapse
    });
  },
  /**
   * Open submenu on page load
   */
  openSubmenu: () => {
    //Get page route
    let route = window.location.href.split('#');
    //Remove last slash symbol
    if (route[0].substr(-1) === '/') {
      route[0] = route[0].substr(0, route[0].length - 1);
    }
    //Get link value from route
    const link = route[0].replace(/(\/create|\/\d+\/edit)/, '');
    //Check menu
    $('.side-menu ul li').each(function () {
      if (link === $(this).children('a').attr('href')) {
        if ($(this).parent('ul').closest('li').length) {
          //Open sub menu
          App.modules.sideMenu.submenuShow($(this).parent('ul').closest('li').children('a'))
        }
        $(this).children('a').addClass('selected');
      }
    });
  },
  /**
   * Hide item submenu
   * @param obj
   */
  submenuHide: (obj) => {
    obj.attr('class', 'arrow-right');
    obj.closest('li').children('ul').removeClass('active');
  },
  /**
   * Show item submenu
   * @param obj
   */
  submenuShow: (obj) => {
    obj.attr('class', 'arrow-down');
    obj.closest('li').children('ul').addClass('active');
  }
};

App.modules.sideMenu.events = {
  'header button[name="menuExpand"]': [
    {
      /**
       * Click burger menu button
       */
      action: 'click',
      callback: App.modules.sideMenu.collapseExpandMenu
    }
  ],
  '.side-menu': [
    {
      /**
       * Collapse/expand main menu
       */
      action: 'on.click',
      item: '.arrow-right, .arrow-down',
      callback: function (e) {
        e.preventDefault();
        if ($(this).hasClass('arrow-right')) {
          App.modules.sideMenu.submenuShow($(this));
        } else {
          App.modules.sideMenu.submenuHide($(this));
        }
      }
    }
  ]
};

$(document).ready(() => {
  //Open submenu
  App.modules.sideMenu.openSubmenu();
});