/**
 * Get list of resources
 * @returns {jQuery|{getAllResponseHeaders: (function(): *), abort: (function(*=): jqXHR), setRequestHeader: (function(*=, *): jqXHR), readyState: number, getResponseHeader: (function(*): *), overrideMimeType: (function(*): jqXHR), statusCode: (function(*=): jqXHR)}|HTMLElement|(function(*=, *=): *)|(function(*=, *=): *)|(function(*=, *=): *)|*}
 */
App.requests.list = () => {
  const url = $('.items-list').data('list');
  return $.ajax({
    url: url,
    method: 'GET',
    headers: {Authorization: 'Bearer ' + $('meta[name="jwt-token"]').attr('content')},
    data: App.modules.itemsList.filter,
    beforeSend: function () {
      $('.preloader-wrap').show();
    },
    success: function (response, status, request) {
      $('.preloader-wrap').hide();
      $('.items-list tbody').empty().append(App.getList(response.collection));
      App.modules.itemsList.rebuildPaginationList(response);
    },
    error: (xhrError) => {
      $('.preloader-wrap').hide();
      App.requests.processErrors(url, 'GET', xhrError)
    }
  });
};

App.modules.itemsList = {
  filter: {
    order: [{
      by: 'id',
      dir: 'desc'
    }],
    take: App.siteSettings.take,
    page: 1
  },
  /**
   * Module initialization behavior
   */
  init: () => {
    if ($('.items-list').length) {
      App.modules.itemsList.filter = $('.items-list').data('filters');
      App.modules.itemsList.resetTableFilters();
      App.requests.list().then(() => {
        $('.items-list thead th').each(function () {
          if ($(this).find('.search-input').length && $(this).find('.search-input').val().length > 0) {
            App.modules.itemsList.expandThSearch($(this));
          }
        })
      });
    }
  },
  /**
   * Figure out pagination values
   * @param {int} page
   * @param {int} take
   * @param {int} totalCount
   * @returns {{current: *, total: number, last: number, start: number}}
   */
  countPaginationValues(page, take, totalCount) {
    //Get total pages count
    const total = Math.ceil(totalCount / take);
    //Get approximate start page number
    let start = page - 3;
    //Get approximate last page number
    let last = page + 3;

    //Check if start page number is less than 0
    if (start < 1) {
      //Set shift value for last page
      let shift = Math.abs(start) + 1;
      //Set start page is 1
      start = 1;
      //Apply shift value
      last += shift;
    }

    //Check if last page number is greater than total pages count
    if (last > total) {
      last = total;
    }

    //Check if current page value is nit shifted
    if (page >= (total - 3)) {
      //Get shift value
      let shift = total - page;
      //Apply shift
      start -= 3 - shift;
      //Check if start page number is not less than 1
      if (start < 1) {
        start = 1;
      }
    }

    return {
      current: page,
      start: start,
      last: last,
      total: total
    }
  },
  /**
   * Show search field in table head column
   * @param $this
   */
  expandThSearch: $this => {
    const width = Math.ceil($this.width() * 0.7);
    $this.find('.search-input').animate({'width': width});
    $this.find('.column').addClass('active')
  },
  /**
   * Rebuild pagination list on change items per page value
   * @param response
   */
  rebuildPaginationList: response => {
    const filter = App.modules.itemsList.countPaginationValues(
        parseInt(App.modules.itemsList.filter.page),
        parseInt(App.modules.itemsList.filter.take),
        parseInt(response.total)
    );

    let query = '';
    if (typeof App.modules.itemsList.filter.search !== 'undefined') {
      for (let i in App.modules.itemsList.filter.search) {
        query += '&search[' + i + ']=' + App.modules.itemsList.filter.search[i];
      }
    }

    const url = location.protocol + '//' + location.host + location.pathname + '?';

    $('.pagination-wrap ul').html(Templates.itemsList.pagination(filter, url, query))
  },
  /**
   * Reset order filters in items-list table
   */
  resetTableFilters: () => {
    $('.items-list thead tr th').each(function () {
      $(this).find('.asc, .desc').removeClass('active');
    });
    const filter = App.modules.itemsList.filter;
    $('.items-list thead tr th#' + filter.order_by + ' .' + filter.order_dir).addClass('active');
  },
  /**
   * Remove table row on item removing
   * @param data
   */
  removeHandler: data => {
    if (data.request.status === 204) {
      data.element.closest('tbody').empty();
      App.requests.list();
    }
  },
  /**
   * Change "disabled" status for bulk delete button
   */
  toggleBulkDeleteDisable: () => {
    const $bulkDelBtn = $('.page-controls-wrap button[name="bulkDelete"]');
    if ($bulkDelBtn.length) {
      if ($('.items-list tbody').find('input[name="action"]:checked').length) {
        $bulkDelBtn.removeAttr('disabled')
      } else {
        $bulkDelBtn.attr('disabled', 'disabled')
      }
    }
  }
};

App.modules.itemsList.events = {
  '.items-list': [{
    /**
     * Click search icon
     */
    action: 'on.click',
    item: '.search-expand',
    callback: function () {
      if ($(this).closest('.column').hasClass('active')) {
        $(this).closest('.search-wrap').find('.search-input').animate({'width': 0}, function () {
          $(this).hide();
        }).val('');
        $(this).closest('.column').removeClass('active');
        App.modules.itemsList.filter.search = null;
        App.modules.itemsList.resetTableFilters();
        App.requests.list();
      } else {
        App.modules.itemsList.expandThSearch($(this).closest('th'))
      }
    }
  }, {
    /**
     * Table sorting
     */
    action: 'on.click',
    item: '.asc, .desc',
    callback: function () {
      if (!$(this).hasClass('active')) {
        $(this).closest('thead').find('.active').removeClass('active');
        $(this).addClass('active');

        App.modules.itemsList.filter.order_by = $(this).closest('th').attr('id');
        App.modules.itemsList.filter.order_dir = $(this).hasClass('asc')
            ? 'asc'
            : 'desc';

        App.modules.itemsList.resetTableFilters();
        App.requests.list();
      }
    }
  }, {
    /**
     * Run search
     */
    action: 'on.keyup',
    item: '.search-input',
    debounce: 1000,
    callback: function () {
      let fields = {};
      $(this).closest('thead').find('th').each(function () {
        if ($(this).find('.search-wrap').length) {
          const min = (typeof $(this).find('.search-input').attr('min') !== 'undefined')
              ? $(this).find('.search-input').attr('min')
              : 0;

          const value = $(this).find('.search-input').val().trim();

          if (value.length >= min) {
            const id = $(this).closest('th').attr('id');
            fields[id] = value;
          }
        }
      });

      App.modules.itemsList.filter.search = fields;
      App.requests.list();
    }
  }, {
    /**
     * Remove table item
     */
    action: 'on.click',
    item: '.remove',
    callback: function (e) {
      e.preventDefault();
      const tdPos = $(this).closest('table').find('thead th#' + App.modules.messages.titleField).index();
      const attribute = $(this).closest('tr').find('td:eq(' + tdPos + ') span').text();

      let res = confirm(App.common.prepareMessage(App.modules.messages.removeQuestion, attribute));

      if (typeof App.modules.messages.removeConfirm !== 'undefined') {
        res = prompt(App.modules.messages.removeConfirm);

        res = (res === attribute)
      }

      if (res) {
        const url = $(this).attr('href');
        const $this = $(this);

        App.requests.remove($this, url).then(data => {
          //Set remove message to local storage
          App.storage.addMessages([{
            warning: App.common.prepareMessage(App.modules.messages.removed, attribute)
          }]);

          //View saved messages
          App.modules.messagesList.applyMessages();
          App.modules.itemsList.removeHandler(data);
        });
      } else {
        App.storage.addMessages([{
          error: 'Entered value has mismatch with entered ' + App.modules.messages.titleField
        }]);
        //View saved messages
        App.modules.messagesList.applyMessages();
      }
    }
  }, {
    /**
     * Check "select all" checkbox
     */
    action: 'on.change',
    item: 'input[name="selectAll"]',
    callback: function () {
      let status = $(this).prop('checked');
      $('.items-list tbody input[name="action"]').prop('checked', status);
      App.modules.itemsList.toggleBulkDeleteDisable();
    }
  }, {
    /**
     * Check checkbox in table
     */
    action: 'on.change',
    item: 'input[name="action"]',
    callback: () => {
      $('.items-list input[name="selectAll"]').prop('checked', false);
      App.modules.itemsList.toggleBulkDeleteDisable();
    }
  }],
  '.bottom-controls-wrap': [{
    /**
     * Change page items number
     */
    action: 'change',
    item: 'select[name="perPage"]',
    callback: () => {
      App.modules.itemsList.filter.take = $('select[name="perPage"]').val();
      App.requests.custom('/api/settings/' + App.siteSettings.user_id + '/upgrade', 'PATCH', {
        key: 'take',
        value: App.modules.itemsList.filter.take
      });
      App.requests.list();
    }
  }],
  '.page-controls-wrap': [{
    /**
     * Bulk delete items
     */
    action: 'on.click',
    item: 'button[name="bulkDelete"]',
    callback: function () {
      if (typeof $(this).attr('disabled') == 'undefined') {
        const res = confirm('Please confirm action.');
        if (res) {
          $('.items-list tbody tr').each(function () {
            if ($(this).find('input[name="action"]').prop('checked')) {
              const $this = $(this);
              const url = $this.find('.remove').attr('href');
              const tdPos = $this.closest('table').find('thead th#' + App.modules.messages.titleField).index();
              const attribute = $this.closest('tr').find('td:eq(' + tdPos + ') span').text();
              //Set remove message to local storage
              App.storage.addMessages([{
                warning: App.common.prepareMessage(App.modules.messages.removed, attribute)
              }]);

              App.requests.remove($this, url).then(response => {
                App.modules.itemsList.removeHandler(response)
              });
            }
          });

          //View saved messages
          App.modules.messagesList.applyMessages();
        }
      }
    }
  }],
};

Templates.itemsList = {
  /**
   * Create pagination list
   * @param filter
   * @param {string} url
   * @param {string} query
   * @returns {string}
   */
  pagination: (filter, url, query) => {
    let html = '';
    if (filter.current > 1) {
      html += '<li><a href="' + url + 'page=1' + query + '" class="fa fa-angle-double-left"></a></li>' +
          '<li><a href="' + url + 'page=' + (filter.current - 1) + query + '" class="fa fa-angle-left"></a></li>';
    }

    for (let i = filter.start; i <= filter.last; i++) {
      html += '<li><a href="' + url + 'page=' + i + query + '"';
      if (i === filter.current) html += ' class = "active"';
      html += '>' + i + '</a></li>';
    }
    if (filter.current < filter.total) {
      html += '<li><a href="' + url + 'page=' + (filter.current + 1) + query + '" class="fa fa-angle-right"></a></li>' +
          '<li><a href="' + url + 'page=' + filter.total + query + '" class="fa fa-angle-double-right"></a></li>'
    }

    return html;
  },
  /**
   * Default row wrap for items table
   * @param {int} id
   * @param {string} body
   * @returns {string}
   */
  defaultTableWrap: (id, body) => {
    const edit = App.common.setIdToEditLink($('.items-list').data('edit'), id);
    const remove = App.common.setIdToRemoveLink($('.items-list').data('remove'), id);

    return '<tr><td><span>' + id + '</span></td>' +
        '<td><div class="actions flex-center-around">' +
        '<div><input name="action" type="checkbox" class="input-checkbox" autocomplete="off" data-content="' + id + '"></div>' +
        '<div><a class="fa fa-pencil edit" href="' + edit + '" title="Edit"></a></div>' +
        '<div><a class="fa fa-times remove" href="' + remove + '" title="Remove"></a></div>\n' +
        '</div></td>' + body + '</tr>';
  }
};