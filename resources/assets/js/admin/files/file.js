App.modules.file = {
  stub: '/img/admin/image-stub.png',
  /**
   * File form replacer
   * @param selector
   * @param value
   */
  replace: (selector, value = null) => {
    const $parent = $(document).find(selector);
    if (typeof App.modules.file[$parent.data('type')] !== 'undefined') {
      App.modules.file[$parent.data('type')].init($parent, value);
    }
  },
  requests: {
    /**
     * Create file
     * @param data
     * @param entity
     * @param entity_id
     * @returns {jQuery|{getAllResponseHeaders: (function(): *), abort: (function(*=): jqXHR), setRequestHeader: (function(*=, *): jqXHR), readyState: number, getResponseHeader: (function(*): *), overrideMimeType: (function(*): jqXHR), statusCode: (function(*=): jqXHR)}|HTMLElement|(function(*=, *=): *)|*}
     */
    create: (data, entity, entity_id) => {
      return App.requests.custom(App.siteSettings.links.files.store, 'POST', {
        files: [data],
        entity: entity,
        entity_id: entity_id
      });
    },
    /**
     * Update file data
     * @param data
     * @param id
     * @returns {jQuery|{getAllResponseHeaders: (function(): *), abort: (function(*=): jqXHR), setRequestHeader: (function(*=, *): jqXHR), readyState: number, getResponseHeader: (function(*): *), overrideMimeType: (function(*): jqXHR), statusCode: (function(*=): jqXHR)}|HTMLElement|(function(*=, *=): *)|*}
     */
    update: (data, id) => {
      const url = App.common.setIdToRemoveLink(App.siteSettings.links.files.update, id);
      return App.requests.custom(url, 'PUT', data);
    },
    /**
     * Remove file
     * @param id
     * @returns {jQuery|{getAllResponseHeaders: (function(): *), abort: (function(*=): jqXHR), setRequestHeader: (function(*=, *): jqXHR), readyState: number, getResponseHeader: (function(*): *), overrideMimeType: (function(*): jqXHR), statusCode: (function(*=): jqXHR)}|HTMLElement|(function(*=, *=): *)|*}
     */
    remove: (id) => {
      const url = App.common.setIdToRemoveLink(App.siteSettings.links.files.destroy, id);
      return App.requests.custom(url, 'DELETE', {});
    }
  },
  controls: {
    /**
     * Open file dialog
     * @param selector
     */
    uploadFile: selector => {
      selector.on('click', 'button[name="upload"]', function () {
        $(this).closest('.form-item-container').find('input[type="file"]').trigger('click');
      });
    },
    /**
     * Upload external image
     * @param selector
     * @param $this
     */
    uploadExternalImage: (selector, $this) => {
      $this.addClass('selected');
      App.modules.popup.openNext('.external-image-link-popup');
      $('.external-image-link-popup input[name="externalLink"]').focus();
      selector.find('.file-preview .file-details').show();
    }
  }
};

Templates.file = {
  fileDetails: '<div class="file-details" title="View details"><div class="fa"></div>' +
    '<div class="form-details-wrap"><div class="row">' +
    '<div class="caption">Image alt attribute:</div>' +
    '<input name="img_alt" class="input-text col-md-95" title="Image alt attribute" placeholder="Alt&hellip;">' +
    '</div><div class="row">' +
    '<div class="caption">Image caption:</div>' +
    '<input name="img_caption" class="input-text col-md-95" title="Image caption" placeholder="Caption&hellip;">' +
    '</div><div class="row">' +
    '<div class="caption">Image description:</div>' +
    '<textarea name="img_description" class="textarea-small col-md-95" title="Image description" placeholder="Description&hellip;"></textarea>' +
    '</div></div></div>',
};