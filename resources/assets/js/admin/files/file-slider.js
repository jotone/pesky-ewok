App.modules.file.slider = {
  init: (selector, value = null) => {
    if (selector) {
      selector.html(Templates.file.slider.wrap());
    }

    App.modules.file.slider.controls.fileDetails(selector);
    App.modules.file.slider.controls.fileSelectedMultiple(selector);
    App.modules.file.slider.controls.moveSlide(selector);
    App.modules.file.slider.controls.removeSlide(selector);
    App.modules.file.slider.controls.sliderListControls(selector);
    App.modules.file.slider.controls.sliderWindowControls(selector);
    App.modules.file.slider.controls.uploadExternalImage(selector);
    App.modules.file.controls.uploadFile(selector);

    if (value) {
      const $slider = selector.find('.slider-preview');
      const $items = selector.find('.slider-images-list');
      for (let i in value) {
        const uid = App.common.uniquid();
        const isFirst = ('0' === i);
        if (typeof value[i].type !== 'undefined') {
          $slider.append(Templates.file.slider.sliderSlide(uid, value[i].src, value[i].src, isFirst));
          $items.append(Templates.file.slider.sliderItem(uid, value[i].src, '', isFirst));
        } else {
          $slider.append(Templates.file.slider.sliderSlide(uid, value[i].src, value[i].name, isFirst, 'data-upload=""'));
          $items.append(Templates.file.slider.sliderItem(uid, value[i].name, '', isFirst));
        }
        $items.find('li[data-uid="' + uid + '"]').attr('data-info', JSON.stringify({
          title: value[i].title,
          caption: value[i].caption,
          description: value[i].description
        }));
        if (isFirst) {
          selector.find('input[name="img_alt"]').val(value[i].title);
          selector.find('input[name="img_caption"]').val(value[i].caption);
          selector.find('textarea[name="img_description"]').val(value[i].description);
        }
      }
      selector.find('.file-details').removeAttr('style');
    }
  },
  info: {
    /**
     * Get slide info
     * @param selector
     */
    get: selector => {
      selector.find('.slider-images-list .active').attr('data-info', JSON.stringify({
        title: selector.find('input[name="img_alt"]').val().trim(),
        caption: selector.find('input[name="img_caption"]').val().trim(),
        description: selector.find('textarea[name="img_description"]').val().trim()
      }));
    },
    /**
     * Set slide info
     * @param selector
     */
    set: selector => {
      const info = (typeof selector.find('.slider-images-list li.active').attr('data-info') === 'undefined')
        ? {
          title: '',
          caption: '',
          description: ''
        }
        : JSON.parse(selector.find('.slider-images-list li.active').attr('data-info'));

      selector.find('input[name="img_alt"]').val(info.title);
      selector.find('input[name="img_caption"]').val(info.caption);
      selector.find('textarea[name="img_description"]').val(info.description);
    }
  },
  /**
   * Remove slide
   * @param $current
   */
  slideRemove: $current => {
    const $parent = $current.closest('.form-item-container');
    const currentUID = $current.attr('data-uid');
    const $next = ($current.next('li').length && $current.hasClass('active'))
      ? $current.next('li')
      : ($current.prev('li').length && $current.hasClass('active'))
        ? $current.prev('li')
        : null;

    $parent.find('li[data-uid="' + currentUID + '"]').remove();

    if ($next) {
      const nextUID = $next.attr('data-uid');
      $parent.find('li[data-uid="' + nextUID + '"]').addClass('active');
    } else {
      $parent.find('.file-details').hide();
    }
  },
  controls: {
    /**
     * View/hide file details
     * @param selector
     */
    fileDetails: selector => {
      selector.find('.form-item-container .file-details').hide();

      selector.on('click', '.file-details > .fa', function () {
        const $parent = $(this).closest('.file-details');
        if ($parent.hasClass('active')) {
          $parent.removeClass('active');
          $parent.animate({'height': '32px'}, 500);

          App.modules.file.slider.info.get($parent);
        } else {
          $parent.addClass('active');
          $parent.animate({'height': '100%'}, 500);
          App.modules.file.slider.info.set(selector);
        }
      });

      selector.on('keyup', 'input[name="img_alt"], input[name="img_caption"], textarea[name="img_description"]', debounce(function () {
        App.modules.file.slider.info.get(selector);
      }, 1000))
    },
    /**
     * Success file dialog for multiple files
     * @param selector
     */
    fileSelectedMultiple: selector => {
      selector.on('change', 'input[type="file"]', function () {
        const $this = $(this);
        const files = $this.prop('files');

        if (files.length) {
          $this.closest('.form-item-container').find('.slider-preview .active').removeClass('active');
          $this.closest('.form-item-container').find('.slider-images-list .active').removeClass('active');

          for (let i = 0; i < files.length; i++) {
            const reader = new FileReader();
            const type = files[i].type;
            if (
              typeof App.siteSettings[type] !== 'undefined'
              && (App.siteSettings[type] === 1 || App.siteSettings[type] === '1')
            ) {
              reader.onload = function (e) {
                const uid = App.common.uniquid();
                $this.closest('.form-item-container').find('.slider-preview')
                  .append(Templates.file.slider.sliderSlide(uid, e.target.result, files[i].name, (0 === i), 'data-upload=""'));

                $this.closest('.form-item-container').find('.slider-images-list')
                  .append(Templates.file.slider.sliderItem(uid, files[i].name, App.common.fileSize(files[i].size), (0 === i)))
              };

              reader.readAsDataURL(files[i]);
            } else {
              App.storage.addMessages([{
                error: 'File does not match allowed mime-type.'
              }]);
              //View saved messages
              App.modules.messagesList.applyMessages();
            }
          }
          selector.find('.file-details').show();
        }
      });
    },
    /**
     * Sorting slides
     * @param selector
     */
    moveSlide: selector => {
      new Sortable(selector.find('.slider-images-list')[0], {
        animation: 150,
        handle: '.move',
        onEnd: function (e) {
          const $el = selector.find('.slider-preview li[data-uid="' + $(e.clone).attr('data-uid') + '"]');

          if (e.newIndex === (selector.find('.slider-images-list li').length - 1)) {
            selector.find('.slider-preview').append($el);
          } else {
            selector.find('.slider-preview li').eq(e.newIndex).before($el);
          }
        }
      });
    },
    /**
     * Remove slide. Check click on "Clear" button or remove button in list
     * @param selector
     */
    removeSlide: selector => {
      selector.on('click', 'button[name="clear"], .slider-images-list .remove', function () {
        App.modules.file.slider.slideRemove($(this).hasClass('remove')
          ? $(this).closest('li')
          : selector.find('.slider-preview li.active'));
      });
    },
    /**
     * Slider elements click
     * @param selector
     */
    sliderListControls: selector => {
      selector.on('click', '.slider-images-list li', function (e) {
        if (!$(e.target).parent('.remove').length) {
          $(this).closest('.slider-images-list').find('.active').removeClass('active');
          $(this).addClass('active');

          selector.find('.slider-preview .active').removeClass('active');
          selector.find('.slider-preview li:eq(' + $(this).index() + ')').addClass('active');
          App.modules.file.slider.info.set(selector);
        }
      });
    },
    /**
     * Slider click left/right
     * @param selector
     */
    sliderWindowControls: selector => {
      selector.on('click', '.slide-left, .slide-right', function () {
        const $active = selector.find('.slider-preview li.active');
        $active.removeClass('active');

        //Save slide info
        App.modules.file.slider.info.get(selector);

        let $el;
        if ($(this).hasClass('slide-right')) {
          $el = ($active.next('li').length)
            ? $active.next('li')
            : selector.find('.slider-preview li:first');

        } else {
          $el = ($active.prev('li').length)
            ? $active.prev('li')
            : selector.find('.slider-preview li:last');
        }
        $el.addClass('active');

        selector.find('.slider-images-list .active').removeClass('active');
        selector.find('.slider-images-list li:eq(' + $el.index() + ')').addClass('active');

        App.modules.file.slider.info.set(selector);
      })
    },
    /**
     * Upload external image
     * @param selector
     */
    uploadExternalImage: selector => {
      selector.on('click', 'button[name="external"]', function () {
        $('.external-image-link-popup .button-wrap button').attr('name', 'sliderApply');
        App.modules.file.controls.uploadExternalImage(selector, $(this));

        $('.external-image-link-popup').on('click', 'button[name="sliderApply"]', function () {
          const $input = $(this).closest('.external-link-wrap').find('input[name="externalLink"]');
          const val = $input.val();
          const $selected = $('button[name="external"].selected');

          if (val.length > 0) {
            const uid = App.common.uniquid();

            $selected.closest('.form-item-container').find('.slider-preview li').removeClass('active');
            $selected.closest('.form-item-container').find('.slider-images-list li').removeClass('active');
            //Append slide
            const $slide = Templates.file.slider.sliderSlide(uid, val, val, true);
            const $slideContainer = $selected.closest('.form-item-container').find('.slider-preview');
            $slideContainer.append($slide);

            //Append list item
            const path = val.split('/');
            const item = Templates.file.slider.sliderItem(uid, path[path.length - 1], '', true);
            const $slideList = $selected.closest('.form-item-container').find('.slider-images-list');
            $slideList.append(item);
            $selected.closest('.form-item-container').find('.file-details').show();
          }

          $input.val('');
          App.modules.popup.close($(this));
        });
      });
    },
  }
};

Templates.file.slider = {
  /**
   * Slider image details
   * @param {string} uid
   * @param {string} name
   * @param {string} size
   * @param {boolean} active
   * @returns {string}
   */
  sliderItem: (uid, name = '', size = '', active = false) =>
    '<li class="flex-center-between ' + (active ? 'active' : '') + '" data-uid="' + uid + '">' +
    '<div class="name">' + name + '</div>' +
    '<div class="size">' + size + '</div>' +
    '<div class="flex-center-start">' +
    '<div class="move">' +
    '<span class="fa fa-ellipsis-v text-gray"></span>' +
    '<span class="fa fa-ellipsis-v text-gray" style="padding-left: 2px"></span>' +
    '</div>' +
    '<div class="remove"><span class="fa fa-times text-red"></span></div>' +
    '</div></li>'
  ,
  /**
   * Slider image
   * @param {string} uid
   * @param {string} src
   * @param {string} alt
   * @param {boolean} active
   * @param {string} etc
   * @returns {string}
   */
  sliderSlide: (uid, src, alt, active = false, etc = '') =>
    '<li class="' + (active ? 'active' : '') + '" data-uid="' + uid + '">' +
    '<div class="flex-center-center"><img src="' + src + '" alt="' + alt + '" ' + etc + '></div>' +
    '</li>',
  /**
   * Slider body
   * @returns {string}
   */
  wrap: () => '<div class="form-item-container">' +
    '<div class="slider-wrap flex-stretch-between col-1">' +
    '<div class="slider-preview-wrap">' +
    '<div class="slide-left flex-center-center"><span class="fa fa-angle-left"></span></div>' +
    '<ul class="slider-preview"></ul>' +
    Templates.file.fileDetails +
    '<div class="slide-right flex-center-center"><span class="fa fa-angle-right"></span></div>' +
    '</div><div class="slider-images-list-wrap">' +
    '<ul class="slider-images-list"></ul>' +
    '</div></div><div class="file-controls">' +
    '<input name="img_url" type="file" style="display: none" multiple="multiple">' +
    '<button name="upload" class="button blue" type="button" title="Click to open image upload menu.">Upload&hellip;</button>' +
    '<button name="external" class="button blue" type="button" title="Click to open menu.">Link&hellip;</button>' +
    '<button name="clear" class="button red" type="button" title="Click to clear image.">Clear&hellip;</button>' +
    '</div></div>',
};