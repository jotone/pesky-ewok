App.modules.file.image = {
  init: (selector, value = null) => {
    if (selector) {
      selector.html(Templates.file.image.wrap());
    }

    App.modules.file.stub = (typeof selector.attr('data-stub') !== 'undefined' && selector.data('stub') !== '')
      ? selector.data('stub')
      : App.modules.file.stub;

    selector.find('.file-wrap .file-details').before('<img src="' + App.modules.file.stub + '" alt="stub">');

    //Event listener
    App.modules.file.image.controls.clearFile(selector);
    App.modules.file.image.controls.fileDetails(selector);
    App.modules.file.image.controls.fileSelected(selector);
    App.modules.file.image.controls.uploadExternalImage(selector);
    App.modules.file.controls.uploadFile(selector);

    if (value || (typeof selector.attr('data-file') !== 'undefined' && selector.data('file') !== '')) {
      const $image = selector.find('.file-wrap img');
      let img;
      if (value) {
        img = value;
      } else {
        img = selector.data('file');
        img.src = img.url;
        img.alt = img.url;
      }

      $image.attr('src', img.src);
      if (typeof img.name !== 'undefined') {
        $image.attr('alt', img.name).attr('data-upload', '');
      } else {
        $image.attr('alt', img.src).removeAttr('data-upload');
      }
      selector.find('input[name="img_alt"]').val(img.title);
      selector.find('input[name="img_caption"]').val(img.caption);
      selector.find('textarea[name="img_description"]').val(img.description);
      selector.find('.file-preview .file-details').show();
    }
  },
  controls: {
    /**
     * Clear image
     * @param selector
     */
    clearFile: selector => {
      selector.on('click', 'button[name="clear"]', function () {
        const $parent = $(this).closest('.form-item-container');
        $parent.find('.file-wrap img').remove();
        $parent.find('.file-wrap .file-details').before('<img src="' + App.modules.file.stub + '" alt="stub">');
        $parent.find('.file-details').hide();
      });
    },
    /**
     * View/hide file details
     * @param selector
     */
    fileDetails: selector => {
      if (
        selector.find('.form-item-container').length
        && selector.find('.form-item-container .file-wrap img').attr('alt') === 'stub'
      ) {
        selector.find('.form-item-container .file-details').hide();
      }

      selector.on('click', '.file-details > .fa', function () {
        const $parent = $(this).closest('.file-details');
        if ($parent.hasClass('active')) {
          $parent.removeClass('active');
          $parent.animate({'height': '32px'}, 500);
        } else {
          $parent.addClass('active');
          $parent.animate({'height': '100%'}, 500);
        }
      });
    },
    /**
     * Success file dialog
     * @param selector
     */
    fileSelected: selector => {
      selector.on('change', 'input[type="file"]', function () {
        const $this = $(this);
        const reader = new FileReader();
        const type = $this.prop('files')[0].type;

        if (
          typeof App.siteSettings[type] !== 'undefined'
          && (App.siteSettings[type] === 1 || App.siteSettings[type] === '1')
        ) {
          reader.onload = function (e) {
            if ($this.closest('.file-preview').length) {
              if (!$this.closest('.file-preview').find('.file-wrap img').length) {
                $this.closest('.file-preview').find('.file-wrap .file-details').before('<img src="" alt="">');
              }
              $this.closest('.file-preview').find('.file-wrap img').attr('src', e.target.result)
                .attr('alt', $this.prop('files')[0].name).attr('data-upload', '');

              selector.find('.file-preview .file-details').show();
            }
          };

          reader.readAsDataURL($this.prop('files')[0]);
        } else {
          App.storage.addMessages([{
            error: 'File does not match allowed mime-type.'
          }]);
          //View saved messages
          App.modules.messagesList.applyMessages();
        }
      });
    },
    /**
     * Upload external image
     * @param selector
     */
    uploadExternalImage: selector => {
      selector.on('click', 'button[name="external"]', function () {
        $('.external-image-link-popup .button-wrap button').attr('name', 'imageApply');
        App.modules.file.controls.uploadExternalImage(selector, $(this));
      });

      $('.external-image-link-popup').on('click', 'button[name="imageApply"]', function () {
        const $input = $(this).closest('.external-link-wrap').find('input[name="externalLink"]');
        const val = $input.val();
        const $selected = $('button[name="external"].selected');

        if (val.length > 0) {
          const $imgContainer = $selected.closest('.file-preview').find('.file-wrap');
          if (!$imgContainer.children('img').length) {
            $imgContainer.find('.file-details').before('<img src="' + val + '" alt="' + val + '">');
          } else {
            $imgContainer.children('img').attr('alt', val).attr('src', val).removeAttr('data-upload');
          }
        }

        $input.val('');
        App.modules.popup.close($(this));
      });
    }
  }
};

Templates.file.image = {
  /**
   * Image upload body
   * @returns {string}
   */
  wrap: () => '<div class="form-item-container">' +
    '<div class="file-preview" data-type="image">' +
    '<div class="file-wrap flex-center-center">' + Templates.file.fileDetails + '</div>' +
    '<div class="file-controls">' +
    '<input name="img_url" type="file" style="display: none">' +
    '<button name="upload" class="button blue" type="button" title="Click to open image upload menu.">Upload&hellip;</button>' +
    '<button name="external" class="button blue" type="button" title="Click to open menu.">Link&hellip;</button>' +
    '<button name="clear" class="button red" type="button" title="Click to clear image.">Clear&hellip;</button>' +
    '</div></div></div>',
};