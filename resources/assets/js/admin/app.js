$.browser = {};
$.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
$.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
$.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
$.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

var Templates = {
  form: {
    /**
     * Form aside menu list
     * @param {string} title
     * @param {string} link
     * @returns {string}
     */
    render: (title, link) => {
      return '<li data-link="' + link + '">' + title + '</li>';
    },
    /**
     * Form expand/collapse item
     * @returns {string}
     */
    expandCollapse: () => '<div class="expand-collapse"><span class="fa fa-chevron-down"></span></div>'
  }
};

var App = {
  form: {
    onSuccessSave: (response, status, request) => {
      //Check if edit redirect exist
      if (!$.isEmptyObject(response) && 201 === parseInt(request.status)) {
        const $form = $('.form-wrap');
        if ($form.length && typeof $form.attr('data-edit') != 'undefined') {
          window.location = App.common.setIdToEditLink($form.data('edit'), response.model.id);
        }
      }
    }
  },
  /**
   * Page filter data
   */
  filters: {},
  /**
   * Month names
   */
  monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  /**
   * Site settings
   */
  siteSettings: {},
  /*
   * Application common functions
   */
  common: {
    /**
     * Convert binary data into hex
     * @param {string} value
     * @returns {string}
     */
    bin2hex: value => parseInt(value, 2).toString(16),
    /**
     * Convert hex data into binary
     * @param hex
     * @returns {string}
     */
    hex2bin: hex => (parseInt(hex, 16)).toString(2).padStart(4, '0'),
    /**
     * Get file basename
     * @param {string} str
     * @returns {string}
     */
    basename: (str) => {
      let base = str.substring(str.lastIndexOf('/') + 1);
      if (base.lastIndexOf(".") !== -1) base = base.substring(0, base.lastIndexOf("."));
      return base;
    },
    /**
     * Create aside navigation menu
     */
    buildAsideMenu: () => {
      //create menu items
      $('ul.page-navigation').empty();
      $('.form-details fieldset:visible').each(function () {
        const $this = $(this);
        //Prepare item
        const link = App.common.transliteration.str2url(
            $this.children('legend').text().replace(/(<[^>]*>|\n|\s+)/g, '')
        );
        //Add items to page navigation menu
        $('ul.page-navigation').append(
            Templates.form.render('#' + $this.children('legend').text().trim(), link)
        );
        $this.attr('data-link', link);

        //Build expand/collapse button
        if (!$this.find('.expand-collapse').length) {
          $this.append(Templates.form.expandCollapse());
        }
      })
    },
    /**
     * Nice file size
     * @param {int} size
     * @returns {string}
     */
    fileSize: size => {
      const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
      if (size > 0) {
        const i = parseInt(Math.floor(Math.log(size) / Math.log(1024)));
        return Math.round(size / Math.pow(1024, i), 2) + ' ' + sizes[i];
      }
      return '0 Byte';
    },
    /**
     * Scroll to element
     * @param href
     */
    goto: href => {
      const scroller = $.browser.webkit
          ? 'body'
          : 'html';

      const target = href.offset().top - 65;
      $(scroller).animate({scrollTop: target}, 500);
    },
    /**
     * Check if string is JSON
     * @param {string} str
     * @returns {boolean}
     */
    isJson: str => {
      try {
        JSON.parse(str);
      } catch (e) {
        return false;
      }
      return true;
    },
    /**
     * Replace attribute in string
     * @param {string} str
     * @param {string} attr
     * @returns {string|void|*}
     */
    prepareMessage: (str, attr) => {
      return str.replace(/\:attribute/, attr);
    },
    /**
     * Set id to edit link
     * @param link
     * @param id
     * @returns {string}
     */
    setIdToEditLink: (link, id) => {
      let resultLink = link.split('/');
      resultLink[resultLink.length - 2] = id;
      resultLink = resultLink.join('/');
      return resultLink;
    },
    /**
     * Set id to remove link
     * @param link
     * @param id
     * @returns {string}
     */
    setIdToRemoveLink: (link, id) => {
      let resultLink = link.split('/');
      resultLink[resultLink.length - 1] = id;
      resultLink = resultLink.join('/');
      return resultLink;
    },
    /**
     * Wrap string with tag
     * @param {string} str
     * @param {string} tag
     * @returns {string}
     */
    tag: (str, tag) => '<' + tag + '>' + str + '</' + tag + '>',
    /*
     * Transliteration to latin symbols
     */
    transliteration: {
      /**
       * Change entire strings to proper strings
       * @param str
       * @returns {string}
       */
      strToEn: (str) => {
        const converter = {
          'á': 'a', 'Á': 'a', 'à': 'a', 'À': 'a', 'ă': 'a', 'Ă': 'a', 'â': 'a', 'Â': 'a', 'å': 'a', 'Å': 'a',
          'ã': 'a', 'Ã': 'a', 'ą': 'a', 'Ą': 'a', 'ā': 'a', 'Ā': 'a', 'α': 'a', 'Α': 'a', 'ά': 'a', 'Ά': 'a',
          'ἁ': 'a', 'Ἁ': 'a', 'ἂ': 'a', 'Ἂ': 'a', 'ἃ': 'a', 'Ἃ': 'a', 'ἄ': 'a', 'Ἄ': 'a', 'ἀ': 'a', 'Ἀ': 'a',
          'ἅ': 'a', 'Ἅ': 'a', 'ἆ': 'a', 'Ἆ': 'a', 'ἇ': 'a', 'Ἇ': 'a', 'ᾀ': 'a', 'ᾈ': 'a', 'ᾁ': 'a', 'ᾉ': 'a',
          'ᾂ': 'a', 'ᾊ': 'a', 'ᾃ': 'a', 'ᾋ': 'a', 'ᾄ': 'a', 'ᾌ': 'a', 'ᾅ': 'a', 'ᾍ': 'a', 'ᾆ': 'a', 'ᾎ': 'a',
          'ᾇ': 'a', 'ᾏ': 'a', 'ᾰ': 'a', 'Ᾰ': 'a', 'ᾱ': 'a', 'Ᾱ': 'a', 'ὰ': 'a', 'Ὰ': 'a', 'ᾳ': 'a', 'ᾼ': 'a',
          'ᾲ': 'a', 'ᾴ': 'a', 'ᾶ': 'a', 'ᾷ': 'a', 'а': 'a', 'А': 'a', 'ả': 'a', 'ა': 'a', 'ậ': 'a', 'ạ': 'a',
          'ẵ': 'a', 'ằ': 'a', 'ắ': 'a', 'ầ': 'a', 'ա': 'a', 'ä': 'ae', 'Ä': 'ae', 'æ': 'ae', 'Æ': 'ae',
          'ḃ': 'b', 'Ḃ': 'b', 'β': 'b', 'Β': 'b', 'б': 'b', 'Б': 'b', 'ბ': 'b', 'բ': 'b',
          'ć': 'c', 'Ć': 'c', 'ĉ': 'c', 'Ĉ': 'c', 'č': 'c', 'Č': 'c', 'ċ': 'c', 'Ċ': 'c', 'ç': 'c', 'Ç': 'c',
          'ჩ': 'c', 'ჭ': 'c', 'ч': 'ch', 'Ч': 'ch', 'ճ': 'ch', 'չ': 'ch',
          'ď': 'd', 'Ď': 'd', 'ḋ': 'd', 'Ḋ': 'd', 'đ': 'd', 'Đ': 'd', 'δ': 'd', 'Δ': 'd', 'д': 'd', 'Д': 'd',
          'დ': 'd', 'ძ': 'd', 'ḑ': 'd', 'Ḑ': 'd', 'դ': 'd', 'ð': 'dh', 'Ð': 'dh', 'ձ': 'dz',
          'é': 'e', 'É': 'e', 'è': 'e', 'È': 'e', 'ĕ': 'e', 'Ĕ': 'e', 'ê': 'e', 'Ê': 'e', 'ě': 'e', 'Ě': 'e',
          'ë': 'e', 'Ë': 'e', 'ė': 'e', 'Ė': 'e', 'ę': 'e', 'Ę': 'e', 'ē': 'e', 'Ē': 'e', 'ε': 'e', 'Ε': 'e',
          'έ': 'e', 'Έ': 'e', 'ἐ': 'e', 'Ἐ': 'e', 'ἑ': 'e', 'Ἑ': 'e', 'ἒ': 'e', 'Ἒ': 'e', 'ἓ': 'e', 'Ἓ': 'e',
          'ἔ': 'e', 'Ἔ': 'e', 'ἕ': 'e', 'Ἕ': 'e', 'ὲ': 'e', 'Ὲ': 'e', 'е': 'e', 'Е': 'e', 'ё': 'e', 'Ё': 'e',
          'э': 'e', 'Э': 'e', 'ე': 'e', 'ế': 'e', 'ệ': 'e', 'ề': 'e', 'ե': 'e', 'է': 'e', 'և': 'ev',
          'ḟ': 'f', 'Ḟ': 'f', 'ƒ': 'f', 'Ƒ': 'f', 'φ': 'f', 'Φ': 'f', 'ф': 'f', 'Ф': 'f', 'ֆ': 'f',
          'ğ': 'g', 'Ğ': 'g', 'ĝ': 'g', 'Ĝ': 'g', 'ġ': 'g', 'Ġ': 'g', 'ģ': 'g', 'Ģ': 'g', 'γ': 'g', 'Γ': 'g',
          'г': 'g', 'Г': 'g', 'გ': 'g', 'ღ': 'g', 'գ': 'g', 'ղ': 'gh',
          'ĥ': 'h', 'Ĥ': 'h', 'ħ': 'h', 'Ħ': 'h', 'ḩ': 'h', 'Ḩ': 'h', 'ჰ': 'h', 'հ': 'h',
          'ĳ': 'i', 'Ĳ': 'I', 'ĭ': 'i', 'Ĭ': 'I', 'ı': 'i', 'İ': 'I', 'η': 'i', 'Η': 'I', 'í': 'i', 'Í': 'I',
          'ì': 'i', 'Ì': 'I', 'î': 'i', 'Î': 'I', 'ï': 'i', 'Ï': 'I', 'ĩ': 'i', 'Ĩ': 'I', 'į': 'i', 'Į': 'I',
          'ī': 'i', 'Ī': 'I', 'ή': 'i', 'Ή': 'I', 'ἠ': 'i', 'Ἠ': 'I', 'ἡ': 'i', 'Ἡ': 'I', 'ἢ': 'i', 'Ἢ': 'I',
          'ἣ': 'i', 'Ἣ': 'I', 'ἤ': 'i', 'Ἤ': 'I', 'ἥ': 'i', 'Ἥ': 'I', 'ἦ': 'i', 'Ἦ': 'I', 'ἧ': 'i', 'Ἧ': 'I',
          'ᾐ': 'i', 'ᾘ': 'I', 'ᾑ': 'i', 'ᾙ': 'I', 'ᾒ': 'i', 'ᾚ': 'I', 'ᾓ': 'i', 'ᾛ': 'I', 'ᾔ': 'i', 'ᾜ': 'I',
          'ᾕ': 'i', 'ᾝ': 'I', 'ᾖ': 'i', 'ᾞ': 'I', 'ᾗ': 'i', 'ᾟ': 'I', 'ὴ': 'i', 'Ὴ': 'I', 'ῃ': 'i', 'ῌ': 'I',
          'ῂ': 'i', 'ῄ': 'i', 'ῆ': 'i', 'ῇ': 'i', 'ι': 'i', 'Ι': 'I', 'ί': 'i', 'Ί': 'I', 'ϊ': 'i', 'Ϊ': 'I',
          'ἰ': 'i', 'Ἰ': 'I', 'ἱ': 'i', 'Ἱ': 'I', 'ἲ': 'i', 'Ἲ': 'I', 'ἳ': 'i', 'Ἳ': 'I', 'ἴ': 'i', 'Ἴ': 'I',
          'ἵ': 'i', 'Ἵ': 'I', 'ἶ': 'i', 'Ἶ': 'I', 'ἷ': 'i', 'Ἷ': 'I', 'ὶ': 'i', 'Ὶ': 'I', 'ῐ': 'i', 'Ῐ': 'I',
          'ῑ': 'i', 'Ῑ': 'I', 'ῒ': 'i', 'ῖ': 'i', 'ῗ': 'i', 'ΐ': 'i', 'и': 'i', 'И': 'i', 'ი': 'i', 'ị': 'i',
          'ỉ': 'i', 'ի': 'i', 'ĵ': 'j', 'Ĵ': 'J', 'й': 'j', 'Й': 'j', 'ჯ': 'j', 'ջ': 'j',
          'ķ': 'k', 'Ķ': 'k', 'ĸ': 'k', 'κ': 'k', 'Κ': 'k', 'к': 'k', 'К': 'k', 'ξ': 'k', 'Ξ': 'k', 'კ': 'k',
          'ქ': 'k', 'ხ': 'k', 'կ': 'k', 'խ': 'kh', 'х': 'kh', 'Х': 'kh',
          'ĺ': 'l', 'Ĺ': 'l', 'ľ': 'l', 'Ľ': 'l', 'ļ': 'l', 'Ļ': 'l', 'ł': 'l', 'Ł': 'l', 'ŀ': 'l', 'Ŀ': 'l',
          'λ': 'l', 'Λ': 'l', 'л': 'l', 'Л': 'l', 'ლ': 'l', 'լ': 'l',
          'ṁ': 'm', 'Ṁ': 'm', 'μ': 'm', 'Μ': 'm', 'м': 'm', 'М': 'm', 'მ': 'm', 'մ': 'm',
          'ń': 'n', 'Ń': 'n', 'ň': 'n', 'Ň': 'n', 'ñ': 'n', 'Ñ': 'n', 'ņ': 'n', 'Ņ': 'n', 'ŋ': 'n', 'Ŋ': 'n',
          'ŉ': 'n', 'ν': 'n', 'Ν': 'n', 'н': 'n', 'Н': 'n', 'ნ': 'n', 'ն': 'n',
          'ó': 'o', 'Ó': 'o', 'ò': 'o', 'Ò': 'o', 'ô': 'o', 'Ô': 'o', 'ő': 'o', 'Ő': 'o', 'ό': 'o', 'Ό': 'o',
          'ὀ': 'o', 'Ὀ': 'o', 'ὁ': 'o', 'Ὁ': 'o', 'ὂ': 'o', 'Ὂ': 'o', 'ὃ': 'o', 'Ὃ': 'o', 'ὄ': 'o', 'Ὄ': 'o',
          'ὅ': 'o', 'Ὅ': 'o', 'ὸ': 'o', 'Ὸ': 'o', 'õ': 'o', 'Õ': 'o', 'ō': 'o', 'Ō': 'o', 'ơ': 'o', 'Ơ': 'o',
          'ŏ': 'o', 'Ŏ': 'o', 'ο': 'o', 'Ο': 'o', 'œ': 'o', 'Œ': 'o', 'ω': 'o', 'Ω': 'o', 'ώ': 'o', 'Ώ': 'o',
          'ὠ': 'o', 'Ὠ': 'o', 'ὡ': 'o', 'Ὡ': 'o', 'ὢ': 'o', 'Ὢ': 'o', 'ὣ': 'o', 'Ὣ': 'o', 'ὤ': 'o', 'Ὤ': 'o',
          'ὥ': 'o', 'Ὥ': 'o', 'ὦ': 'o', 'Ὦ': 'o', 'ὧ': 'o', 'Ὧ': 'o', 'ᾠ': 'o', 'ᾨ': 'o', 'ᾡ': 'o', 'ᾩ': 'o',
          'ᾢ': 'o', 'ᾪ': 'o', 'ᾣ': 'o', 'ᾫ': 'o', 'ᾤ': 'o', 'ᾬ': 'o', 'ᾥ': 'o', 'ᾭ': 'o', 'ᾦ': 'o', 'ᾮ': 'o',
          'ᾧ': 'o', 'ᾯ': 'o', 'ὼ': 'o', 'Ὼ': 'o', 'ῳ': 'o', 'ῼ': 'o', 'ῲ': 'o', 'ῴ': 'o', 'ῶ': 'o', 'ῷ': 'o',
          'о': 'o', 'О': 'o', 'ồ': 'o', 'ộ': 'o', 'ო': 'o', 'օ': 'o', 'ø': 'oe', 'Ø': 'oe', 'ö': 'oe', 'Ö': 'oe',
          'ṗ': 'p', 'Ṗ': 'p', 'ψ': 'p', 'Ψ': 'p', 'п': 'p', 'П': 'p', 'პ': 'p', 'ფ': 'p', 'պ': 'p', 'փ': 'p',
          'π': 'pi', 'Π': 'pi',
          'ყ': 'q', 'ք': 'q',
          'ŕ': 'r', 'Ŕ': 'r', 'ř': 'r', 'Ř': 'r', 'ŗ': 'r', 'Ŗ': 'r', 'ρ': 'r', 'Ρ': 'r', 'ῥ': 'r', 'Ῥ': 'r',
          'р': 'r', 'Р': 'r', 'ῤ': 'r', 'რ': 'r', 'ռ': 'r', 'ր': 'r',
          'ś': 's', 'Ś': 's', 'ŝ': 's', 'Ŝ': 's', 'š': 's', 'Š': 's', 'ṡ': 's', 'Ṡ': 's', 'ş': 's', 'Ş': 's',
          'ș': 's', 'Ș': 's', 'ſ': 's', 'ς': 's', 'σ': 's', 'Σ': 's', 'с': 's', 'С': 's', 'ս': 's', 'ს': 's',
          'შ': 's', 'ß': 'ss', 'ш': 'sh', 'Ш': 'sh', 'շ': 'sh', 'щ': 'sch', 'Щ': 'sch',
          'ť': 't', 'Ť': 't', 'ṫ': 't', 'Ṫ': 't', 'ţ': 't', 'Ţ': 't', 'ț': 't', 'Ț': 't', 'ŧ': 't', 'Ŧ': 't',
          'θ': 't', 'Θ': 't', 'τ': 't', 'Τ': 't', 'т': 't', 'Т': 't', 'თ': 't', 'ტ': 't', 'ც': 't', 'წ': 't',
          'թ': 't', 'տ': 't', 'þ': 'th', 'Þ': 'th', 'ц': 'ts', 'Ц': 'ts', 'ծ': 'ts', 'ց': 'ts',
          'ú': 'u', 'Ú': 'U', 'ù': 'u', 'Ù': 'U', 'ŭ': 'u', 'Ŭ': 'U', 'û': 'u', 'Û': 'U', 'ů': 'u', 'Ů': 'U',
          'ű': 'u', 'Ű': 'U', 'ũ': 'u', 'Ũ': 'U', 'ų': 'u', 'Ų': 'U', 'ū': 'u', 'Ū': 'U', 'ư': 'u', 'Ư': 'U',
          'у': 'u', 'У': 'U', 'უ': 'u', 'ừ': 'u', 'ու': 'u', 'ü': 'ue', 'Ü': 'ue', 'ю': 'ju', 'Ю': 'ju',
          'в': 'v', 'В': 'v', 'ვ': 'v', 'վ': 'v',
          'ẃ': 'w', 'Ẃ': 'w', 'ẁ': 'w', 'Ẁ': 'w', 'ŵ': 'w', 'Ŵ': 'w', 'ẅ': 'w', 'Ẅ': 'w',
          'χ': 'x', 'Χ': 'x',
          'ý': 'y', 'Ý': 'y', 'ỳ': 'y', 'Ỳ': 'y', 'ŷ': 'y', 'Ŷ': 'y', 'ÿ': 'y', 'Ÿ': 'y', 'υ': 'y', 'Υ': 'y',
          'ύ': 'y', 'Ύ': 'y', 'ϋ': 'y', 'Ϋ': 'y', 'ὑ': 'y', 'Ὑ': 'y', 'ὓ': 'y', 'Ὓ': 'y', 'ὕ': 'y', 'Ὕ': 'y',
          'ὗ': 'y', 'Ὗ': 'y', 'ῠ': 'y', 'Ῠ': 'y', 'ῡ': 'y', 'Ῡ': 'y', 'ὺ': 'y', 'Ὺ': 'y', 'ΰ': 'y', 'ὐ': 'y',
          'ὒ': 'y', 'ὔ': 'y', 'ը': 'y', 'յ': 'y', 'ὖ': 'y', 'ῢ': 'y', 'ῦ': 'y', 'ῧ': 'y', 'ы': 'y', 'Ы': 'y',
          'я': 'ya', 'Я': 'ya',
          'ź': 'z', 'Ź': 'z', 'ž': 'z', 'Ž': 'z', 'ż': 'z', 'Ż': 'z', 'ζ': 'z', 'Ζ': 'z', 'з': 'z', 'З': 'z',
          'ზ': 'z', 'ჟ': 'z', 'զ': 'z', 'ժ': 'zh', 'ж': 'zh', 'Ж': 'zh',
          '$': 's', '+': 'and', '&': 'and', '°': 'degree', 'ъ': '', 'Ъ': '', 'ь': '', 'Ь': ''
        };
        str = str.trim().split('');
        let result = '';
        for (let char in str) {
          result += (typeof converter[str[char]] != 'undefined')
              ? converter[str[char]]
              : str[char];
        }
        return result;
      },
      /**
       * Convert string
       * @param str
       * @returns {string}
       */
      str2url: str => {
        str = App.common.transliteration.strToEn(str);
        return str.toLowerCase().replace(/[^-a-z0-9_\.\#]/g, '-');
      },
    },
    /**
     * Capitalize first letter
     * @param {string} str
     * @returns {string}
     */
    ucfirst: str => str.charAt(0).toUpperCase() + str.slice(1),
    /**
     * Generate unique id value
     * @returns {string}
     */
    uniquid: () => (Math.floor(Math.random() * 1e19)).toString(16)
  },
  /*
   * Application modules list
   */
  modules: {
    default: {
      events: {
        /**
         * Check fieldset expand/collapse click
         */
        'fieldset': [{
          action: 'on.click',
          item: '.expand-collapse',
          callback: function () {
            const $elem = $(this).find('.fa');
            const $parent = $(this).closest('fieldset');
            if ($elem.hasClass('fa-chevron-down')) {
              $elem.removeClass('fa-chevron-down').addClass('fa-chevron-up');
              $parent.find('.row').addClass('hide');
              $parent.find('.image-preview').addClass('hide');
            } else {
              $elem.removeClass('fa-chevron-up').addClass('fa-chevron-down');
              $parent.find('.row').removeClass('hide');
              $parent.find('.image-preview').removeClass('hide');
            }
          }
        }],
        /**
         * Page navigation menu click
         */
        'ul.page-navigation': [{
          action: 'on.click',
          item: 'li',
          callback: function () {
            const link = $(this).attr('data-link');
            App.common.goto($('fieldset[data-link="' + link + '"]'));
          }
        }]
      }
    }
  },
  /*
   * Application requests
   */
  requests: {
    /**
     * Run custom ajax request
     * @param {string} url
     * @param {string} method
     * @param {object} data
     * @returns {jQuery|{getAllResponseHeaders: (function(): *), abort: (function(*=): jqXHR), setRequestHeader: (function(*=, *): jqXHR), readyState: number, getResponseHeader: (function(*): *), overrideMimeType: (function(*): jqXHR), statusCode: (function(*=): jqXHR)}|HTMLElement|(function(*=, *=): *)|(function(*=, *=): *)|(function(*=, *=): *)|*}
     */
    custom: (url, method, data) => {
      return $.ajax({
        url: url,
        method: method,
        headers: {
          Authorization: 'Bearer ' + $('meta[name="jwt-token"]').attr('content')
        },
        data: data,
        success: (response, status, request) => {
        },
        error: (xhrError) => {
          $('.preloader-wrap').hide();
          App.requests.processErrors(url, method, xhrError)
        }
      });
    },
    /**
     * Send the item save request
     * @param url
     * @param method
     * @param data
     * @returns {jQuery|{getAllResponseHeaders: (function(): *), abort: (function(*=): jqXHR), setRequestHeader: (function(*=, *): jqXHR), readyState: number, getResponseHeader: (function(*): *), overrideMimeType: (function(*): jqXHR), statusCode: (function(*=): jqXHR)}|HTMLElement|(function(*=, *=): *)|(function(*=, *=): *)|jQuery|*}
     */
    save: (data, url = null, method = null) => {
      if (!url) {
        url = $('.form-wrap').attr('action');
      }
      if (!method) {
        method = $('.form-wrap').attr('method');
      }

      return $.ajax({
        url: url,
        method: method,
        headers: {
          Authorization: 'Bearer ' + $('meta[name="jwt-token"]').attr('content')
        },
        data: data,
        beforeSend: () => {
          $('.preloader-wrap').show();
        },
        success: (response, status, request) => {
          if (typeof data[App.modules.messages.titleField] !== 'undefined') {
            //Set saving message to local storage
            App.storage.addMessages([{
              success: App.common.prepareMessage(App.modules.messages.success, data[App.modules.messages.titleField])
            }]);
          } else {
            for (let i in data) {
              if (data[i].length) {
                App.storage.addMessages([{
                  success: App.common.prepareMessage(App.modules.messages.updated, data[i])
                }]);
              }
            }
          }

          //Check if resource has been modified
          if (request.status === 200) {
            //View saved messages
            App.modules.messagesList.applyMessages();
          }

          $('.preloader-wrap').hide();
        },
        error: (xhrError) => {
          $('.preloader-wrap').hide();
          App.requests.processErrors(url, method, xhrError)
        }
      })
    },
    /**
     * Send remove item request
     *
     * @param $this
     * @param url
     * @returns {*|jQuery|{getAllResponseHeaders, abort, setRequestHeader, readyState, getResponseHeader, overrideMimeType, statusCode}}
     */
    remove: ($this, url) => {
      return $.ajax({
        url: url,
        method: 'DELETE',
        headers: {Authorization: 'Bearer ' + $('meta[name="jwt-token"]').attr('content')},
        beforeSend: () => {
          if ($this) {
            $this.closest('tr').css({'background-color': '#ff5131'});
          }
        },
        success: (response, status, request) => {
        },
        error: (xhrError) => {
          App.requests.processErrors(url, 'DELETE', xhrError)
        }
      });
    },
    /**
     * Take $_GET request parameters
     * @returns {{}}
     */
    getRequest: () => {
      return window.location.search.replace('?', '').split('&').reduce(
          (prev, curr) => {
            const temp = curr.split('=');
            prev[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
            return prev;
          }, {}
      );
    },
    /**
     * Handle Ajax errors
     * @param url
     * @param method
     * @param xhrError
     */
    processErrors: (url, method, xhrError) => {
      switch (xhrError.status) {
          //Validation failed
        case 400:
          if (typeof xhrError.responseJSON.errors != 'undefined') {
            App.validation.handle(xhrError.responseJSON.errors);
          }
          break;
          //Resource not found
        case 404:
          App.modules.popup.error(url, method, xhrError.status);
          break;
          //Other error type
        default:
          App.modules.popup.error(url, method, xhrError.status, xhrError.responseJSON);
      }
    },
  }
};

/**
 * Init application module events
 */
function initModules() {
  //Run module events
  for (let module in App.modules) {
    /**
     * Check if modules has initial functions
     */
    if (typeof App.modules[module].init === 'function') {
      App.modules[module].init();
    }
    //take selectors
    for (let selector in App.modules[module].events) {
      //take selector event listener description
      let descriptor = App.modules[module].events[selector];
      //get events for selector
      for (let i in descriptor) {
        //check if action is "live"
        let action = descriptor[i].action.split('.');
        if (action.length > 1) {
          //check inner selector
          if (typeof descriptor[i].item !== 'undefined') {
            if (typeof descriptor[i].debounce !== 'undefined') {
              $(selector)[action[0]](action[1], descriptor[i].item, debounce(descriptor[i].callback, descriptor[i].debounce));
            } else {
              $(selector)[action[0]](action[1], descriptor[i].item, descriptor[i].callback);
            }
          } else {
            if (typeof descriptor[i].debounce !== 'undefined') {
              $(selector)[action[0]](action[1], debounce(descriptor[i].callback, descriptor[i].debounce));
            } else {
              $(selector)[action[0]](action[1], descriptor[i].callback);
            }
          }
        } else {
          if (typeof descriptor[i].debounce !== 'undefined') {
            $(selector)[action[0]](debounce(descriptor[i].callback, descriptor[i].debounce));
          } else {
            $(selector)[action[0]](descriptor[i].callback);
          }
        }
      }
    }
  }
}

$(document).ready(() => {
  $.ajax({
    url: '/api/settings/admin',
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + $('meta[name="jwt-token"]').attr('content')
    },
    success: response => {
      if (typeof response.sidemenu != 'undefined' && response.sidemenu === 1) {
        $('body').addClass('collapse');
      }
    },
    error: (xhrError) => {
      App.requests.processErrors(url, 'DELETE', xhrError)
    }
  }).then(settings => {
    App.siteSettings = settings;
    initModules();
    //DateTime picker init
    if ($('.datepickerInit').length) {
      $('.datepickerInit').datepicker();
    }

    //Page Navigation Menu
    if ($('ul.page-navigation').length) {
      //create menu items
      App.common.buildAsideMenu();
    }

    /**
     * Submit form
     */
    $('form.form-wrap').submit(e => {
      e.preventDefault();
      App.validation.run(App.form.validation, App.form.submit);
    });
  });
});