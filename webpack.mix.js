const mix = require('laravel-mix');

const fs = require('fs');
const path = require('path');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const moduleFolder = 'Modules';
const dirs = p => fs.readdirSync(p).filter(f => fs.statSync(path.resolve(p, f)).isDirectory());
const modules = dirs(moduleFolder);

for (let i in modules) {
  const source = [moduleFolder, modules[i], 'Resources', 'assets', 'js', 'admin'].join('/');
  const destination = 'public/js/admin/scripts/' + modules[i].toLowerCase() + '.js';

  fs.readdir(source, function (err, files) {
    if (typeof files != 'undefined' && files.length) {
      mix.combine(source + '/*.*', destination)
         .minify(destination);
    }
  });
}

mix
  //CKEditor
  .copy('node_modules/ckeditor4/config.js', 'public/js/ckeditor/config.js')
  .copy('node_modules/ckeditor4/ckeditor.js', 'public/js/ckeditor/ckeditor.js')
  .copy('node_modules/ckeditor4/styles.js', 'public/js/ckeditor/styles.js')
  .copy('node_modules/ckeditor4/contents.css', 'public/js/ckeditor/contents.css')
  .copyDirectory('node_modules/ckeditor4/skins', 'public/js/ckeditor/skins')
  .copyDirectory('node_modules/ckeditor4/lang', 'public/js/ckeditor/lang')
  .copyDirectory('node_modules/ckeditor4/plugins', 'public/js/ckeditor/plugins')
  //Application
  .combine('resources/assets/js/admin/app.js', 'public/js/admin/app.js')
  .sass('resources/assets/sass/admin/theme/app.scss', 'public/css/admin')
  //Libs
  .combine([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/air-datepicker/dist/js/datepicker.js',
    'resources/assets/js/admin/libs/debounce.js',
  ], 'public/js/admin/libs.js')
  //Components
  .combine([
    'resources/assets/js/admin/components/*.*',
    'resources/assets/js/admin/files/file.js',
    'resources/assets/js/admin/files/file-image.js',
    'resources/assets/js/admin/files/file-slider.js',
  ], 'public/js/admin/components.js')
  //Sortable
  .combine('node_modules/sortablejs/Sortable.min.js', 'public/js/admin/sortable.min.js')
  //minify
  .minify([
    'public/js/admin/app.js',
    'public/js/admin/libs.js',
    'public/js/admin/components.js'
  ])
  //Datepicker
  .styles(['node_modules/air-datepicker/dist/css/datepicker.css'], 'public/css/admin/datepicker.css')
  //Reset
  .styles(['resources/assets/sass/admin/theme/reset.scss'], 'public/css/admin/reset.css')
  //Font-awesome
  .sass('resources/assets/sass/admin/font-awesome/all.scss', 'public/css/admin/font-awesome.css')
  //Static pages content constructor
  .sass('resources/assets/sass/admin/theme/content-constructor.scss', 'public/css/admin/content-constructor.css')
  .disableNotifications();