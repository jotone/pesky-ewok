<?php

namespace Tests\TestCases;

use Modules\Permissions\Entities\Permission;
use Modules\Roles\Entities\Role;

trait PermissionsProvider
{
    /**
     * Set permissions to user
     * @param array $permissions
     * @return $this
     */
    protected function providePermissions(array $permissions)
    {
        self::$user->roles()->detach();

        foreach ($permissions as $permission => $value) {
            $permission = Permission::where('route', $permission)->first();

            $role = factory(Role::class)->create();
            $role->permissions()->save($permission, ['value' => $value]);
            $role->users()->attach(self::$user->id);
        }

        return $this;
    }
}