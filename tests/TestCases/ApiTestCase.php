<?php

namespace Tests\TestCases;

use Modules\User\Entities\User;
use Tests\TestCase;

abstract class ApiTestCase extends TestCase
{
    use PermissionsProvider;
    /**
     * Tester model
     *
     * @var mixed
     */
    protected static $user = null;

    /**
     * Tester JWT token
     *
     * @var string
     */
    protected static $token = null;

    /**
     * @var mixed
     */
    protected static $model;

    /**
     * @param $name
     * @param $callback
     */
    protected function wrapApiTest($name, $callback)
    {
        $this->wrap($name, function () use ($callback) {
            $this->checkDBMockData();
            if (!self::$token) {
                $this->createToken();
            }

            $callback();
        });
    }

    /**
     * Provide jwt authorisation
     *
     * @return string
     */
    protected function createToken()
    {
        if (empty(self::$user) || !(self::$user instanceof User)) {
            self::$user = factory(User::class)->create();
        }
        $response = $this->post(route('api.auth.login'), [
            'email'    => self::$user->email,
            'password' => env('ADMIN_PSW')
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'jwt',
                'data',
                'ttl'
            ]);

        self::$token = json_decode($response->content())->jwt;
        return self::$token;
    }
}