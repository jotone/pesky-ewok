<?php

namespace Tests\TestCases;

use Modules\User\Entities\User;
use Facebook\WebDriver\WebDriverBy;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Concerns\ProvidesBrowser;
use Tests\Browser\DuskTestingTrait;
use Tests\DuskTestCase;

class DuskTestCaseSingleton extends DuskTestCase
{
    use DuskTestingTrait;
    use PermissionsProvider;
    use ProvidesBrowser;

    /**
     * Instance of testing environment
     *
     * @var null
     */
    private static $instance = null;

    /**
     * Tester observer user
     *
     * @var User
     */
    protected static $user;

    /**
     * Singleton
     * @return DuskTestCaseSingleton|null
     */
    public static function getInstance()
    {
        return (self::$instance === null)
            ? self::$instance = new static()
            : self::$instance;
    }

    /**
     * Get number of selected items
     * @param Browser $browser
     * @param string $css_selector
     * @return int
     */
    protected function countItems(Browser $browser, string $css_selector): int
    {
        return count($browser->driver->findElements(WebDriverBy::cssSelector($css_selector)));
    }

    /**
     * Get time in ms
     * @return float
     */
    protected function getMS()
    {
        return round(microtime(true) * 1000);
    }

    /**
     * Check if user is logged in
     * @param $browser
     * @return bool
     */
    public function logIn($browser)
    {
        if (empty($browser->driver->manage()->getCookieNamed('jwt-token'))) {
            $browser->visitRoute('admin.auth.index')
                ->type('input[name="email"]', self::$user->email)
                ->type('input[name="password"]', env('ADMIN_PSW'))
                ->click('button[type="submit"]')
                ->waitForText('Welcome', 15)
                ->assertSee('Welcome')
                ->assertRouteIs('admin.dashboard.index')
                ->assertAuthenticatedAs(self::$user);
        }

        return $browser;
    }

    /**
     * Get message
     * @param string $message
     * @param string $value
     * @return string|string[]|null
     */
    protected function message(string $message, string $value)
    {
        return preg_replace('/:attribute/u', $value, $message);
    }

    /**
     * Wrap test with execution time function
     * @param string $name
     * @param $callback
     */
    protected function wrap($name, $callback)
    {
        $executionTime = $this->getMS();
        if (empty(self::$user) || !(self::$user instanceof User)) {
            self::$user = factory(User::class)->create();
        }

        self::$user->remember_token = env('APP_KEY');
        self::$user->save();

        $callback();

        dump($name . ' finished. Passed time is: ' . ($this->getMS() - $executionTime) . ' ms');
    }
}
