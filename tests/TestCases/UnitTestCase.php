<?php

namespace Tests\TestCases;

use Modules\User\Entities\User;
use Tests\TestCase;

abstract class UnitTestCase extends TestCase
{
    /**
     * Name of testing class
     *
     * @var string
     */
    protected $class;

    /**
     * Testing model
     *
     * @var mixed
     */
    protected static $model;

    /**
     * Wrap test with execution time function
     *
     * @param string $name
     * @param $callback
     */
    protected function wrapUnit($name, $callback)
    {
        $this->wrap($name, function () use ($callback) {
            if (!User::where('remember_token', env('APP_KEY'))->count()) {
                factory(User::class)->create();
            }

            if (!isset(self::$model) || !(self::$model instanceof $this->class)) {
                self::$model = factory($this->class)->create();
            }

            $callback();
        });
    }

    /**
     * Run remove model test
     *
     * @param string $field
     * @param string $value
     */
    protected function runRemove(string $field, string $value)
    {
        $models = $this->class::where($field, 'like', $value)->pluck('id')->toArray();

        foreach ($models as $id) {
            $this->class::destroy($id);

            $this->assertDatabaseMissing($this->table, ['id' => $id]);
        }

        $this->assertDatabaseMissing($this->table, [
            $field => $value
        ]);
    }
}