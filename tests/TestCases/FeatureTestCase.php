<?php

namespace Tests\TestCases;

use App\Models\AdminRole;
use App\Models\Permission;
use Modules\User\Entities\User;
use Tests\TestCase;

abstract class FeatureTestCase extends TestCase
{
    /**
     * Tester model
     *
     * @var mixed
     */
    protected static $user;

    /**
     * @var mixed
     */
    protected static $model;

    /**
     * Wrap test with execution time function
     *
     * @param $name
     * @param $callback
     */
    protected function wrapFeature($name, $callback)
    {
        $this->wrap($name, function () use ($callback) {
            if (empty(self::$user) || !(self::$user instanceof User)) {
                self::$user = factory(User::class)->create();
            }

            $callback();

            \DB::table('user_roles')->where('user_id', self::$user->id)->delete();
        });
    }

    /**
     * Provide permission to specified user
     *
     * @param string $permission
     * @param string $value
     * @return $this
     */
    protected function providePermissions(string $permission, string $value = 'f')
    {
        self::$user->roles()->detach();

        $permission = Permission::where('route', $permission)->first();

        $role = factory(AdminRole::class)->create();
        $role->permissions()->save($permission, ['value' => $value]);
        $role->users()->attach(self::$user->id);

        return $this;
    }

    /**
     * Check middleware permissions
     *
     * @param string $permission
     * @param string $route
     * @param string $method
     * @return $this
     */
    protected function checkMiddleware(string $permission, string $route, string $method = 'get')
    {
        $this->providePermissions($permission, '0')
            ->actingAs(self::$user)->{$method}($route)
            ->assertStatus(403);

        return $this;
    }

    /**
     * Regenerate model value
     */
    protected function refreshModel()
    {
        if (!isset(self::$model) || !(self::$model instanceof $this->class)) {
            self::$model = factory($this->class)->create();
        }

        return $this;
    }
}