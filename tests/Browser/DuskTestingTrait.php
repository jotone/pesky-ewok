<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;

trait DuskTestingTrait
{
    /**
     * Testing model
     * @var mixed
     */
    protected static $model;

    /**
     * Run index page testing
     * @param string $function
     */
    protected function runIndex(string $function)
    {
        self::getInstance()->browse(function (Browser $browser) use ($function) {
            $this->wrap($function, function () use ($browser) {
                $models = (isset(static::$options['condition']))
                    ? $this->class::where(static::$options['condition'])->take(30)->orderBy('id', 'asc')->get()->toArray()
                    : $this->class::take(30)->orderBy('id', 'asc')->get()->toArray();
                $last_model = (isset(static::$options['condition']))
                    ? $this->class::where(static::$options['condition'])->take(1)->orderBy('id', 'desc')->first()
                    : $this->class::take(1)->orderBy('id', 'desc')->first();
                self::$model = $this->class::where(static::$options['where'])->inRandomOrder()->first();

                //Test middleware
                $this->providePermissions(['dashboard' => '8']);

                $this->logIn($browser)
                    ->visitRoute('admin.' . static::$options['route'] . '.index')
                    ->waitForText('403', 20)
                    ->assertSee('403')
                    ->assertDontSee(static::$options['text']['index']);

                //Test page components
                $this->providePermissions([
                    static::$options['route'] => '8',
                    'dashboard'               => '8'
                ]);

                //Check list query
                $browser->visitRoute('admin.' . static::$options['route'] . '.index')
                    ->waitForText(static::$options['text']['index'], 20)
                    ->assertSee(static::$options['text']['index'])
                    ->waitForText($models[0][static::$options['test_field']], 20)
                    ->assertSee($models[0][static::$options['test_field']])
                    ->assertSee($models[24][static::$options['test_field']]);

                $this->assertTrue(25 == $this->countItems($browser, '.items-list tbody tr'));

                //Check pagination
                $browser->pause(500)
                    ->click('.bottom-controls-wrap .pagination-wrap ul li:nth-child(2)')
                    ->waitForText($models[25][static::$options['test_field']], 20)
                    ->assertSee($models[25][static::$options['test_field']])
                    //Check changing per-page value
                    ->select('select[name="perPage"]', '10')
                    ->waitForText($models[10][static::$options['test_field']], 20)
                    ->assertSee($models[10][static::$options['test_field']])
                    ->assertSee($models[19][static::$options['test_field']]);

                $this->assertTrue(10 == $this->countItems($browser, '.items-list tbody tr'));

                //Check directions
                $browser->click('.bottom-controls-wrap .pagination-wrap ul li:nth-child(1)')
                    ->waitForText($models[0][static::$options['test_field']], 20)
                    ->assertSee($models[0][static::$options['test_field']])
                    ->click('th#id .directions .desc')
                    ->pause(750)
                    ->waitForText($last_model->{static::$options['test_field']}, 20)
                    ->assertsee($last_model->{static::$options['test_field']})
                    ->assertDontSee($models[0][static::$options['test_field']])
                    //Check search
                    ->click('th#id .search-expand')
                    ->pause(750)
                    ->type('th#id .search-input', self::$model->id)
                    ->waitForText(self::$model->{static::$options['test_field']}, 20)
                    ->assertSee(self::$model->{static::$options['test_field']});
            });
        });
    }

    /**
     * Run create page test
     * @param string $function
     * @param $creation_callback
     */
    protected function runCreate(string $function, $creation_callback)
    {
        self::getInstance()->browse(function (Browser $browser) use ($function, $creation_callback) {
            $this->wrap($function, function () use ($browser, $creation_callback) {
                //Test middleware
                $this->providePermissions([
                    static::$options['route'] => '8',
                    'dashboard'               => '8'
                ]);

                $first_model = (isset(static::$options['condition']))
                    ? $this->class::where(static::$options['condition'])->orderBy('id', 'asc')->first()
                    : $this->class::orderBy('id', 'asc')->first();
                $model = factory($this->class)->make();

                $this->logIn($browser)
                    ->visitRoute('admin.' . static::$options['route'] . '.index')
                    ->waitForText(static::$options['text']['index'], 20)
                    ->assertSee(static::$options['text']['index'])
                    ->assertSee('Create')
                    ->waitForText($first_model->{static::$options['test_field']}, 20)
                    ->assertSee($first_model->{static::$options['test_field']})
                    ->click('.page-controls-wrap .buttons-wrap .green')
                    ->assertDontSee(static::$options['text']['create'])
                    ->waitForText('403', 20)
                    ->assertSee('403');

                $this->assertDatabaseMissing(static::$options['table'], [
                    static::$options['test_field'] => $model->{static::$options['test_field']}
                ]);

                //Test user creation
                $this->providePermissions([
                    static::$options['route'] => 'e'
                ]);

                $browser->visitRoute('admin.' . static::$options['route'] . '.create')
                    ->waitForText(static::$options['text']['create'], 20)
                    ->assertSee(static::$options['text']['create']);

                $creation_callback($browser, $model, $first_model);
            });
        });
    }

    /**
     * Run update page test
     * @param string $function
     * @param $update_callback
     */
    protected function runUpdate(string $function, $update_callback)
    {
        self::getInstance()->browse(function (Browser $browser) use ($function, $update_callback) {
            $this->wrap($function, function () use ($browser, $update_callback) {
                $this->providePermissions([
                    'dashboard'               => '8',
                    static::$options['route'] => 'a'
                ]);

                $first_model = (isset(static::$options['condition']))
                    ? $this->class::where(static::$options['condition'])->orderBy('id', 'asc')->first()
                    : $this->class::orderBy('id', 'asc')->first();
                $model = factory($this->class)->make();
                self::$model = $this->class::where(static::$options['where'])->inRandomOrder()->first();

                $this->logIn($browser)
                    ->visitRoute('admin.' . static::$options['route'] . '.index')
                    ->waitForText(static::$options['text']['index'], 20)
                    ->assertSee(static::$options['text']['index'])
                    ->waitForText($first_model->{static::$options['test_field']}, 20)
                    ->assertSee($first_model->{static::$options['test_field']})
                    ->click('th#' . static::$options['test_field'] . ' .search-expand')
                    ->pause(750);

                $this->search($browser);

                $this->assertTrue(1 == $this->countItems($browser, '.items-list tbody tr'));

                $browser->click('.items-list tbody tr .actions .edit')
                    ->waitForText(static::$options['text']['edit'], 20)
                    ->assertSee(static::$options['text']['edit']);

                $update_callback($browser, $model, $first_model);
            });
        });
    }

    /**
     * Run model delete test
     * @param string $function
     * @param $remove_callback
     */
    protected function runDelete(string $function, $remove_callback)
    {
        self::getInstance()->browse(function (Browser $browser) use ($function, $remove_callback) {
            $this->wrap($function, function () use ($browser, $remove_callback) {
                $this->providePermissions([
                    'dashboard'               => '8',
                    static::$options['route'] => '9'
                ]);

                $first_model = (isset(static::$options['condition']))
                    ? $this->class::where(static::$options['condition'])->orderBy('id', 'asc')->first()
                    : $this->class::orderBy('id', 'asc')->first();
                self::$model = $this->class::where(static::$options['where'])->inRandomOrder()->first();

                $this->logIn($browser)
                    ->visitRoute('admin.' . static::$options['route'] . '.index')
                    ->waitForText(static::$options['text']['index'], 20)
                    ->assertSee(static::$options['text']['index'])
                    ->assertSee('Create')
                    ->waitForText($first_model->{static::$options['test_field']}, 20)
                    ->assertSee($first_model->{static::$options['test_field']})
                    ->click('th#' . static::$options['test_field'] . ' .search-expand')
                    ->pause(750);

                $this->search($browser);

                $this->assertTrue(1 == $this->countItems($browser, '.items-list tbody tr'));

                $remove_callback($browser, $first_model);
            });
        });
    }
}