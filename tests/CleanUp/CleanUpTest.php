<?php

namespace Tests\CleanUp;

use Modules\Permissions\Entities\Permission;
use Modules\Roles\Entities\Role;
use Modules\User\Entities\User;
use Tests\TestCase;

class CleanUpTest extends TestCase
{
    /**
     * Remove all test data from application
     */
    public function testCleanUp()
    {
        $this->wrap('Clean Up DB', function () {
            $users = User::where('remember_token', env('APP_KEY'))->pluck('id')->toArray();
            $this->remove($users, User::class);

            $roles = Role::where('title', 'like', 'test_role_%')->pluck('id')->toArray();
            $this->remove($roles, Role::class);

            $permissions = Permission::where('title', 'like', 'test_permission_%')->pluck('id')->toArray();
            $this->remove($permissions, Permission::class);
        });
    }

    /**
     * Remove models
     * @param array $models
     * @param string $class
     */
    protected function remove(array $models, string $class)
    {
        foreach ($models as $id) {
            $class::destroy($id);

            $this->assertTrue(empty($class::find($id)));
        }
    }
}