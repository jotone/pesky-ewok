<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Modules\Permissions\Entities\Permission;
use Modules\Roles\Entities\Role;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Get time in ms
     *
     * @return float
     */
    protected function getMS()
    {
        return round(microtime(true) * 1000);
    }

    /**
     * Wrap test with execution time function
     *
     * @param string $name
     * @param $callback
     */
    protected function wrap($name, $callback) {
        $executionTime = $this->getMS();

        $callback();

        dump($name . ' finished. Passed time is: ' . ($this->getMS() - $executionTime) . ' ms');
    }
}
